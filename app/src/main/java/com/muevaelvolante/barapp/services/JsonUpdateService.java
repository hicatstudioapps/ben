package com.muevaelvolante.barapp.services;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.internal.Streams;
import com.lacostra.utils.notification.Sheduler;
import com.muevaelvolante.Aplication;
import com.muevaelvolante.barapp.cache.JsonCacheReader;
import com.muevaelvolante.barapp.cache.JsonCacheWriter;
import com.muevaelvolante.barapp.commons.Constants;
import com.muevaelvolante.barapp.interfaces.IResults;
import com.muevaelvolante.barapp.interfaces.ISettings;
import com.muevaelvolante.barapp.model.results.Result;
import com.muevaelvolante.barapp.model.results.ResultRoot;
import com.muevaelvolante.barapp.model.setting.Menu;
import com.muevaelvolante.barapp.model.setting.Setting;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by CQ on 28/04/2016.
 * Clase para la actualizacion de los json locales
 * en caso de que lo requieran, en este caso es solo
 * para los json del home por requerimiento del cliente
 */
public class JsonUpdateService extends Service {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        new CheckUpdateTask().execute();
        return 1;
    }

    private static PendingIntent getPendingIntent(Context var0) {
        return PendingIntent.getService(var0, 0, new Intent(var0, JsonUpdateService.class), PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public static void stopSchedule(Context var0) {
        PendingIntent var1 = getPendingIntent(var0);
        Sheduler.getAlarmManager(var0).cancel(var1);
    }

    public static void scheduleService(final Context var0, int timeMin) {

        new Thread(new Runnable() {
            public void run() {
                stopSchedule(var0);
                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(System.currentTimeMillis());
                cal.add(Calendar.MINUTE, timeMin);
                Sheduler.getAlarmManager(var0).set(1, cal.getTimeInMillis(), getPendingIntent(var0));
            }
        }).start();
    }

    class CheckUpdateTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            boolean updateSetting = false, updateHome = false;
            try {
                 /*
                  * primero cargamos el json que tenemos en local
                  * en este caso el de settings
                  * */
                String localSettingJson = JsonCacheReader.getCacheJson(Constants.jsonCacheRootDir + "/settings.json");
                Log.d("JsonUpdateService", "comence");
                /**
                 * intentamos cargar el json online
                 */
                ISettings iSettings = ServiceGenerator.createService(ISettings.class);
                Call<Setting> settingCall = iSettings.getSetings("en");
             /*
                    * aqui obtuvimos el json online, ahora vemos si difiere del local
                    * */
                String onlineJson = new Gson().toJson(settingCall.execute().body());
                if (!onlineJson.equals(localSettingJson)) {
                    updateSetting = true;
                        /*
                        * aqui guardamos esos datos en cache y notificamos a la activity
                        * */
                    synchronized (this) {
                        JsonCacheWriter.writeCache(Aplication.getAppFilepath(getBaseContext()) + "/settings.json");

                    }
                }
                Setting setting= new Gson().fromJson(onlineJson,Setting.class);
                for (Menu item :
                        setting.getMenu()) {
                    if(item.getID().toLowerCase().equals("results")){
                        IResults resultsApi = ServiceGenerator.createService(IResults.class);
                        Call<ResultRoot> methodCall = resultsApi.getResultsRoot(item.getURL());
                        ResultRoot root= methodCall.execute().body();
                        String resultRoot= new Gson().toJson(root, ResultRoot.class);
                        JsonCacheWriter.writeCache(Aplication.getAppFilepath(getBaseContext())+"/resultRoot.json",resultRoot);
                        Aplication.result_order=root.getResultsRaceOrders().get(0).getRace_default_order();
                        ArrayList<Result> result = resultsApi.getResults(root.getResultsLink().get(0).getURL()).execute().body();
                        String dataList= new Gson().toJson(result, ArrayList.class);
                        JsonCacheWriter.writeCache(Aplication.getAppFilepath(getBaseContext())+"/result.json",dataList);
                        Log.d("Cache", item.getID() + " updated");
                    }else
                    donwloadAndSave(item.getID().toLowerCase(), item.getURL());
                }
                localSettingJson = JsonCacheReader.getCacheJson(Constants.jsonCacheRootDir + "/newhome.json");
                Call<ResponseBody> homeData = iSettings.getHome(Aplication.homeUrl);
                onlineJson = homeData.execute().body().string();
                FileWriter writer = new FileWriter(Constants.jsonCacheRootDir + "/newhomeT.json");
                writer.write(onlineJson);
                writer.close();
                onlineJson = JsonCacheReader.getCacheJson(Constants.jsonCacheRootDir + "/newhomeT.json");
                if (!onlineJson.equals(localSettingJson)) {
                    updateSetting = true;
                        /*
                        * aqui guardamos esos datos en cache y notificamos a la activity
                        * */
                    synchronized (this) {
                        writer = new FileWriter(Constants.jsonCacheRootDir + "/newhome.json");
                        writer.write(onlineJson);
                        writer.close();
                    }
                }

            } catch (Exception ex) {
                JsonUpdateService.scheduleService(getApplicationContext(), 1);
                return null;
            }
                       /*
                        * guardo en las preferencia que se necesita actualizar la vista
                        * en caso de que la activity este en foreground y se ejecute el update
                        * en el onresume
                        * */
            if (updateHome || updateSetting) {
                SharedPreferences.Editor editor = getApplicationContext().getSharedPreferences(Constants.JSON_UPDATE_PREFERENCE, MODE_PRIVATE).edit();
                editor.putBoolean(Constants.JSON_UPDATE_NEEDED, true).commit();
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent("UPDATE"));
            }
            JsonUpdateService.scheduleService(getApplicationContext(), 5);
            return null;
        }

        private void donwloadAndSave(String id, String uri) {
            ISettings iSettings = ServiceGenerator.createService(ISettings.class);
            try {
                String dataString= iSettings.getUrl(uri).execute().body().string();
                JsonCacheWriter.writeCache(Aplication.getAppFilepath(getBaseContext())+ "/"+id+".json",dataString);
                Log.d("Cache", id + " updated");
            } catch (IOException e) {
                e.printStackTrace();
                JsonUpdateService.scheduleService(getApplicationContext(), 1);
            }
        }
    }
}
