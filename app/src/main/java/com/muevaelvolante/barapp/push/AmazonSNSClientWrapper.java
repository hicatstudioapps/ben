package com.muevaelvolante.barapp.push;

/*
 * Copyright 2014 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 * http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import com.amazonaws.mobileconnectors.cognito.CognitoSyncManager;
import com.amazonaws.mobileconnectors.cognito.Dataset;
import com.amazonaws.mobileconnectors.cognito.Record;
import com.amazonaws.mobileconnectors.cognito.SyncConflict;
import com.amazonaws.mobileconnectors.cognito.exceptions.DataStorageException;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.CreatePlatformEndpointRequest;
import com.amazonaws.services.sns.model.CreatePlatformEndpointResult;
import com.amazonaws.services.sns.model.GetEndpointAttributesRequest;
import com.amazonaws.services.sns.model.GetEndpointAttributesResult;
import com.amazonaws.services.sns.model.InvalidParameterException;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.NotFoundException;
import com.amazonaws.services.sns.model.SetEndpointAttributesRequest;
import com.amazonaws.services.sns.model.SubscribeResult;
import com.google.android.gms.nearby.messages.internal.SubscribeRequest;
import com.muevaelvolante.Aplication;
import com.muevaelvolante.barapp.commons.Constants;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
//import com.amazonaws.sns.samples.tools.SampleMessageGenerator.Platform;

public class AmazonSNSClientWrapper {

	private final AmazonSNS snsClient;
	private final SharedPreferences pf;
	private String plattaformARN="arn:aws:sns:us-east-1:594219141893:app/GCM/bar_MOBILEHUB_194564674";

	private String newsArn="arn:aws:sns:us-east-1:594219141893:BAR-NEWS";

	private String racingArn="arn:aws:sns:us-east-1:594219141893:BAR-RACING";

	private String endPointARN;
	private boolean updateNeeded;
	private boolean createNeeded;
	Context context;
	Dataset dataSet;
	public AmazonSNSClientWrapper(AmazonSNS client, Context context) {
		this.snsClient = client;
		pf= context.getSharedPreferences("GCM",Context.MODE_PRIVATE);
		this.context=context;
		CognitoSyncManager syncClient= new CognitoSyncManager(context, Regions.EU_WEST_1,
				Aplication.cognitoClientManager.getCredentials());
		dataSet = syncClient.openOrCreateDataset("profile");
	}

	private CreatePlatformEndpointResult createPlatformEndpoint(
			String customData, String platformToken) {
		CreatePlatformEndpointRequest platformEndpointRequest = new CreatePlatformEndpointRequest();
		platformEndpointRequest.setCustomUserData(customData);
		String token = platformToken;
		platformEndpointRequest.setToken(token);
		platformEndpointRequest.setPlatformApplicationArn(plattaformARN);
		return snsClient.createPlatformEndpoint(platformEndpointRequest);
	}

	public void demoNotification(String platformToken) {

		String token = pf.getString("token","");
		endPointARN = pf.getString("arn","");
		Log.d("ARN",endPointARN);
		//si no tengo un arn, lo registro
		if (endPointARN == null || TextUtils.isEmpty(endPointARN)) {
			createARN(platformToken);
		}
		try {
			GetEndpointAttributesRequest geaReq = new GetEndpointAttributesRequest()
							.withEndpointArn(endPointARN);
			GetEndpointAttributesResult geaRes = snsClient.getEndpointAttributes(geaReq);

			updateNeeded = !geaRes.getAttributes().get("Token").equals(platformToken)
					|| !geaRes.getAttributes().get("Enabled").equalsIgnoreCase("true");

		} catch (NotFoundException nfe) {
			// We had a stored ARN, but the platform endpoint associated with it
			// disappeared. Recreate it.
			createNeeded = true;
		}
		if (createNeeded) {
			createARN(platformToken);
		}

		System.out.println("updateNeeded = " + updateNeeded);

		if (updateNeeded) {
			// The platform endpoint is out of sync with the current data;
			// update the token and enable it.
			System.out.println("Updating platform endpoint " + endPointARN);
			Map attribs = new HashMap();
			attribs.put("Token", platformToken);
			attribs.put("Enabled", "true");
			SetEndpointAttributesRequest saeReq =
					new SetEndpointAttributesRequest()
							.withEndpointArn(endPointARN)
							.withAttributes(attribs);
			snsClient.setEndpointAttributes(saeReq);
			pf.edit().putString("token",platformToken).apply();
		}

		//here handle chanel subscription
		try{
		//start whith news
		String newsAlert= dataSet.get(Constants.NOTIFICATION_NEWS);
		String newsSubsCriptionArn= pf.getString(Constants.NOTIFICATION_NEWS_ARN,"");
		if(newsAlert.equals("0") && !TextUtils.isEmpty(newsSubsCriptionArn)){
			try {
				Log.d("Quitando"," subscripcion de news.....");
				snsClient.unsubscribe(newsSubsCriptionArn);
				pf.edit().putString(Constants.NOTIFICATION_NEWS_ARN,"").apply();
				Log.d(""," subscripcion eliminada.....");
			}catch (Exception ex){
				Log.d("error","esta pinga no pincha");
				ex.printStackTrace();
			}
			
		}
		else if(newsAlert.equals("1")) {
			com.amazonaws.services.sns.model.SubscribeRequest subscribeRequest = new com.amazonaws.services.sns.model.SubscribeRequest
					(newsArn, "application", endPointARN);
			SubscribeResult subscribeResult = snsClient.subscribe(subscribeRequest);
			String subscriptionArn = subscribeResult.getSubscriptionArn();
			if(subscriptionArn != null && !TextUtils.isEmpty(subscriptionArn)){
				pf.edit().putString(Constants.NOTIFICATION_NEWS_ARN,subscriptionArn).apply();
				Log.d("News chanel",subscriptionArn);
			}
		}
			//now with racing
			String racingAlert= dataSet.get(Constants.NOTIFICATION_RACING_ALERT);
			String racingSubsCriptionArn= pf.getString(Constants.NOTIFICATION_RACING_ARN,"");
			if(racingAlert.equals("0") && !TextUtils.isEmpty(racingSubsCriptionArn)) {
				try {
					snsClient.unsubscribe(racingSubsCriptionArn);
					pf.edit().putString(Constants.NOTIFICATION_RACING_ARN, "").apply();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
			else if(racingAlert.equals("1")) {
				com.amazonaws.services.sns.model.SubscribeRequest subscribeRequest = new com.amazonaws.services.sns.model.SubscribeRequest
						(racingArn, "application", endPointARN);
				SubscribeResult subscribeResult = snsClient.subscribe(subscribeRequest);
				String subscriptionArn = subscribeResult.getSubscriptionArn();
				if(subscriptionArn != null && !TextUtils.isEmpty(subscriptionArn)){
					pf.edit().putString(Constants.NOTIFICATION_RACING_ARN,subscriptionArn).apply();
					Log.d("Racing chanel",subscriptionArn);
				}
			}
			//guardamos las preferencia en cognito
			dataSet.synchronize(new Dataset.SyncCallback() {
				@Override
				public void onSuccess(Dataset dataset, List<Record> list) {

				}

				@Override
				public boolean onConflict(Dataset dataset, List<SyncConflict> list) {
					return false;
				}

				@Override
				public boolean onDatasetDeleted(Dataset dataset, String s) {
					return false;
				}

				@Override
				public boolean onDatasetsMerged(Dataset dataset, List<String> list) {
					return false;
				}

				@Override
				public void onFailure(DataStorageException e) {

				}
			});
		}catch (Exception ex){
			ex.printStackTrace();
		}
	}

	void createARN(String platformToken){
		CreatePlatformEndpointResult platformEndpointResult = createPlatformEndpoint(
				"CustomData - Useful to store endpoint specific data",
				platformToken);
		try {
			endPointARN = platformEndpointResult.getEndpointArn();
			Log.d("ARN",endPointARN);
		}catch (InvalidParameterException ipe){
			String message = ipe.getErrorMessage();
			System.out.println("Exception message: " + message);
			Pattern p = Pattern
					.compile(".*Endpoint (arn:aws:sns[^ ]+) already exists " +
							"with the same token.*");
			Matcher m = p.matcher(message);
			if (m.matches()) {
				// The platform endpoint already exists for this token, but with
				// additional custom data that
				// createEndpoint doesn't want to overwrite. Just use the
				// existing platform endpoint.
				endPointARN = m.group(1);
			} else {
				// Rethrow the exception, the input is actually bad.
				throw ipe;
			}
		}
		pf.edit().putString("token",platformToken).apply();
		pf.edit().putString("arn",endPointARN).apply();
		System.out.println(platformEndpointResult);
	}
}
