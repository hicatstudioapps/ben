package com.muevaelvolante.barapp.push;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by CQ on 11/07/2016.
 */
public class TestReciver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context,"Entro un push",Toast.LENGTH_SHORT).show();
    }
}
