package com.muevaelvolante.barapp.push;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.widget.Toast;

import com.amazonaws.com.google.gson.Gson;
import com.muevaelvolante.barapp.model.push.Push;

public class ExternalReceiver extends WakefulBroadcastReceiver {

    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context,"Llego un push",Toast.LENGTH_LONG).show();
        if(intent!=null){
            Intent noty= new Intent(context,NotifyIntentService.class);
            noty.putExtras(intent.getExtras());
            startWakefulService(context,noty);
          }
    }
}

