package com.muevaelvolante.barapp.push;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.amazonaws.com.google.gson.Gson;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.activity.SplashActivity;
import com.muevaelvolante.barapp.model.push.Push;

/**
 * Created by CQ on 09/05/2016.
 */
@SuppressWarnings("WrongConstant")
public class NotifyIntentService extends IntentService {

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public NotifyIntentService() {
        super("gjgjfghf");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            Bundle bundle = intent.getExtras();

            for (String key : bundle.keySet()) {
                Object value = bundle.get(key);
                Log.d("values", String.format("%s %s (%s)", key,
                        value.toString(), value.getClass().getName()));
            }
            String json = (String) intent.getExtras().get("default");
            String json1 = (String) intent.getExtras().get("GCM");
            //Push push = new Gson().fromJson(json, Push.class);
            Bundle actionToPerform = new Bundle();
            String action= bundle.getString("action");
            String tittle= bundle.getString("title");
            String body= bundle.getString("body");
            //action to perform
            if (action.equalsIgnoreCase("results")){
                actionToPerform.putString("section", "results");
                //envio el broad cast
                Intent racingBroadCast = new Intent("RACING");
                Bundle data = new Bundle();
                data.putString("tittle", tittle);
                data.putString("msg", body);
                data.putString("action", action);
                racingBroadCast.putExtras(data);
                LocalBroadcastManager.getInstance(this).sendBroadcast(racingBroadCast);
         }
            else
                actionToPerform.putString("section", "home");
            Intent newIntent = new Intent();
            newIntent.setClass(this, SplashActivity.class);
            newIntent.putExtras(actionToPerform);
            newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            postNotification(tittle, body, newIntent, this);
        }catch (Exception ex){
        }
    }

    private void postNotification(String title, String msg, Intent intentAction, NotifyIntentService notifyIntentService) {
        final NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        final PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intentAction, Notification.DEFAULT_LIGHTS | Notification.FLAG_AUTO_CANCEL);
        Uri sound= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        long[] pattern = new long[] { 500, 500, 1000, 1000, 500, 500 };
        final Notification notification = new NotificationCompat.Builder(this).setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(msg)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .setSound(sound)
                .setVibrate(pattern)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .getNotification();
        mNotificationManager.notify(23, notification);
        PowerManager pm = (PowerManager)this.getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isScreenOn();
        if(isScreenOn==false)
        {
            PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK |PowerManager.ACQUIRE_CAUSES_WAKEUP |PowerManager.ON_AFTER_RELEASE,"MyLock");
            wl.acquire(10000);
            PowerManager.WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,"MyCpuLock");
            wl_cpu.acquire(3000);
        }
    }
}
