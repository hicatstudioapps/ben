package com.muevaelvolante.barapp.push;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.com.google.gson.Gson;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.activity.SplashActivity;
import com.muevaelvolante.barapp.model.push.Push;

/*
 * This service is designed to run in the background and receive messages from gcm. If the app is in the foreground
 * when a message is received, it will immediately be posted. If the app is not in the foreground, the message will be saved
 * and a notification is posted to the NotificationManager.
 */
@SuppressWarnings("WrongConstant")
public class MessageReceivingService extends AsyncTask<Void,Void,Void>{
    private GoogleCloudMessaging gcm;
    public static SharedPreferences savedValues;
    private AmazonSNSClient sns;
    Context context;

    public MessageReceivingService(Context context) {
        this.context = context;
        gcm = GoogleCloudMessaging.getInstance(this.context);
        sns = new AmazonSNSClient(new BasicAWSCredentials( "AKIAI7YFUREI5KW6N6GA", "qHiDc9EQWcQ+7I5NroB7gZTv92dVcFs/DM7aUzgQ" ),
                new ClientConfiguration().withMaxConnections(100)
                        .withConnectionTimeout(120 * 1000)
                        .withSocketTimeout(10000)
                        .withMaxErrorRetry(15));
        sns.setRegion(Region.getRegion(Regions.US_EAST_1));
    }

    protected static void postNotification(String tittle, String msg,Intent intentAction, Context context){
        final NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        final PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intentAction, Notification.DEFAULT_LIGHTS | Notification.FLAG_AUTO_CANCEL);
        Uri sound= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        long[] pattern = new long[] { 500, 500, 1000, 1000, 500, 500 };
        final Notification notification = new NotificationCompat.Builder(context).setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(tittle)
                .setContentText(msg)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .setSound(sound)
                .setVibrate(pattern)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .getNotification();
        mNotificationManager.notify(23, notification);
        PowerManager pm = (PowerManager)context.getSystemService(Context.POWER_SERVICE);

        boolean isScreenOn = pm.isScreenOn();

       if(isScreenOn==false)
        {
            PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK |PowerManager.ACQUIRE_CAUSES_WAKEUP |PowerManager.ON_AFTER_RELEASE,"MyLock");
            wl.acquire(10000);
            PowerManager.WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,"MyCpuLock");
            wl_cpu.acquire(3000);
        }
    }

    private void register(final Context context) {
                String token;
                try {
                    token = gcm.register("39673393529");
                    SharedPreferences snspf= context.getSharedPreferences("sns",context.MODE_PRIVATE);
                    if(TextUtils.isEmpty(snspf.getString("token",""))){
                        snspf.edit().putString("token",token).commit();
                    }
                    Log.i("registrationId", token);
                    AmazonSNSClientWrapper wrapper= new AmazonSNSClientWrapper(sns,context);
                    wrapper.demoNotification(token);
                } 
                catch (Exception e) {
                    Log.i("Registration Error", e.getMessage());
                }
                    }



    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    protected Void doInBackground(Void... params) {
        register(context);
        return null;
    }
}