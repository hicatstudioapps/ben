
package com.muevaelvolante.barapp.model.setting;

//
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//
public class PushNotification {

    @SerializedName("Name")
    @Expose
    private String Name;
    @SerializedName("Channel")
    @Expose
    private String Channel;

    /**
     * 
     * @return
     *     The Name
     */
    public String getName() {
        return Name;
    }

    /**
     * 
     * @param Name
     *     The Name
     */
    public void setName(String Name) {
        this.Name = Name;
    }

    /**
     * 
     * @return
     *     The Channel
     */
    public String getChannel() {
        return Channel;
    }

    /**
     * 
     * @param Channel
     *     The Channel
     */
    public void setChannel(String Channel) {
        this.Channel = Channel;
    }

}
