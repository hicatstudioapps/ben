
package com.muevaelvolante.barapp.model.results;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Competitor_ {

    @SerializedName("index")
    @Expose
    private String index;
    @SerializedName("competitorid")
    @Expose
    private String competitorid;
    @SerializedName("rank")
    @Expose
    private String rank;
    @SerializedName("points")
    @Expose
    private String points;
    @SerializedName("status")
    @Expose
    private String status;

    /**
     * 
     * @return
     *     The index
     */
    public String getIndex() {
        return index;
    }

    /**
     * 
     * @param index
     *     The index
     */
    public void setIndex(String index) {
        this.index = index;
    }

    /**
     * 
     * @return
     *     The competitorid
     */
    public String getCompetitorid() {
        return competitorid;
    }

    /**
     * 
     * @param competitorid
     *     The competitorid
     */
    public void setCompetitorid(String competitorid) {
        this.competitorid = competitorid;
    }

    /**
     * 
     * @return
     *     The rank
     */
    public String getRank() {
        return rank;
    }

    /**
     * 
     * @param rank
     *     The rank
     */
    public void setRank(String rank) {
        this.rank = rank;
    }

    /**
     * 
     * @return
     *     The points
     */
    public String getPoints() {
        return points;
    }

    /**
     * 
     * @param points
     *     The points
     */
    public void setPoints(String points) {
        this.points = points;
    }

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

}
