package com.muevaelvolante.barapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by CQ on 04/04/2016.
 */
public class FaceBookImage {

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @SerializedName("is_silhouette")
    @Expose
    private String type;
    @SerializedName("url")
    @Expose
    private String url;
}
