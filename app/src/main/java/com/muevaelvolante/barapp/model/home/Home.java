
package com.muevaelvolante.barapp.model.home;

import java.util.ArrayList;
import java.util.List;
//import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.muevaelvolante.barapp.model.social.Social_;

//@Generated("org.jsonschema2pojo")
public class Home {

    @SerializedName("home-highlights")
    @Expose
    private List<HomeHighlight> homeHighlights = new ArrayList<HomeHighlight>();
    @SerializedName("home-news")
    @Expose
    private List<HomeNews> homeNews = new ArrayList<HomeNews>();
    @SerializedName("videos")
    @Expose
    private List<Video> videos = new ArrayList<Video>();
    @SerializedName("galleries")
    @Expose
    private List<Gallery> galleries = new ArrayList<Gallery>();
    @SerializedName("home-social")
    @Expose
    private List<Social_> social = new ArrayList<Social_>();

    public List<Social_> getSocial() {
        return social;
    }

    public void setSocial(List<Social_> social) {
        this.social = social;
    }

    /**
     * 
     * @return
     *     The homeHighlights
     */
    public List<HomeHighlight> getHomeHighlights() {
        return homeHighlights;
    }

    /**
     * 
     * @param homeHighlights
     *     The home-highlights
     */
    public void setHomeHighlights(List<HomeHighlight> homeHighlights) {
        this.homeHighlights = homeHighlights;
    }

    /**
     * 
     * @return
     *     The homeNews
     */
    public List<HomeNews> getHomeNews() {
        return homeNews;
    }

    /**
     * 
     * @param homeNews
     *     The home-news
     */
    public void setHomeNews(List<HomeNews> homeNews) {
        this.homeNews = homeNews;
    }

    /**
     * 
     * @return
     *     The videos
     */
    public List<Video> getVideos() {
        return videos;
    }

    /**
     * 
     * @param videos
     *     The videos
     */
    public void setVideos(List<Video> videos) {
        this.videos = videos;
    }

    /**
     * 
     * @return
     *     The galleries
     */
    public List<Gallery> getGalleries() {
        return galleries;
    }

    /**
     * 
     * @param galleries
     *     The galleries
     */
    public void setGalleries(List<Gallery> galleries) {
        this.galleries = galleries;
    }

}
