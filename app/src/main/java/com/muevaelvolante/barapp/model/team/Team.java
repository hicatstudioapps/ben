
package com.muevaelvolante.barapp.model.team;

import java.util.ArrayList;
import java.util.List;
//
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//
public class Team {

    @SerializedName("sailing")
    @Expose
    private List<Sailing> sailing = new ArrayList<Sailing>();

    /**
     * 
     * @return
     *     The sailing
     */
    public List<Sailing> getSailing() {
        return sailing;
    }

    /**
     * 
     * @param sailing
     *     The sailing
     */
    public void setSailing(List<Sailing> sailing) {
        this.sailing = sailing;
    }

}
