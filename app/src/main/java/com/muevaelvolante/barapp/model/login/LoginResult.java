package com.muevaelvolante.barapp.model.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by CQ on 11/07/2016.
 */
public class LoginResult {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("userData")
    @Expose
    private UserData userData;
    @SerializedName("StatusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("FunctionError")
    @Expose
    private String functionError;
    @SerializedName("LogResult")
    @Expose
    private String logResult;

    /**
     *
     * @return
     *     The success
     */
    public Boolean getSuccess() {
        return success;
    }

    /**
     *
     * @param success
     *     The success
     */
    public void setSuccess(Boolean success) {
        this.success = success;
    }

    /**
     *
     * @return
     *     The userData
     */
    public UserData getUserData() {
        return userData;
    }

    /**
     *
     * @param userData
     *     The userData
     */
    public void setUserData(UserData userData) {
        this.userData = userData;
    }

    /**
     *
     * @return
     *     The statusCode
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     *
     * @param statusCode
     *     The StatusCode
     */
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    /**
     *
     * @return
     *     The functionError
     */
    public String getFunctionError() {
        return functionError;
    }

    /**
     *
     * @param functionError
     *     The FunctionError
     */
    public void setFunctionError(String functionError) {
        this.functionError = functionError;
    }

    /**
     *
     * @return
     *     The logResult
     */
    public String getLogResult() {
        return logResult;
    }

    /**
     *
     * @param logResult
     *     The LogResult
     */
    public void setLogResult(String logResult) {
        this.logResult = logResult;
    }
}


