
package com.muevaelvolante.barapp.model.news;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
//
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class News implements Serializable{

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("short_text")
    @Expose
    private String shortText;
    @SerializedName("text")
    @Expose
    private List<Object> text = new ArrayList<Object>();
    @SerializedName("locked")
    @Expose
    private String locked;
    @SerializedName("microclip")
    @Expose
    private String microclip;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("media")
    @Expose
    private List<Medium> media = new ArrayList<Medium>();

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The date
     */
    public String getDate() {
        return date;
    }

    /**
     * 
     * @param date
     *     The date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The shortText
     */
    public String getShortText() {
        return shortText;
    }

    /**
     * 
     * @param shortText
     *     The short_text
     */
    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    /**
     * 
     * @return
     *     The text
     */
    public List<Object> getText() {
        return text;
    }

    /**
     * 
     * @param text
     *     The text
     */
    public void setText(List<Object> text) {
        this.text = text;
    }

    /**
     * 
     * @return
     *     The locked
     */
    public String getLocked() {
        return locked;
    }

    /**
     * 
     * @param locked
     *     The locked
     */
    public void setLocked(String locked) {
        this.locked = locked;
    }

    /**
     * 
     * @return
     *     The microclip
     */
    public String getMicroclip() {
        return microclip;
    }

    /**
     * 
     * @param microclip
     *     The microclip
     */
    public void setMicroclip(String microclip) {
        this.microclip = microclip;
    }

    /**
     * 
     * @return
     *     The url
     */
    public String getUrl() {
        return url;
    }

    /**
     * 
     * @param url
     *     The url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 
     * @return
     *     The media
     */
    public List<Medium> getMedia() {
        return media;
    }

    /**
     * 
     * @param media
     *     The media
     */
    public void setMedia(List<Medium> media) {
        this.media = media;
    }

}
