
package com.muevaelvolante.barapp.model.push;

//import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@Generated("org.jsonschema2pojo")
public class Push {


    @SerializedName("alert")
    @Expose
    private Alert alert;
    @SerializedName("sound")
    @Expose
    private String sound;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("action")
    @Expose
    private String action;

    /**
     *
     * @return
     *     The alert
     */
    public Alert getAlert() {
        return alert;
    }

    /**
     *
     * @param alert
     *     The alert
     */
    public void setAlert(Alert alert) {
        this.alert = alert;
    }

    /**
     *
     * @return
     *     The sound
     */
    public String getSound() {
        return sound;
    }

    /**
     *
     * @param sound
     *     The sound
     */
    public void setSound(String sound) {
        this.sound = sound;
    }

    /**
     *
     * @return
     *     The category
     */
    public String getCategory() {
        return category;
    }

    /**
     *
     * @param category
     *     The category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     *
     * @return
     *     The action
     */
    public String getAction() {
        return action;
    }

    /**
     *
     * @param action
     *     The action
     */
    public void setAction(String action) {
        this.action = action;
    }

}
