
package com.muevaelvolante.barapp.model.sustainability;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Pledge {

    @SerializedName("Pledge")
    @Expose
    private String Pledge;
    @SerializedName("Votes")
    @Expose
    private String Votes;

    public String getShare_Message() {
        return Share_Message;
    }

    public void setShare_Message(String share_Message) {
        Share_Message = share_Message;
    }

    @SerializedName("Share-Message")
    @Expose
    private String Share_Message;
    @SerializedName("ID")
    @Expose
    private String ID;

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }

    @SerializedName("Color")
    @Expose
    private String Color;

    /**
     * 
     * @return
     *     The Pledge
     */
    public String getPledge() {
        return Pledge;
    }

    /**
     * 
     * @param Pledge
     *     The Pledge
     */
    public void setPledge(String Pledge) {
        this.Pledge = Pledge;
    }

    /**
     * 
     * @return
     *     The Votes
     */
    public String getVotes() {
        return Votes;
    }

    /**
     * 
     * @param Votes
     *     The Votes
     */
    public void setVotes(String Votes) {
        this.Votes = Votes;
    }

    /**
     * 
     * @return
     *     The ID
     */
    public String getID() {
        return ID;
    }

    /**
     * 
     * @param ID
     *     The ID
     */
    public void setID(String ID) {
        this.ID = ID;
    }

}
