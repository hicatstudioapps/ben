package com.muevaelvolante.barapp.model.setting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by CQ on 21/06/2016.
 */
public class HomeDefault {
    @SerializedName("home_default")
    @Expose
    private String homeDefault;

    public String getHomeDefault() {
        return homeDefault;
    }

    public void setHomeDefault(String homeDefault) {
        this.homeDefault = homeDefault;
    }
}
