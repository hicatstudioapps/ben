
package com.muevaelvolante.barapp.model.results;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Total {

    @SerializedName("competitorid")
    @Expose
    private String competitorid;
    @SerializedName("points")
    @Expose
    private String points;
    @SerializedName("rank")
    @Expose
    private String rank;

    /**
     * 
     * @return
     *     The competitorid
     */
    public String getCompetitorid() {
        return competitorid;
    }

    /**
     * 
     * @param competitorid
     *     The competitorid
     */
    public void setCompetitorid(String competitorid) {
        this.competitorid = competitorid;
    }

    /**
     * 
     * @return
     *     The points
     */
    public String getPoints() {
        return points;
    }

    /**
     * 
     * @param points
     *     The points
     */
    public void setPoints(String points) {
        this.points = points;
    }

    /**
     * 
     * @return
     *     The rank
     */
    public String getRank() {
        return rank;
    }

    /**
     * 
     * @param rank
     *     The rank
     */
    public void setRank(String rank) {
        this.rank = rank;
    }

}
