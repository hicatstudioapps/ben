
package com.muevaelvolante.barapp.model.photos;

import java.util.ArrayList;
import java.util.List;
//import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@Generated("org.jsonschema2pojo")
public class Gallery {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("short_text")
    @Expose
    private String shortText;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("locked")
    @Expose
    private String locked;
    @SerializedName("microclip")
    @Expose
    private String microclip;
    @SerializedName("media")
    @Expose
    private List<Medium> media = new ArrayList<Medium>();

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The date
     */
    public String getDate() {
        return date;
    }

    /**
     * 
     * @param date
     *     The date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The shortText
     */
    public String getShortText() {
        return shortText;
    }

    /**
     * 
     * @param shortText
     *     The short_text
     */
    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    /**
     * 
     * @return
     *     The text
     */
    public String getText() {
        return text;
    }

    /**
     * 
     * @param text
     *     The text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * 
     * @return
     *     The locked
     */
    public String getLocked() {
        return locked;
    }

    /**
     * 
     * @param locked
     *     The locked
     */
    public void setLocked(String locked) {
        this.locked = locked;
    }

    /**
     * 
     * @return
     *     The microclip
     */
    public String getMicroclip() {
        return microclip;
    }

    /**
     * 
     * @param microclip
     *     The microclip
     */
    public void setMicroclip(String microclip) {
        this.microclip = microclip;
    }

    /**
     * 
     * @return
     *     The media
     */
    public List<Medium> getMedia() {
        return media;
    }

    /**
     * 
     * @param media
     *     The media
     */
    public void setMedia(List<Medium> media) {
        this.media = media;
    }

}
