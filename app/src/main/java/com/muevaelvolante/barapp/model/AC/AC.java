
package com.muevaelvolante.barapp.model.AC;

import java.util.ArrayList;
import java.util.List;
//import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@Generated("org.jsonschema2pojo")
public class AC {

    @SerializedName("ac-the-guide")
    @Expose
    private List<AcTheGuide> acTheGuide = new ArrayList<AcTheGuide>();

    /**
     * 
     * @return
     *     The acTheGuide
     */
    public List<AcTheGuide> getAcTheGuide() {
        return acTheGuide;
    }

    /**
     * 
     * @param acTheGuide
     *     The ac-the-guide
     */
    public void setAcTheGuide(List<AcTheGuide> acTheGuide) {
        this.acTheGuide = acTheGuide;
    }

}
