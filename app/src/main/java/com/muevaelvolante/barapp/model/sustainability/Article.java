
package com.muevaelvolante.barapp.model.sustainability;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Article {

    @SerializedName("Section")
    @Expose
    private String Section;
    @SerializedName("Title")
    @Expose
    private String Title;
    @SerializedName("Short_Text")
    @Expose
    private String ShortText;
    @SerializedName("Text")
    @Expose
    private String Text;
    @SerializedName("media")
    @Expose
    private List<Medium_> media = new ArrayList<Medium_>();

    /**
     * 
     * @return
     *     The Section
     */
    public String getSection() {
        return Section;
    }

    /**
     * 
     * @param Section
     *     The Section
     */
    public void setSection(String Section) {
        this.Section = Section;
    }

    /**
     * 
     * @return
     *     The Title
     */
    public String getTitle() {
        return Title;
    }

    /**
     * 
     * @param Title
     *     The Title
     */
    public void setTitle(String Title) {
        this.Title = Title;
    }

    /**
     * 
     * @return
     *     The ShortText
     */
    public String getShortText() {
        return ShortText;
    }

    /**
     * 
     * @param ShortText
     *     The Short_Text
     */
    public void setShortText(String ShortText) {
        this.ShortText = ShortText;
    }

    /**
     * 
     * @return
     *     The Text
     */
    public String getText() {
        return Text;
    }

    /**
     * 
     * @param Text
     *     The Text
     */
    public void setText(String Text) {
        this.Text = Text;
    }

    /**
     * 
     * @return
     *     The media
     */
    public List<Medium_> getMedia() {
        return media;
    }

    /**
     * 
     * @param media
     *     The media
     */
    public void setMedia(List<Medium_> media) {
        this.media = media;
    }

}
