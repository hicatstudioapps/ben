
package com.muevaelvolante.barapp.model.setting;

//
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

//
public class Countdown {

    @SerializedName("Date")
    @Expose
    private String Date;

    public String getType() {

        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    @SerializedName("Type")
    @Expose
    private String Type;
    @SerializedName("Text")
    @Expose
    private String Text;
    @SerializedName("Active")
    @Expose
    private String Active;
    @SerializedName("ButtonText")
    @Expose
    private String ButtonText;
    @SerializedName("URL")
    @Expose
    private String URL;

    public List<Medium> getMedia() {
        return media;
    }

    public void setMedia(List<Medium> media) {
        this.media = media;
    }

    @SerializedName("media")
    @Expose
    private List<Medium> media = new ArrayList<Medium>();


    /**
     * 
     * @return
     *     The Date
     */
    public String getDate() {
        return Date;
    }

    /**
     * 
     * @param Date
     *     The Date
     */
    public void setDate(String Date) {
        this.Date = Date;
    }

    /**
     * 
     * @return
     *     The Text
     */
    public String getText() {
        return Text;
    }

    /**
     * 
     * @param Text
     *     The Text
     */
    public void setText(String Text) {
        this.Text = Text;
    }

    /**
     * 
     * @return
     *     The Active
     */
    public String getActive() {
        return Active;
    }

    /**
     * 
     * @param Active
     *     The Active
     */
    public void setActive(String Active) {
        this.Active = Active;
    }

    /**
     * 
     * @return
     *     The ButtonText
     */
    public String getButtonText() {
        return ButtonText;
    }

    /**
     * 
     * @param ButtonText
     *     The ButtonText
     */
    public void setButtonText(String ButtonText) {
        this.ButtonText = ButtonText;
    }

    /**
     * 
     * @return
     *     The url
     */
    public String getUrl() {
        return URL;
    }

    /**
     * 
     * @param url
     *     The url
     */
    public void setUrl(String url) {
        this.URL = url;
    }

}
