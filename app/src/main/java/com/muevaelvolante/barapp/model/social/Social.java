
package com.muevaelvolante.barapp.model.social;

import java.util.ArrayList;
import java.util.List;
//import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

////@Generated("org.jsonschema2pojo")
public class Social {

    @SerializedName("social-banner")
    @Expose
    private List<SocialBanner> socialBanner = new ArrayList<SocialBanner>();
    @SerializedName("follow-us")
    @Expose
    private List<FollowU> followUs = new ArrayList<FollowU>();
    @SerializedName("photo-share")
    @Expose
    private List<PhotoShare> photoShare = new ArrayList<PhotoShare>();
    @SerializedName("social")
    @Expose
    private List<Social_> social = new ArrayList<Social_>();

    /**
     * 
     * @return
     *     The socialBanner
     */
    public List<SocialBanner> getSocialBanner() {
        return socialBanner;
    }

    /**
     * 
     * @param socialBanner
     *     The social-banner
     */
    public void setSocialBanner(List<SocialBanner> socialBanner) {
        this.socialBanner = socialBanner;
    }

    /**
     * 
     * @return
     *     The followUs
     */
    public List<FollowU> getFollowUs() {
        return followUs;
    }

    /**
     * 
     * @param followUs
     *     The follow-us
     */
    public void setFollowUs(List<FollowU> followUs) {
        this.followUs = followUs;
    }

    /**
     * 
     * @return
     *     The photoShare
     */
    public List<PhotoShare> getPhotoShare() {
        return photoShare;
    }

    /**
     * 
     * @param photoShare
     *     The photo-share
     */
    public void setPhotoShare(List<PhotoShare> photoShare) {
        this.photoShare = photoShare;
    }

    /**
     * 
     * @return
     *     The social
     */
    public List<Social_> getSocial() {
        return social;
    }

    /**
     * 
     * @param social
     *     The social
     */
    public void setSocial(List<Social_> social) {
        this.social = social;
    }

}
