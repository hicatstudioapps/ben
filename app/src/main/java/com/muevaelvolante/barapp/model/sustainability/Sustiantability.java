
package com.muevaelvolante.barapp.model.sustainability;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Sustiantability {

    @SerializedName("topstories")
    @Expose
    private List<Topstory> topstories = new ArrayList<Topstory>();
    @SerializedName("articles")
    @Expose
    private List<Article> articles = new ArrayList<Article>();
    @SerializedName("pledge")
    @Expose
    private List<Pledge> pledge = new ArrayList<Pledge>();
    @SerializedName("sponsors")
    @Expose
    private List<Sponsor> sponsors = new ArrayList<Sponsor>();
    @SerializedName("more-content")
    @Expose
    private List<MoreContent> moreContents = new ArrayList<MoreContent>();

    public List<MoreContent> getMoreContents() {
        return moreContents;
    }

    public void setMoreContents(List<MoreContent> moreContents) {
        this.moreContents = moreContents;
    }

    /**
     * 
     * @return
     *     The topstories
     */
    public List<Topstory> getTopstories() {
        return topstories;
    }

    /**
     * 
     * @param topstories
     *     The topstories
     */
    public void setTopstories(List<Topstory> topstories) {
        this.topstories = topstories;
    }

    /**
     * 
     * @return
     *     The articles
     */
    public List<Article> getArticles() {
        return articles;
    }

    /**
     * 
     * @param articles
     *     The articles
     */
    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    /**
     * 
     * @return
     *     The pledge
     */
    public List<Pledge> getPledge() {
        return pledge;
    }

    /**
     * 
     * @param pledge
     *     The pledge
     */
    public void setPledge(List<Pledge> pledge) {
        this.pledge = pledge;
    }

    /**
     * 
     * @return
     *     The sponsors
     */
    public List<Sponsor> getSponsors() {
        return sponsors;
    }

    /**
     * 
     * @param sponsors
     *     The sponsors
     */
    public void setSponsors(List<Sponsor> sponsors) {
        this.sponsors = sponsors;
    }

}
