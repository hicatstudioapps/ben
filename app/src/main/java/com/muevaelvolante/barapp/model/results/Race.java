
package com.muevaelvolante.barapp.model.results;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Race {

    @SerializedName("raceid")
    @Expose
    private String raceid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("start")
    @Expose
    private String start;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("competitors")
    @Expose
    private List<Competitor_> competitors = new ArrayList<Competitor_>();

    /**
     * 
     * @return
     *     The raceid
     */
    public String getRaceid() {
        return raceid;
    }

    /**
     * 
     * @param raceid
     *     The raceid
     */
    public void setRaceid(String raceid) {
        this.raceid = raceid;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The date
     */
    public String getDate() {
        return date;
    }

    /**
     * 
     * @param date
     *     The date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * 
     * @return
     *     The start
     */
    public String getStart() {
        return start;
    }

    /**
     * 
     * @param start
     *     The start
     */
    public void setStart(String start) {
        this.start = start;
    }

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The competitors
     */
    public List<Competitor_> getCompetitors() {
        return competitors;
    }

    /**
     * 
     * @param competitors
     *     The competitors
     */
    public void setCompetitors(List<Competitor_> competitors) {
        this.competitors = competitors;
    }

}
