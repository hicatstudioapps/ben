
package com.muevaelvolante.barapp.model.results;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Competitor {

    @SerializedName("competitorid")
    @Expose
    private String competitorid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("abbr")
    @Expose
    private String abbr;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("media")
    @Expose
    private String media;

    /**
     * 
     * @return
     *     The competitorid
     */
    public String getCompetitorid() {
        return competitorid;
    }

    /**
     * 
     * @param competitorid
     *     The competitorid
     */
    public void setCompetitorid(String competitorid) {
        this.competitorid = competitorid;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The abbr
     */
    public String getAbbr() {
        return abbr;
    }

    /**
     * 
     * @param abbr
     *     The abbr
     */
    public void setAbbr(String abbr) {
        this.abbr = abbr;
    }

    /**
     * 
     * @return
     *     The country
     */
    public String getCountry() {
        return country;
    }

    /**
     * 
     * @param country
     *     The country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * 
     * @return
     *     The link
     */
    public String getLink() {
        return link;
    }

    /**
     * 
     * @param link
     *     The link
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     * 
     * @return
     *     The media
     */
    public String getMedia() {
        return media;
    }

    /**
     * 
     * @param media
     *     The media
     */
    public void setMedia(String media) {
        this.media = media;
    }

}
