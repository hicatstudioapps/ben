
package com.muevaelvolante.barapp.model.results;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ResultRoot {

    @SerializedName("results-link")
    @Expose
    private List<ResultsLink> resultsLink = new ArrayList<ResultsLink>();
    @SerializedName("results-help")
    @Expose
    private List<ResultsHelp> resultsHelp = new ArrayList<ResultsHelp>();

    public List<ResultsRaceOrder> getResultsRaceOrders() {
        return resultsRaceOrders;
    }

    public void setResultsRaceOrders(List<ResultsRaceOrder> resultsRaceOrders) {
        this.resultsRaceOrders = resultsRaceOrders;
    }

    @SerializedName("results-race-order")
    @Expose
    private List<ResultsRaceOrder> resultsRaceOrders = new ArrayList<ResultsRaceOrder>();

    /**
     * 
     * @return
     *     The resultsLink
     */
    public List<ResultsLink> getResultsLink() {
        return resultsLink;
    }

    /**
     * 
     * @param resultsLink
     *     The results-link
     */
    public void setResultsLink(List<ResultsLink> resultsLink) {
        this.resultsLink = resultsLink;
    }

    /**
     * 
     * @return
     *     The resultsHelp
     */
    public List<ResultsHelp> getResultsHelp() {
        return resultsHelp;
    }

    /**
     * 
     * @param resultsHelp
     *     The results-help
     */
    public void setResultsHelp(List<ResultsHelp> resultsHelp) {
        this.resultsHelp = resultsHelp;
    }

}
