
package com.muevaelvolante.barapp.model.photos;

import java.util.ArrayList;
import java.util.List;
//import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@Generated("org.jsonschema2pojo")
public class Photos {

    @SerializedName("galleries")
    @Expose
    private List<Gallery> galleries = new ArrayList<Gallery>();

    /**
     * 
     * @return
     *     The galleries
     */
    public List<Gallery> getGalleries() {
        return galleries;
    }

    /**
     * 
     * @param galleries
     *     The galleries
     */
    public void setGalleries(List<Gallery> galleries) {
        this.galleries = galleries;
    }

}
