
package com.muevaelvolante.barapp.model.news;

import java.util.ArrayList;
import java.util.List;
//
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class HomeContent {

    @SerializedName("home-news")
    @Expose
    private List<News> homeNews = new ArrayList<News>();
    @SerializedName("videos")
    @Expose
    private List<Video> videos = new ArrayList<Video>();
    @SerializedName("galleries")
    @Expose
    private List<Gallery> galleries = new ArrayList<Gallery>();

    /**
     * 
     * @return
     *     The homeNews
     */
    public List<News> getHomeNews() {
        return homeNews;
    }

    /**
     * 
     * @param homeNews
     *     The home-news
     */
    public void setHomeNews(List<News> homeNews) {
        this.homeNews = homeNews;
    }

    /**
     * 
     * @return
     *     The videos
     */
    public List<Video> getVideos() {
        return videos;
    }

    /**
     * 
     * @param videos
     *     The videos
     */
    public void setVideos(List<Video> videos) {
        this.videos = videos;
    }

    /**
     * 
     * @return
     *     The galleries
     */
    public List<Gallery> getGalleries() {
        return galleries;
    }

    /**
     * 
     * @param galleries
     *     The galleries
     */
    public void setGalleries(List<Gallery> galleries) {
        this.galleries = galleries;
    }

}
