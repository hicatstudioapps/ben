package com.muevaelvolante.barapp.model.results;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by CQ on 30/10/2016.
 */

public class ResultsRaceOrder {
    public String getRace_default_order() {
        return race_default_order;
    }

    public void setRace_default_order(String race_default_order) {
        this.race_default_order = race_default_order;
    }

    @SerializedName("race-default-order")
    @Expose
    private String race_default_order;
}
