package com.muevaelvolante.barapp.model;

import com.muevaelvolante.barapp.model.news.Gallery;
import com.muevaelvolante.barapp.model.news.News;
import com.muevaelvolante.barapp.model.news.Video;
import com.muevaelvolante.barapp.util.Utils;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Created by CQ on 24/03/2016.
 */
public class GenericModel implements Comparable<GenericModel> {

    public Object getNews() {
        return news;
    }

    private Object news;

    public GenericModel(Object news) {
        this.news = news;
    }

    @Override
    public int compareTo(GenericModel another) {
        String myDate="",parameterDate="";
        long parameterTime;
        long myTime;
        boolean gallery=false;
        boolean galleryP=false;
        if(((GenericModel)another).getNews() instanceof News)
            parameterDate=((News)another.getNews()).getDate();
        if(((GenericModel)another).getNews() instanceof Video)
            parameterDate=((Video)another.getNews()).getDate();
        if(((GenericModel)another).getNews() instanceof Gallery){
            parameterDate=((Gallery)another.getNews()).getDate();
            galleryP=true;
        }
        if(this.news instanceof News)
            myDate=((News)this.news).getDate();
        if(this.news instanceof Video)
            myDate=((Video)this.news).getDate();
        if(this.news instanceof Gallery){
            myDate=((Gallery)this.news).getDate();
            gallery=true;
        }
        if(galleryP){
            DateTimeFormatter dtf= DateTimeFormat.forPattern("yyyy-MM-dd");
             parameterTime= dtf.parseDateTime(parameterDate).getMillis();
        }else
             parameterTime= Utils.convertStringToDate(parameterDate).getMillis();
        if(gallery){
            DateTimeFormatter dtf= DateTimeFormat.forPattern("yyyy-MM-dd");
            myTime= dtf.parseDateTime(myDate).getMillis();
        }
        else
         myTime= Utils.convertStringToDate(myDate).getMillis();

        if(parameterTime>=myTime)
         return    1;
        return -1;
    }
}
