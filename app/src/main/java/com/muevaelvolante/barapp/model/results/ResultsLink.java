
package com.muevaelvolante.barapp.model.results;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ResultsLink {

    @SerializedName("URL")
    @Expose
    private String uRL;

    /**
     * 
     * @return
     *     The uRL
     */
    public String getURL() {
        return uRL;
    }

    /**
     * 
     * @param uRL
     *     The URL
     */
    public void setURL(String uRL) {
        this.uRL = uRL;
    }

}
