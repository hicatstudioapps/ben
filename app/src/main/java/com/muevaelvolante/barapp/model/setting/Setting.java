
package com.muevaelvolante.barapp.model.setting;

import java.util.ArrayList;
import java.util.List;
//
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//
public class Setting {

//    @SerializedName("countdown")
//    @Expose
    private List<Countdown> countdown = new ArrayList<Countdown>();
    @SerializedName("menu")
    @Expose
    private List<Menu> menu = new ArrayList<Menu>();
    @SerializedName("push-notifications")
    @Expose
    private List<PushNotification> pushNotifications = new ArrayList<PushNotification>();
    @SerializedName("newsletter")
    @Expose
    private List<Newsletter> newsletter = new ArrayList<Newsletter>();
    @SerializedName("login")
    @Expose
    private List<Login> loginOptionses = new ArrayList<Login>();
    @SerializedName("home-default")
    @Expose
    private List<HomeDefault> homeDefault = new ArrayList<HomeDefault>();

    public List<HomeDefault> getHomeDefault() {
        return homeDefault;
    }

    public void setHomeDefault(List<HomeDefault> homeDefault) {
        this.homeDefault = homeDefault;
    }

    /**
     * 
     * @return
     *     The countdown
     */
    public List<Countdown> getCountdown() {
        return countdown;
    }

    /**
     * 
     * @param countdown
     *     The countdown
     */
    public void setCountdown(List<Countdown> countdown) {
        this.countdown = countdown;
    }

    /**
     * 
     * @return
     *     The menu
     */
    public List<Menu> getMenu() {
        return menu;
    }

    /**
     * 
     * @param menu
     *     The menu
     */
    public void setMenu(List<Menu> menu) {
        this.menu = menu;
    }

    /**
     * 
     * @return
     *     The pushNotifications
     */
    public List<PushNotification> getPushNotifications() {
        return pushNotifications;
    }

    /**
     * 
     * @param pushNotifications
     *     The push-notifications
     */
    public void setPushNotifications(List<PushNotification> pushNotifications) {
        this.pushNotifications = pushNotifications;
    }

    /**
     * 
     * @return
     *     The newsletter
     */
    public List<Newsletter> getNewsletter() {
        return newsletter;
    }

    /**
     * 
     * @param newsletter
     *     The newsletter
     */
    public void setNewsletter(List<Newsletter> newsletter) {
        this.newsletter = newsletter;
    }

    public List<Login> getLoginOptionses() {
        return loginOptionses;
    }

    public void setLoginOptionses(List<Login> loginOptionses) {
        this.loginOptionses = loginOptionses;
    }
}
