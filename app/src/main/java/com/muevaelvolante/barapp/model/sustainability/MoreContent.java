package com.muevaelvolante.barapp.model.sustainability;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CQ on 22/06/2016.
 */
public class MoreContent {
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public String getShort_text() {
        return short_text;
    }

    public void setShort_text(String short_text) {
        this.short_text = short_text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public List<Object> getMedia() {
        return media;
    }

    public void setMedia(List<Object> media) {
        this.media = media;
    }

    @SerializedName("Type")

    @Expose
    private String type;
    @SerializedName("Title")
    @Expose
    private String tittle;
    @SerializedName("Short_text")
    @Expose
    private String short_text;
    @SerializedName("Text")
    @Expose
    private String text;
    @SerializedName("Name")
    @Expose
    private String Name;
    @SerializedName("media")
    @Expose
    private List<Object> media = new ArrayList<Object>();
}
