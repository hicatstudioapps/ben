
package com.muevaelvolante.barapp.model.setting;

import java.util.ArrayList;
import java.util.List;
//
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//
public class Menu {

    @SerializedName("Section")
    @Expose
    private String Section;
    @SerializedName("ID")
    @Expose
    private String ID;
    @SerializedName("Text")
    @Expose
    private String Text;
    @SerializedName("URL")
    @Expose
    private String URL;
    @SerializedName("MainMenuPhone")
    @Expose
    private String MainMenuPhone;
    @SerializedName("MainMenuTablet")
    @Expose
    private String MainMenuTablet;
    @SerializedName("media")
    @Expose
    private List<Medium_> media = new ArrayList<Medium_>();

    /**
     * 
     * @return
     *     The Section
     */
    public String getSection() {
        return Section;
    }

    /**
     * 
     * @param Section
     *     The Section
     */
    public void setSection(String Section) {
        this.Section = Section;
    }

    /**
     * 
     * @return
     *     The Text
     */
    public String getText() {
        return Text;
    }

    /**
     * 
     * @param Text
     *     The Text
     */
    public void setText(String Text) {
        this.Text = Text;
    }

    /**
     * 
     * @return
     *     The URL
     */
    public String getURL() {
        return URL;
    }

    /**
     * 
     * @param URL
     *     The URL
     */
    public void setURL(String URL) {
        this.URL = URL;
    }

    /**
     * 
     * @return
     *     The MainMenuPhone
     */
    public String getMainMenuPhone() {
        return MainMenuPhone;
    }

    /**
     * 
     * @param MainMenuPhone
     *     The MainMenuPhone
     */
    public void setMainMenuPhone(String MainMenuPhone) {
        this.MainMenuPhone = MainMenuPhone;
    }

    /**
     * 
     * @return
     *     The MainMenuTablet
     */
    public String getMainMenuTablet() {
        return MainMenuTablet;
    }

    /**
     * 
     * @param MainMenuTablet
     *     The MainMenuTablet
     */
    public void setMainMenuTablet(String MainMenuTablet) {
        this.MainMenuTablet = MainMenuTablet;
    }

    /**
     * 
     * @return
     *     The media
     */
    public List<Medium_> getMedia() {
        return media;
    }

    /**
     * 
     * @param media
     *     The media
     */
    public void setMedia(List<Medium_> media) {
        this.media = media;
    }


    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }
}
