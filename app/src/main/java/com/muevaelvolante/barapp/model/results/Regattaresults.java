
package com.muevaelvolante.barapp.model.results;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Regattaresults {

    @SerializedName("regattaid")
    @Expose
    private String regattaid;
    @SerializedName("races")
    @Expose
    private List<Race> races = new ArrayList<Race>();
    @SerializedName("total")
    @Expose
    private List<Total> total = new ArrayList<Total>();

    /**
     * 
     * @return
     *     The regattaid
     */
    public String getRegattaid() {
        return regattaid;
    }

    /**
     * 
     * @param regattaid
     *     The regattaid
     */
    public void setRegattaid(String regattaid) {
        this.regattaid = regattaid;
    }

    /**
     * 
     * @return
     *     The races
     */
    public List<Race> getRaces() {
        return races;
    }

    /**
     * 
     * @param races
     *     The races
     */
    public void setRaces(List<Race> races) {
        this.races = races;
    }

    /**
     * 
     * @return
     *     The total
     */
    public List<Total> getTotal() {
        return total;
    }

    /**
     * 
     * @param total
     *     The total
     */
    public void setTotal(List<Total> total) {
        this.total = total;
    }

}
