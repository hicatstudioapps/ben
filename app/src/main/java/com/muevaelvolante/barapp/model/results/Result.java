
package com.muevaelvolante.barapp.model.results;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


public class Result implements Comparable<Result>{

    @SerializedName("regattaid")
    @Expose
    private String regattaid;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("competitors")
    @Expose
    private List<Competitor> competitors = new ArrayList<Competitor>();
    @SerializedName("regattaresults")
    @Expose
    private Regattaresults regattaresults;

    /**
     * 
     * @return
     *     The regattaid
     */
    public String getRegattaid() {
        return regattaid;
    }

    /**
     * 
     * @param regattaid
     *     The regattaid
     */
    public void setRegattaid(String regattaid) {
        this.regattaid = regattaid;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The date
     */
    public String getDate() {
        return date;
    }

    /**
     * 
     * @param date
     *     The date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * 
     * @return
     *     The competitors
     */
    public List<Competitor> getCompetitors() {
        return competitors;
    }

    /**
     * 
     * @param competitors
     *     The competitors
     */
    public void setCompetitors(List<Competitor> competitors) {
        this.competitors = competitors;
    }

    /**
     * 
     * @return
     *     The regattaresults
     */
    public Regattaresults getRegattaresults() {
        return regattaresults;
    }

    /**
     * 
     * @param regattaresults
     *     The regattaresults
     */

    public void setRegattaresults(Regattaresults regattaresults) {
        this.regattaresults = regattaresults;
    }

    @Override
    public int compareTo(Result another) {
        DateTimeFormatter fm= DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTime dta= fm.parseDateTime(another.getDate());
        DateTime dt= fm.parseDateTime(this.getDate());
        if(dt.getMillis()>=dta.getMillis())
        return 1;
        else return -1;
    }
}
