package com.muevaelvolante.barapp.model.setting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by CQ on 15/04/2016.
 */
public class Login {
    @SerializedName("login_first_time")
    @Expose
    private String login_first_time;

    public String getLogin_next_days() {
        return login_next_days;
    }

    public void setLogin_next_days(String login_next_days) {
        this.login_next_days = login_next_days;
    }

    public String getLogin_first_time() {
        return login_first_time;
    }

    public void setLogin_first_time(String login_first_time) {
        this.login_first_time = login_first_time;
    }

    @SerializedName("login_next_days")
    @Expose

    private String login_next_days;
}
