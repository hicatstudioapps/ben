
package com.muevaelvolante.barapp.model.boat;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Boat {

    @SerializedName("boats")
    @Expose
    private List<Boat_> boats = new ArrayList<Boat_>();

    /**
     * 
     * @return
     *     The boats
     */
    public List<Boat_> getBoats() {
        return boats;
    }

    /**
     * 
     * @param boats
     *     The boats
     */
    public void setBoats(List<Boat_> boats) {
        this.boats = boats;
    }

}
