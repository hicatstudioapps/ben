package com.muevaelvolante.barapp.activity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;

import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.fragment.GalleryItem;

import butterknife.Bind;
import butterknife.ButterKnife;

public class GalleryActivity extends AppCompatActivity implements GalleryItem.ToolBarControl {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    com.muevaelvolante.barapp.model.home.Gallery gallery;
    private boolean isToolbarVisible = true;

    public ViewPager getPager() {
        return pager;
    }

    @Bind(R.id.pager)
    ViewPager pager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_gallery);
        ButterKnife.bind(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(" ");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.back));
        gallery = (com.muevaelvolante.barapp.model.home.Gallery) getIntent().getExtras().getSerializable("gal");
        pager.setAdapter(new PageAdaper(getSupportFragmentManager()));
        pager.setOffscreenPageLimit(10);
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
               // hideTooBar();
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if((Configuration.ORIENTATION_LANDSCAPE==getResources().getConfiguration().orientation))
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                hideTooBar();
            }
        }, 1000);
    }

    private void hideTooBar() {
        if(!isToolbarVisible)
            return;
        toolbar.setAnimation(AnimationUtils.loadAnimation(GalleryActivity.this, R.anim.slide_top_out));
        toolbar.setVisibility(View.GONE);
        isToolbarVisible = false;
    }

    private void showTooBar() {
        if(isToolbarVisible)
            return;
        toolbar.setAnimation(AnimationUtils.loadAnimation(GalleryActivity.this, R.anim.slide_top_in));
        toolbar.setVisibility(View.VISIBLE);
        isToolbarVisible = true;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                hideTooBar();
            }
        }, 3000);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            super.onBackPressed();
        ;
        return true;
    }

    @Override
    public void toogleToolBar() {
        if (isToolbarVisible)
            hideTooBar();
        else
            showTooBar();
    }


    class PageAdaper extends FragmentStatePagerAdapter {

        public PageAdaper(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                default:
                    return GalleryItem.newInstance(gallery.getMedia().get(position).getMed(), gallery.getMedia().size(), position);
            }
        }

        @Override
        public int getCount() {
            return gallery.getMedia().size();
        }
    }


}
