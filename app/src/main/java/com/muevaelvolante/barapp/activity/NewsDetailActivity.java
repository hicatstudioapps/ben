package com.muevaelvolante.barapp.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.internal.LinkedHashTreeMap;
import com.google.gson.internal.LinkedTreeMap;
import com.muevaelvolante.Aplication;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.model.home.HomeNews;
import com.muevaelvolante.barapp.model.news.News;
import com.muevaelvolante.barapp.util.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.io.File;
import java.io.FileOutputStream;
import java.util.LinkedHashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class NewsDetailActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.date)
    TextView date;
    @Bind(R.id.tittle)
    TextView tittle;
    HomeNews news;
    @Bind(R.id.imageView14)
    ImageView picture;
    @Bind(R.id.imageView16)
    View share;
    @Bind(R.id.container)
    LinearLayout container;

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home,menu);
        return true;
    }

    Uri photoURI=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle=getIntent().getExtras();
        news=(HomeNews) bundle.getSerializable("news");
        setContentView(R.layout.content_news_detail);
        ButterKnife.bind(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(" ");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.back));
        if(news.getMedia().size()>0){
            ImageLoader.getInstance().displayImage(((LinkedHashMap<String, String>) news.getMedia().get(0)).get("med"), picture, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String s, View view) {

                }

                @Override
                public void onLoadingFailed(String s, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                    float proportion= (float)bitmap.getWidth()/bitmap.getHeight();
                    int alto= (int) (Utils.getScreenWidth(NewsDetailActivity.this)/proportion);
                    RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams) picture.getLayoutParams();
                    lp.height=alto;
                    lp.width=Utils.getScreenWidth(NewsDetailActivity.this);
                    picture.setLayoutParams(lp);
                    //save temp bitMap for share
                    String file_path = Environment.getExternalStorageDirectory().getAbsolutePath();
                    File dir = new File(file_path);
                    File file = new File(dir, "123.png");
                    FileOutputStream fOut;
                    try {
                        fOut = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.PNG, 85, fOut);
                        fOut.flush();
                        fOut.close();
                        photoURI= Uri.fromFile(file);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onLoadingCancelled(String s, View view) {

                }
            });
        }
        else
        picture.setVisibility(View.GONE);
        share.setVisibility(View.VISIBLE);
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                share();
            }
        });
        tittle.setText(news.getTitle());
        if(news.getDate() != null)
        date.setText(Utils.getDateString(Utils.convertStringToDate(news.getDate())));
        else{
            date.setVisibility(View.GONE);

        }
        List<Object> texts=news.getText();
        for(int i=0; i< texts.size();i++){
            Object item=texts.get(i);
            if(item instanceof  String){
                View string= getLayoutInflater().inflate(R.layout.news_text,null);
                ((TextView)string.findViewById(R.id.text)).setText(Html.fromHtml(String.valueOf(item)));
                container.addView(string);
                continue;
            }
            if(item instanceof LinkedHashMap){
                LinkedHashMap<String,String> map= (LinkedHashMap<String, String>) item;
                if(map.size()==3){ //es una imagen
                    View imagen= getLayoutInflater().inflate(R.layout.news_image,null);
                    final ImageView image= (ImageView) imagen.findViewById(R.id.image);
                    Log.d("image URL", map.get("med"));
                    ImageLoader.getInstance().displayImage(map.get("med"), image, new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String s, View view) {

                        }

                        @Override
                        public void onLoadingFailed(String s, View view, FailReason failReason) {

                        }

                        @Override
                        public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                            int bmW=bitmap.getWidth();
                            int bmH=bitmap.getHeight();
                            float proportion= (float)bitmap.getWidth()/bitmap.getHeight();
                            int alto= (int) (Utils.getScreenWidth(NewsDetailActivity.this)/proportion);
                            RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams) image.getLayoutParams();
                            lp.height=alto;
                            lp.width=Utils.getScreenWidth(NewsDetailActivity.this);
                            image.setLayoutParams(lp);
                        }

                        @Override
                        public void onLoadingCancelled(String s, View view) {

                        }
                    });
                    container.addView(imagen);
                    continue;
                }
                if(map.size()==4) {//es un video
                 final View video= getLayoutInflater().inflate(R.layout.news_video,null);
                    final ImageView video_image=((ImageView)video.findViewById(R.id.video_image));
                    final String video_url= map.get("video");
                    Log.d("video_url", video_url);
                    ImageLoader.getInstance().displayImage(map.get("med"),video_image, new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String s, View view) {

                        }

                        @Override
                        public void onLoadingFailed(String s, View view, FailReason failReason) {

                        }
                        @Override
                        public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                            float proportion= (float)bitmap.getWidth()/bitmap.getHeight();
                            int alto= (int) (Utils.getScreenWidth(NewsDetailActivity.this)/proportion);
                            RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams) video_image.getLayoutParams();
                            lp.height=alto;
                            lp.width=Utils.getScreenWidth(NewsDetailActivity.this);
                            video_image.setLayoutParams(lp);
                        }

                        @Override
                        public void onLoadingCancelled(String s, View view) {

                        }
                    });
//                    ImageLoader.getInstance().displayImage(Uri.parse("http://192.168.137.1/ben/image.jpg").toString(), video_image);
                    video.findViewById(R.id.play).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Utils.playVideoYoutube(NewsDetailActivity.this, video_url);
                        }
                    });
                    container.addView(video);
                }
            }
        }
    }

    private void share() {
        Intent share= new Intent(Intent.ACTION_SEND);
        share.putExtra(Intent.EXTRA_TITLE,news.getTitle());
        share.putExtra(Intent.EXTRA_SUBJECT,news.getTitle());
        share.setType("image/*");
        if(news.getUrl() != null && !news.getUrl().equals("")){
            share.putExtra(Intent.EXTRA_TEXT, news.getUrl());
        }else
            share.putExtra(Intent.EXTRA_TEXT, "http://land-rover-bar.americascup.com/en/app.html");
        if(photoURI!=null)
            share.putExtra(Intent.EXTRA_STREAM,photoURI);
        startActivity(share);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
            super.onBackPressed();;
        if(item.getItemId()== R.id.action_settings)
        {
            share();
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        String name= "News Details";
        Aplication application = (Aplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName(name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }
}
