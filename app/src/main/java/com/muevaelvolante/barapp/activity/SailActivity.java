package com.muevaelvolante.barapp.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.muevaelvolante.Aplication;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.model.team.Sailing;
import com.muevaelvolante.barapp.util.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SailActivity extends AppCompatActivity {

    @Bind(R.id.picture)
    ImageView picture;
    @Bind(R.id.name_country)
    TextView name_country;
    @Bind(R.id.position)
    TextView position;
    @Bind(R.id.place)
    TextView place;
    @Bind(R.id.role)
    TextView lang;
    @Bind(R.id.helmet)
    TextView helmet;
    @Bind(R.id.tw)
    TextView tw;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.container)
    LinearLayout container;
    @Bind(R.id.toolBartittle)
    TextView toolBarTittle;
    Uri photoURI = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sail);
        ButterKnife.bind(this);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.back));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        final Sailing sail = (Sailing) getIntent().getExtras().getSerializable("sail");
        name_country.setText(sail.getName() + " (" + sail.getCountry() + ")");
        name_country.setTypeface(Aplication.robotoM);
        position.setText(sail.getPosition());
        place.setText(sail.getBirthplace());
        toolBarTittle.setText(sail.getName());
        ;
        helmet.setText(sail.getHelmetNumber());
        lang.setText(sail.getRole());
        tw.setText("@" + sail.getTwitter());
        tw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/" + sail.getTwitter())));
            }
        });
        List<Object> texts = sail.getText();
        for (int i = 0; i < texts.size(); i++) {
            Object item = texts.get(i);
            if (item instanceof String) {
                View string = getLayoutInflater().inflate(R.layout.news_text, null);
                ((TextView) string.findViewById(R.id.text)).setText(Html.fromHtml(String.valueOf(item)));
                container.addView(string);
                continue;
            }
            if (item instanceof LinkedHashMap) {
                LinkedHashMap<String, String> map = (LinkedHashMap<String, String>) item;
                if (map.size() == 3) { //es una imagen
                    View imagen = getLayoutInflater().inflate(R.layout.news_image, null);
                    final ImageView image = (ImageView) imagen.findViewById(R.id.image);
                    Log.d("image URL", map.get("med"));
                    ImageLoader.getInstance().displayImage(map.get("med"), image, new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String s, View view) {

                        }

                        @Override
                        public void onLoadingFailed(String s, View view, FailReason failReason) {

                        }

                        @Override
                        public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                            float proportion = (float) bitmap.getWidth() / bitmap.getHeight();
                            int alto = (int) (Utils.getScreenWidth(SailActivity.this) / proportion);
                            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) image.getLayoutParams();
                            lp.height = alto;
                            lp.width = Utils.getScreenWidth(SailActivity.this);
                            image.setLayoutParams(lp);
                        }

                        @Override
                        public void onLoadingCancelled(String s, View view) {

                        }
                    });
                    container.addView(imagen);
                    continue;
                }
                if (map.size() == 4) {//es un video
                    final String video_url = map.get("video");
                    ImageLoader.getInstance().displayImage(map.get("med"), picture, new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String s, View view) {

                        }

                        @Override
                        public void onLoadingFailed(String s, View view, FailReason failReason) {

                        }

                        @Override
                        public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                            float proportion = (float) bitmap.getWidth() / bitmap.getHeight();
                            int alto = (int) (Utils.getScreenWidth(SailActivity.this) / proportion);
                            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) picture.getLayoutParams();
                            lp.height = alto;
                            lp.width = Utils.getScreenWidth(SailActivity.this);
                            picture.setLayoutParams(lp);
                            //save temp bitMap for share
                            String file_path = Environment.getExternalStorageDirectory().getAbsolutePath();
                            File dir = new File(file_path);
                            File file = new File(dir, "123.png");
                            FileOutputStream fOut;
                            try {
                                fOut = new FileOutputStream(file);
                                bitmap.compress(Bitmap.CompressFormat.PNG, 85, fOut);
                                fOut.flush();
                                fOut.close();
                                photoURI = Uri.fromFile(file);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onLoadingCancelled(String s, View view) {

                        }
                    });
                    picture.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Utils.playVideoYoutube(SailActivity.this, video_url);
                        }
                    });
                    findViewById(R.id.imageView19).setVisibility(View.VISIBLE);
                    findViewById(R.id.imageView19).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Utils.playVideoYoutube(SailActivity.this, video_url);
                        }
                    });
                }
                continue;
            }
            if (item instanceof ArrayList) {//es una galeria
                View galeria = getLayoutInflater().inflate(R.layout.news_gallery, null);
                ViewPager pager = (ViewPager) galeria.findViewById(R.id.pager);
                GalleryAdapter galleryAdapter = new GalleryAdapter(pager, (ArrayList<LinkedHashMap<String, String>>) item);
                pager.setAdapter(galleryAdapter);
                container.addView(galeria);
                TextView t = new TextView(this);
                t.setText("sfsdfsdf");
                t.setVisibility(View.INVISIBLE);
                container.addView(t);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        String name= "Sail View";
        Aplication application = (Aplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName(name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_settings:
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("image/*");
                share.putExtra(Intent.EXTRA_TEXT, "http://land-rover-bar.americascup.com/en/app.html");
                if (photoURI != null)
                    share.putExtra(Intent.EXTRA_STREAM, photoURI);
                startActivity(share);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    class GalleryAdapter extends PagerAdapter {


        @Bind(R.id.image)
        ImageView image;
        @Bind(R.id.next)
        View next;
        @Bind(R.id.prev)
        View prev;
        ViewPager pager;
        boolean isViewpagerResized = false;

        ArrayList<LinkedHashMap<String, String>> items;


        public GalleryAdapter(ViewPager pager, ArrayList<LinkedHashMap<String, String>> items) {
            this.pager = pager;
            this.items = items;
        }

        @Override
        public Object instantiateItem(final ViewGroup container, int position) {
            final LinkedHashMap<String, String> galleryItem = items.get(position);
            View view = ((LayoutInflater) SailActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.news_gallery_item, container, false);
            ButterKnife.bind(this, view);
            ImageLoader.getInstance().displayImage(Uri.parse(galleryItem.get("med")).toString(), image, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String s, View view) {

                }

                @Override
                public void onLoadingFailed(String s, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                    if (!isViewpagerResized) {
                        float proportion = (float) bitmap.getWidth() / bitmap.getHeight();
                        int alto = (int) (Utils.getScreenWidth(SailActivity.this) / proportion);
                        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) image.getLayoutParams();
                        lp.height = alto;
                        lp.width = Utils.getScreenWidth(SailActivity.this);
                        image.setLayoutParams(lp);
                        pager.setLayoutParams(lp);
                        isViewpagerResized = true;
                    }
                }

                @Override
                public void onLoadingCancelled(String s, View view) {

                }
            });

            if (position < items.size() - 1) {
                next.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (pager.getCurrentItem() + 1 <= items.size() - 1)
                            pager.setCurrentItem(pager.getCurrentItem() + 1);
                    }
                });
            } else
                next.setVisibility(View.GONE);
            if (position > 0) {
                prev.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (pager.getCurrentItem() - 1 >= 0)
                            pager.setCurrentItem(pager.getCurrentItem() - 1);
                    }
                });
            } else
                prev.setVisibility(View.GONE);
            container.addView(view);
            return view;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

        @Override
        public void destroyItem(View container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }


    }
}
