package com.muevaelvolante.barapp.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.muevaelvolante.Aplication;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.commons.Constants;
import com.muevaelvolante.barapp.interfaces.ISustainability;
import com.muevaelvolante.barapp.model.sustainability.Pledge;
import com.muevaelvolante.barapp.services.ServiceGenerator;
import com.muevaelvolante.barapp.util.Utils;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PledgeActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener{

    @Bind(R.id.root)
    LinearLayout root;
    RadioButton prevuiosButton;
    View previousView;

    @Bind(R.id.lp1)
    View lp1;
    @Bind(R.id.lp2)
    View lp2;@Bind(R.id.lp3)
    View lp3;@Bind(R.id.lp4)
    View lp4;@Bind(R.id.lp5)
    View lp5;

    @Bind(R.id.pledge1)
    RadioButton pledge1;
    @Bind(R.id.pledge2)
    RadioButton pledge2;
    @Bind(R.id.pledge3)
    RadioButton pledge3;
    @Bind(R.id.pledge4)
    RadioButton pledge4;
    @Bind(R.id.pledge5)
    RadioButton pledge5;
    @Bind(R.id.textView11)
    View share1;
    @Bind(R.id.radio)
    View radio;
    @Bind(R.id.share)
    View share;
    private ISustainability service;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pledge_dialog);
        ButterKnife.bind(this);
        FrameLayout.LayoutParams lp= (FrameLayout.LayoutParams) root.getLayoutParams();
        lp.width= (int) (Utils.getScreenWidth(this)/1.2);
        for(int i=0; i< 3;i++){
            Pledge pledge= Aplication.pledge.get(i);
            switch (i){
                case 0:
                    pledge1.setTag(pledge);
                    pledge1.setText(pledge.getPledge());
                    pledge1.setOnCheckedChangeListener(this);
                    pledge1.setVisibility(View.VISIBLE);
                    break;
                case 1:
                    pledge2.setTag(pledge);
                    pledge2.setText(pledge.getPledge());
                    pledge2.setOnCheckedChangeListener(this);
                    pledge2.setVisibility(View.VISIBLE);
                    break;
                case 2:
                    pledge3.setTag(pledge);
                    pledge3.setText(pledge.getPledge());
                    pledge3.setOnCheckedChangeListener(this);
                    pledge3.setVisibility(View.VISIBLE);
                    break;
                case 3:
                    pledge4.setTag(pledge);
                    pledge4.setText(pledge.getPledge());
                    pledge4.setOnCheckedChangeListener(this);
                    pledge4.setVisibility(View.VISIBLE);
                    break;
                case 4:
                    pledge5.setTag(pledge);
                    pledge5.setText(pledge.getPledge());
                    pledge5.setOnCheckedChangeListener(this);
                    pledge5.setVisibility(View.VISIBLE);
                    break;
            }
        }
        service= ServiceGenerator.createService(ISustainability.class);
        root.setLayoutParams(lp);
        share1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(previousView!=null){
                    Pledge pledge= (Pledge) prevuiosButton.getTag();
                    Intent share= new Intent(Intent.ACTION_SEND);
                    share.setType("text/plain");
                    share.putExtra(Intent.EXTRA_TEXT, pledge.getShare_Message());
                    startActivity(share);
                    finish();
                }else {
                    Toast.makeText(getApplicationContext(),"At least one item must be selected..",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(previousView!=null){
        Call<ResponseBody> pledgeCall= service.sendPledge("http://mueva.eu/BARapp/index.php","bar_app_makeapledge", Integer.parseInt(((Pledge)prevuiosButton.getTag()).getID()),
                getSharedPreferences(Constants.LOGIN_DATA,MODE_PRIVATE).getString(Constants.LOGIN_DATA_IDENTITY,"2563214789"));
        Log.d("url",pledgeCall.request().url().url().toString());
        pledgeCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        String name= "Pledge View";
        Aplication application = (Aplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName(name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked){
            //al actual lo pongo negro
            ((View) buttonView.getParent()).setBackgroundColor(Color.parseColor("#e60016"));

        if(previousView==null){
            root.setBackgroundColor(Color.parseColor("#ee0000"));
            previousView= (View) buttonView.getParent();
            prevuiosButton= (RadioButton) buttonView;
            radio.setVisibility(View.INVISIBLE);
            share.setVisibility(View.VISIBLE);
        }
        else{
            previousView.setBackgroundColor(getResources().getColor(android.R.color.transparent));
            previousView= (View) buttonView.getParent();
            prevuiosButton.setChecked(false);
            prevuiosButton= (RadioButton) buttonView;
        }

        }
    }
}
