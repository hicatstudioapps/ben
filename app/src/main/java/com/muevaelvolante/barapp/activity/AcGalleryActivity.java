package com.muevaelvolante.barapp.activity;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ViewFlipper;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.muevaelvolante.Aplication;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.fragment.AcGalleryItemFragment;
import com.muevaelvolante.barapp.fragment.GalleryItem;
import com.muevaelvolante.barapp.model.AC.AcTheGuide;
import com.muevaelvolante.barapp.model.news.Gallery;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

public class AcGalleryActivity extends AppCompatActivity {


    @Bind(R.id.toolbar)
    Toolbar toolbar;
    AcTheGuide gallery;
    @Bind(R.id.pager)
    ViewPager pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_gallery);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(" ");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.back));
        gallery= (AcTheGuide)getIntent().getExtras().getSerializable("gal");
        pager.setAdapter(new PageAdaper(getSupportFragmentManager()));
        pager.setOffscreenPageLimit(10);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
            super.onBackPressed();;
        return true;
    }


    class PageAdaper extends FragmentStatePagerAdapter {

        public PageAdaper(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            LinkedHashMap<String,Object> map= (LinkedHashMap<String,Object>) gallery.getItems().get(position);
            ArrayList list=(ArrayList)map.get("media");
            String url= ((LinkedHashMap<String,String>)list.get(0)).get("original").toString();
            switch (position){
                default:
                    return AcGalleryItemFragment.newInstance(url);
            }
        }

        @Override
        public int getCount() {
            return gallery.getItems().size();
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        String name= "AC Gallery";
        Aplication application = (Aplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName(name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

}
