package com.muevaelvolante.barapp.activity.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.amazonaws.mobileconnectors.cognito.CognitoSyncManager;
import com.amazonaws.mobileconnectors.cognito.Dataset;
import com.amazonaws.mobileconnectors.cognito.Record;
import com.amazonaws.mobileconnectors.cognito.SyncConflict;
import com.amazonaws.mobileconnectors.cognito.exceptions.DataStorageException;
import com.amazonaws.regions.Regions;
import com.muevaelvolante.Aplication;
import com.muevaelvolante.barapp.HomeActivity;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.commons.Constants;
import com.muevaelvolante.barapp.push.MessageReceivingService;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ProfileFragment extends Fragment implements CompoundButton.OnCheckedChangeListener{


    @Bind(R.id.ck_email)
    CheckBox email;
    @Bind(R.id.ck_news)
    CheckBox news;
    @Bind(R.id.ck_racing)
    CheckBox racing;
    @Bind(R.id.ck_sust)
    CheckBox sust;
    Dataset dataSet;
    @Bind(R.id.button3)
    View buttonSave;
    @Bind(R.id.profile_image)
    ImageView picture;
    @Bind(R.id.user)
    TextView user;
    @Bind(R.id.name)
    EditText userName;
    @Bind(R.id.email)
    EditText emailText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View layout= inflater.inflate(R.layout.fragment_profile,null);
        ButterKnife.bind(this,layout);
        CognitoSyncManager syncClient= new CognitoSyncManager(getActivity(), Regions.EU_WEST_1,
                Aplication.cognitoClientManager.getCredentials());
        dataSet = syncClient.openOrCreateDataset("profile");
        String news= dataSet.get(Constants.NOTIFICATION_NEWS);
        String racing= dataSet.get(Constants.NOTIFICATION_RACING_ALERT);
        String sust= dataSet.get(Constants.NOTIFICATION_SUST_ALERT);
        String email= dataSet.get(Constants.NOTIFICATION_EMAIL_ALERT);
        final SharedPreferences pref= getActivity().getSharedPreferences(Constants.LOGIN_DATA, Context.MODE_PRIVATE);
        String mailText= pref.getString(Constants.LOGIN_DATA_EMAIL,"");
        String name=pref.getString(Constants.LOGIN_DATA_USER,"");
        emailText.setText(mailText);
        userName.setText(name);
        if(TextUtils.isEmpty(mailText))
            emailText.setHint("Email Address");
        if(news!= null && news.equals("1"))
            this.news.setChecked(true);
        else
            this.news.setChecked(false);
        if( racing!= null && racing.equals("1"))
            this.racing.setChecked(true);
        else
            this.racing.setChecked(false);
        if(sust!= null && sust.equals("1") )
            this.sust.setChecked(true);
        else
            this.sust.setChecked(false);
        if(email!= null && email.equals("1"))
            this.email.setChecked(true);
        else
            this.email.setChecked(false);
        this.news.setOnCheckedChangeListener(this);
        this.racing.setOnCheckedChangeListener(this);
        this.sust.setOnCheckedChangeListener(this);
        this.email.setOnCheckedChangeListener(this);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Aplication.isLoginFromWeb){
                    ((HomeActivity)getActivity()).showHome();
                    return;
                }
                dataSet.put("name",userName.getText().toString());
                dataSet.put("mail",emailText.getText().toString());
                pref.edit().putString(Constants.LOGIN_DATA_USER,userName.getText().toString()).commit();
                pref.edit().putString(Constants.LOGIN_DATA_EMAIL,emailText.getText().toString()).commit();
                dataSet.synchronize(new Dataset.SyncCallback() {
                    @Override
                    public void onSuccess(Dataset dataset, List<Record> list) {

                    }

                    @Override
                    public boolean onConflict(Dataset dataset, List<SyncConflict> list) {
                        return false;
                    }

                    @Override
                    public boolean onDatasetDeleted(Dataset dataset, String s) {
                        return false;
                    }

                    @Override
                    public boolean onDatasetsMerged(Dataset dataset, List<String> list) {
                        return false;
                    }

                    @Override
                    public void onFailure(DataStorageException e) {

                    }
                });
                new MessageReceivingService(getContext()).execute();
                ((HomeActivity)getActivity()).showHome();
            }
        });
        SharedPreferences pf= getActivity().getSharedPreferences(Constants.LOGIN_DATA, Context.MODE_PRIVATE);
        ImageLoader.getInstance().displayImage(pf.getString(Constants.LOGIN_DATA_PICT,""),picture);
        user.setText(pf.getString(Constants.LOGIN_DATA_USER,"anonimous"));
        return layout;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()){
            case R.id.ck_news:
                if(isChecked)
                    dataSet.put(Constants.NOTIFICATION_NEWS,"1");
                else
                    dataSet.put(Constants.NOTIFICATION_NEWS,"0");
                break;
            case R.id.ck_racing:
                if(isChecked)
                    dataSet.put(Constants.NOTIFICATION_RACING_ALERT,"1");
                else
                    dataSet.put(Constants.NOTIFICATION_RACING_ALERT,"0");
                break;
            case R.id.ck_sust:
                if(isChecked)
                    dataSet.put(Constants.NOTIFICATION_SUST_ALERT,"1");
                else
                    dataSet.put(Constants.NOTIFICATION_SUST_ALERT,"0");
                break;
            case R.id.ck_email:
                if(isChecked)
                    dataSet.put(Constants.NOTIFICATION_EMAIL_ALERT,"1");
                else
                    dataSet.put(Constants.NOTIFICATION_EMAIL_ALERT,"0");
                break;
        }
    }
}
