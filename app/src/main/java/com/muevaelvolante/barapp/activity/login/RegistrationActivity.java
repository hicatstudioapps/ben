package com.muevaelvolante.barapp.activity.login;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.muevaelvolante.Aplication;
import com.muevaelvolante.barapp.HomeActivity;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.commons.Constants;
import com.muevaelvolante.barapp.interfaces.IRegistrationAndLogin;
import com.muevaelvolante.barapp.services.ServiceGenerator;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends AppCompatActivity {

    @Bind(R.id.name)
    EditText name;
    @Bind(R.id.last)
    EditText last;
    @Bind(R.id.email)
    EditText email;
    @Bind(R.id.password)
    EditText password;
    @Bind(R.id.button3)
    View register;
    private ProgressDialog mProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //valido campos no nulos
                if (!TextUtils.isEmpty(name.getText().toString()) ||
                        !TextUtils.isEmpty(last.getText().toString()) ||
                        !TextUtils.isEmpty(email.getText().toString()) ||
                        !TextUtils.isEmpty(password.getText().toString())) {
                    //valido email
                    if(!Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches() ||
                            email.getText().toString().toLowerCase().equals("test@error.com")){
                        Toast.makeText(RegistrationActivity.this,"Invalid email address!!",Toast.LENGTH_LONG).show();
                        return;
                    }
                    showProgressDialog();
                    IRegistrationAndLogin registrationAndLogin = ServiceGenerator.createService(IRegistrationAndLogin.class);
                    Map<String, String> mapData = new LinkedHashMap<String, String>();
                    mapData.put("tableName", "registration");
                    mapData.put("email", email.getText().toString());
                    mapData.put("firstname", name.getText().toString());
                    mapData.put("lastname", last.getText().toString());
                    mapData.put("password", password.getText().toString());
                    Call<ResponseBody> call = registrationAndLogin.register(" http://mueva.eu/BARapp/index.php", mapData);
//                    Call<ResponseBody> call = registrationAndLogin.register("http://192.168.101.1:8081/ben/", mapData);
                    call.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.isSuccessful()) {
                                try {
                                    Log.d("result", response.body().string());
                                    //como todo fue ok, almaceno el email como identity id
                                    SharedPreferences pref= getSharedPreferences(Constants.LOGIN_DATA, MODE_PRIVATE);
                                    SharedPreferences.Editor edit=pref.edit();
                                    edit.putString(Constants.LOGIN_DATA_ACCES_TOKEN,email.getText().toString()+"");
                                    edit.putString(Constants.LOGIN_DATA_EMAIL,email.getText().toString());
                                    edit.putString(Constants.LOGIN_DATA_USER,name.getText().toString() +" "+ last.getText().toString());
                                    edit.commit();
                                    Intent home= new Intent(RegistrationActivity.this, HomeActivity.class);
                                    startActivity(home);
                                    if(Aplication.loginActivity!=null)
                                        Aplication.loginActivity.finish();
                                    RegistrationActivity.this.finish();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                hideProgressDialog();
                                showNoConnectionError();
                            }
                        }
                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            hideProgressDialog();
                            showNoConnectionError();
                        }
                    });
                }else{
                    Toast.makeText(RegistrationActivity.this,"All fields must be filled..",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Sending data...");
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    public void showNoConnectionError(){
        AlertDialog.Builder dialog= new AlertDialog.Builder(this);
        dialog.setMessage("You have no internet connection!!");
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                RegistrationActivity.this.finish();
            }
        });
        dialog.show();
    }
}
