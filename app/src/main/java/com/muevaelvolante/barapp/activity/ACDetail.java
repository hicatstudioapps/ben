package com.muevaelvolante.barapp.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.muevaelvolante.Aplication;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.model.AC.AcTheGuide;
import com.muevaelvolante.barapp.util.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.LinkedHashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ACDetail extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.date)
    TextView date;
    @Bind(R.id.tittle)
    TextView tittle;
    AcTheGuide news;
    @Bind(R.id.imageView14)
    ImageView picture;
    @Bind(R.id.container)
    LinearLayout container;

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle=getIntent().getExtras();
        news=(AcTheGuide) bundle.getSerializable("news");
        setContentView(R.layout.content_news_detail);
        ButterKnife.bind(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(" ");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.back));
        ImageLoader.getInstance().displayImage(news.getMedia().get(0).getMed(), picture);
//        ImageLoader.getInstance().displayImage(Uri.parse("http://192.168.137.1/ben/image.jpg").toString(), picture);
        tittle.setText(news.getTitle());
        date.setText(Utils.getDateString(Utils.convertStringToDate(news.getDate())));
        List<Object> texts=news.getText();
        for(int i=0; i< texts.size();i++){
            Object item=texts.get(i);
            if(item instanceof  String){
                View string= getLayoutInflater().inflate(R.layout.news_text,null);
                ((TextView)string.findViewById(R.id.text)).setText(Html.fromHtml(String.valueOf(item)));
                container.addView(string);
                continue;
            }
            if(item instanceof LinkedHashMap){
                LinkedHashMap<String,String> map= (LinkedHashMap<String, String>) item;
                if(map.size()==4){ //es una imagen
                    View imagen= getLayoutInflater().inflate(R.layout.news_image,null);
                    ImageView image= (ImageView) imagen.findViewById(R.id.image);
                    Log.d("image URL", map.get("med"));
                    ImageLoader.getInstance().displayImage(map.get("med"),image);
//                      ImageLoader.getInstance().displayImage(Uri.parse("http://192.168.137.1/ben/image.jpg").toString(), image);
                    container.addView(imagen);
                    continue;
                }
                if(map.size()==5) {//es un video
                    View video= getLayoutInflater().inflate(R.layout.news_video,null);
                    ImageView video_image=((ImageView)video.findViewById(R.id.video_image));
                    final String video_url= map.get("video");
                    Log.d("video_url", video_url);
                    ImageLoader.getInstance().displayImage(map.get("med"),video_image);
//                    ImageLoader.getInstance().displayImage(Uri.parse("http://192.168.137.1/ben/image.jpg").toString(), video_image);
                    video.findViewById(R.id.play).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Utils.playVideoYoutube(ACDetail.this, video_url);
                        }
                    });
                    container.addView(video);
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
            super.onBackPressed();;
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        String name= "AC Details";
        Aplication application = (Aplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName(name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }
}
