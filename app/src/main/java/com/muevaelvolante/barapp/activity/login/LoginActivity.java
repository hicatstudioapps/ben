package com.muevaelvolante.barapp.activity.login;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.preference.Preference;
import android.support.v7.app.AppCompatActivity;

import android.os.AsyncTask;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.Scopes;
import com.google.android.gms.plus.Plus;
import com.lacostra.utils.notification.Permission.Permission;
import com.lacostra.utils.notification.Permission.PermissionActivity;
import com.muevaelvolante.barapp.model.setting.Login;
import com.muevaelvolante.barapp.util.ExceptionHandler;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import io.fabric.sdk.android.Fabric;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.amazonaws.mobileconnectors.cognito.CognitoSyncManager;
import com.amazonaws.mobileconnectors.cognito.Dataset;
import com.amazonaws.mobileconnectors.cognito.Record;
import com.amazonaws.mobileconnectors.cognito.SyncConflict;
import com.amazonaws.mobileconnectors.cognito.exceptions.DataStorageException;
import com.amazonaws.regions.Regions;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.muevaelvolante.Aplication;
import com.muevaelvolante.barapp.HomeActivity;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.cognito.CognitoClientManager;
import com.muevaelvolante.barapp.commons.Constants;
import com.muevaelvolante.barapp.interfaces.IFacebookImage;
import com.muevaelvolante.barapp.model.FaceBookImage;
import com.muevaelvolante.barapp.model.facebook.ImageModel;
import com.muevaelvolante.barapp.services.ServiceGenerator;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends PermissionActivity implements OnClickListener,
        GoogleApiClient.OnConnectionFailedListener{

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "WK8KZ619J4b7egOpl89pWMcY4";
    private static final String TWITTER_SECRET = "sG7PNlpGMfjyV0VF3sVkSPsxZuMgkDxTbJ04phv7l5T4gPARC5";


    public static String TAG="LoginActivity";
    @Bind(R.id.login)
    Button login;
    @Bind(R.id.join)
    Button join;
    @Bind(R.id.not_now)
    TextView not_now;

    //Botones para el login
    @Bind(R.id.googleSig)
    ImageButton google;
    @Bind(R.id.faceSig)
    ImageButton facebook;
    @Bind(R.id.tLogin)
    TwitterLoginButton tLogin;
    @Bind(R.id.imageButton2)
    View bTwitter;

    //facebook
    private CallbackManager callbackManager;

    //REQUEST LOGIN CODES
    public static int GOOGLE_SIG_IN=900;
    private GoogleApiClient mGoogleApiClient;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        setContentView(R.layout.activity_login);
        ExceptionHandler.register(this, "Error ben app", "leoperezortiz@gmail.com");
        ButterKnife.bind(this);
        join.setOnClickListener(this);
        login.setOnClickListener(this);
        not_now.setOnClickListener(this);
        google.setOnClickListener(this);
        facebook.setOnClickListener(this);
        bTwitter.setOnClickListener(this);
        callbackManager = CallbackManager.Factory.create();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        // [END build_client]
        tLogin.setCallback(new LoginTwitterHandler());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(!Permission.hasPermission(this,Manifest.permission.GET_ACCOUNTS))
            Permission.fail(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.not_now:
                startActivity(new Intent(this, HomeActivity.class));
                finish();
                Aplication.isAnonimous=true;
                break;
            case R.id.join:
                startActivity(new Intent(this,RegistrationActivity.class));
                Aplication.loginActivity=this;
                break;
            case R.id.login:
                startActivity(new Intent(this,LoginLoginActivity.class));
                Aplication.loginActivity=this;
                break;
            case R.id.googleSig:
                CognitoClientManager.getCredentials().clear();
                if(Permission.hasPermission(this,Manifest.permission.GET_ACCOUNTS)){
                showProgressDialog();
                    signInGoogle();
                }
                else
                {
                    ArrayList<String> perm= new ArrayList<>();
                    perm.add(Manifest.permission.GET_ACCOUNTS);
                    askPermision((ArrayList<String>) perm, 11);
                }
                break;
            case R.id.faceSig:
                CognitoClientManager.getCredentials().clear();
                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("public_profile",
                        "email"));
                LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        new SyncFacebook(loginResult).execute();
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(LoginActivity.this, "Facebook login cancelled",
                                Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Toast.makeText(LoginActivity.this, "Error in Facebook login " +
                                error.getMessage(), Toast.LENGTH_LONG).show();
                        LoginManager.getInstance().logOut();
                    }
                });
                break;
            case R.id.imageButton2:
                CognitoClientManager.getCredentials().clear();
                tLogin.performClick();
                break;
        }
    }



    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    // [START signIn]
    private void signInGoogle() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, GOOGLE_SIG_IN);
    }
    // [END signIn]

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
       if(requestCode == GOOGLE_SIG_IN){
           handleSignInResultGoogle(Auth.GoogleSignInApi.getSignInResultFromIntent(data));
           return;
       }
        callbackManager.onActivityResult(requestCode, resultCode, data);
        tLogin.onActivityResult(requestCode, resultCode, data);
    }
    // [START handleSignInResult]
    private void handleSignInResultGoogle(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            new LoadToken().execute(acct);

        } else {
            // Signed out, show unauthenticated UI.
            hideProgressDialog();
          Log.d("Login","FAILED");
        }
    }
    // [END handleSignInResult]

    class LoadToken extends AsyncTask<GoogleSignInAccount,Void,String>{

        @Override
        protected String doInBackground(GoogleSignInAccount... params) {
            try {
                String token=null;
                final String accountName =params[0].getEmail();
                final Account googleAccount = new Account(accountName, GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE);
                final String scopes = "audience:server:client_id:" +"39673393529-em5nchrd1oi1ifu3rm1h3fanhd1hkpir.apps.googleusercontent.com";
                token = GoogleAuthUtil.getToken(LoginActivity.this, googleAccount, scopes);
                Log.d("token",token);
                Map<String, String> logins = new HashMap<String, String>();
                logins.put("accounts.google.com", token);
                Aplication.cognitoClientManager.getCredentials().clearCredentials();
                Aplication.cognitoClientManager.getCredentials().withLogins(logins);
                Aplication.cognitoClientManager.getCredentials().refresh();
                SharedPreferences pref= getSharedPreferences(Constants.LOGIN_DATA, MODE_PRIVATE);
                SharedPreferences.Editor edit=pref.edit();
                if(CognitoClientManager.isAuthenticated()){
                    Log.d("identiyID", CognitoClientManager.getCredentials().getIdentityId());
                    edit.putString(Constants.LOGIN_DATA_IDENTITY,CognitoClientManager.getCredentials().getIdentityId());
                }
                edit.putString(Constants.LOGIN_DATA_USER_G,Constants.LOGIN_DATA_USER_G);
                edit.putString(Constants.LOGIN_DATA_ACCES_TOKEN, token);
                edit.putString(Constants.LOGIN_DATA_USER,params[0].getDisplayName());
                if(params[0].getPhotoUrl()!=null)
                edit.putString(Constants.LOGIN_DATA_PICT,params[0].getPhotoUrl().toString());
                edit.putString(Constants.LOGIN_DATA_EMAIL,params[0].getEmail());
                edit.commit();
                SyncProfile();
                return "1";
            } catch (Exception e) {
                e.printStackTrace();
                StringBuilder sb = new StringBuilder();
                String message = e.getLocalizedMessage();
                StackTraceElement[] traces = e.getStackTrace();
                sb.append("Message: "+message+"\n");
                for(StackTraceElement trace:traces){
                    sb.append("at "+trace.getClassName()+"."+trace.getMethodName()+"("+trace.getFileName()+":"+trace.getLineNumber()+")\n");
                }
//
            }

            return null;
        }
        @Override
        protected void onPostExecute(String response) {
            hideProgressDialog();
            if (response != null) {
                //iniciamos activity para la gestion del perfil
                startActivity(new Intent(LoginActivity.this,AfterLoginSocialActivity.class));
                finish();
                hideProgressDialog();
            } else {
                Toast.makeText(LoginActivity.this, "Unable to get user name from Google",
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    //sync cognito preferences

    void SyncProfile(){
        // Initialize the Cognito Sync client
        CognitoSyncManager syncClient = new CognitoSyncManager(
                getApplicationContext(),
                Regions.EU_WEST_1, // Region
                Aplication.cognitoClientManager.getCredentials());
        // Create a record in a dataset and synchronize with the server
        Dataset dataset = syncClient.openOrCreateDataset("profile");
        dataset.synchronize(new Dataset.SyncCallback() {
            @Override
            public void onSuccess(Dataset dataset, List<Record> list) {
                Log.d("esto fallo","");
            }

            @Override
            public boolean onConflict(Dataset dataset, List<SyncConflict> list) {
                return false;
            }

            @Override
            public boolean onDatasetDeleted(Dataset dataset, String s) {
                return false;
            }

            @Override
            public boolean onDatasetsMerged(Dataset dataset, List<String> list) {
                return false;
            }

            @Override
            public void onFailure(DataStorageException e) {
                Log.d("esto fallo","");
            }
        });
        //put name and email address
        SharedPreferences pref= getSharedPreferences(Constants.LOGIN_DATA, MODE_PRIVATE);
        String name= dataset.get("name");
        if(name==null || TextUtils.isEmpty(name))
            dataset.put("name",pref.getString(Constants.LOGIN_DATA_USER,""));
            else
            pref.edit().putString(Constants.LOGIN_DATA_USER,name).commit();
        String mail= dataset.get("mail");
        if(mail==null ||  TextUtils.isEmpty(mail))
            dataset.put("mail",pref.getString(Constants.LOGIN_DATA_EMAIL,""));
        else
            pref.edit().putString(Constants.LOGIN_DATA_EMAIL,mail).commit();
        String news= dataset.get(Constants.NOTIFICATION_NEWS);
        if(news== null || TextUtils.isEmpty(news))
            dataset.put(Constants.NOTIFICATION_NEWS,"1");
        String racing= dataset.get(Constants.NOTIFICATION_RACING_ALERT);
        if(racing== null || TextUtils.isEmpty(racing))
            dataset.put(Constants.NOTIFICATION_RACING_ALERT,"1");
        String sus= dataset.get(Constants.NOTIFICATION_SUST_ALERT);
        if(sus== null || TextUtils.isEmpty(sus))
            dataset.put(Constants.NOTIFICATION_SUST_ALERT,"1");
        String email= dataset.get(Constants.NOTIFICATION_EMAIL_ALERT);
        if(email== null || TextUtils.isEmpty(email))
            dataset.put(Constants.NOTIFICATION_EMAIL_ALERT,"1");
        dataset.synchronize(new Dataset.SyncCallback() {
            @Override
            public void onSuccess(Dataset dataset, List<Record> list) {

            }
            @Override
            public boolean onConflict(Dataset dataset, List<SyncConflict> list) {
                return false;
            }

            @Override
            public boolean onDatasetDeleted(Dataset dataset, String s) {
                return false;
            }

            @Override
            public boolean onDatasetsMerged(Dataset dataset, List<String> list) {
                return false;
            }

            @Override
            public void onFailure(DataStorageException e) {

            }
        });
    }

    private class SyncFacebook extends AsyncTask<Void, Void, String> {

        private final LoginResult loginResult;

        public SyncFacebook(LoginResult loginResult) {
            this.loginResult = loginResult;
        }

        @Override
        protected void onPreExecute() {
            showProgressDialog();
        }

        @Override
        protected String doInBackground(Void... params) {
            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {
                            // Application code
                            Log.v("LoginActivity", response.toString());
                        }
                    });
            Bundle parameters = new Bundle();
//            parameters.putString("fields", "name,email");
            parameters.putString("fields", "name,picture.type(large),email");
            request.setParameters(parameters);
            GraphResponse graphResponse = request.executeAndWait();
            Map<String, String> logins = new HashMap<String, String>();
            logins.put("graph.facebook.com", loginResult.getAccessToken().getToken());
            Aplication.cognitoClientManager.getCredentials().clearCredentials();
            Aplication.cognitoClientManager.getCredentials().withLogins(logins);
            try {
                Aplication.cognitoClientManager.getCredentials().refresh();
                //El login fue bien y pasamos a guardar los datos
                SharedPreferences pref= getSharedPreferences(Constants.LOGIN_DATA, MODE_PRIVATE);
                SharedPreferences.Editor edit=pref.edit();
                if(CognitoClientManager.isAuthenticated()){
                    Log.d("identiyID", CognitoClientManager.getCredentials().getIdentityId());
                    edit.putString(Constants.LOGIN_DATA_IDENTITY,CognitoClientManager.getCredentials().getIdentityId());
                }
                edit.putString(Constants.LOGIN_DATA_USER_F,Constants.LOGIN_DATA_USER_F);
                edit.putString(Constants.LOGIN_DATA_ACCES_TOKEN,loginResult.getAccessToken().getToken());
                edit.putString(Constants.LOGIN_DATA_USER,graphResponse.getJSONObject().getString("name"));
                String url= "https://graph.facebook.com/v2.5/"+graphResponse.getJSONObject().getString("id").toString()+"/picture?redirect=false";
                JSONObject pict= (JSONObject) ((JSONObject) graphResponse.getJSONObject().get("picture")).get("data");
                String picturl= (String) pict.get("url");
                edit.putString(Constants.LOGIN_DATA_PICT,picturl);
                try {
                    edit.putString(Constants.LOGIN_DATA_EMAIL, graphResponse.getJSONObject().getString("email"));
                }catch (JSONException e){
                    StringBuilder sb = new StringBuilder();
                    String message = e.getLocalizedMessage();
                    StackTraceElement[] traces = e.getStackTrace();
                    sb.append("Message: "+message+"\n");
                    for(StackTraceElement trace:traces){
                        sb.append("at "+trace.getClassName()+"."+trace.getMethodName()+"("+trace.getFileName()+":"+trace.getLineNumber()+")\n");
                    }
                    Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                            "mailto","leoperezortiz@gmail.com", null));
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "error login g+");
                    emailIntent.putExtra(Intent.EXTRA_TEXT,  sb.toString());
                    LoginActivity.this.startActivity(emailIntent);
                    edit.commit();
                    SyncProfile();
                    return "1";
                }
                edit.commit();
                SyncProfile();
                return "1";
            }catch (Exception e){
                StringBuilder sb = new StringBuilder();
                String message = e.getLocalizedMessage();
                StackTraceElement[] traces = e.getStackTrace();
                sb.append("Message: "+message+"\n");
                for(StackTraceElement trace:traces){
                    sb.append("at "+trace.getClassName()+"."+trace.getMethodName()+"("+trace.getFileName()+":"+trace.getLineNumber()+")\n");
                }
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String response) {
           hideProgressDialog();
            if (response != null) {
                //iniciamos activity para la gestion del perfil
                startActivity(new Intent(LoginActivity.this,AfterLoginSocialActivity.class));
                finish();
            } else {
                Toast.makeText(LoginActivity.this, "Unable to get user name from Facebook",
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    private class SyncTwitter extends AsyncTask<Void, Void, String> {
        private final TwitterSession twitterSession;

        public SyncTwitter(TwitterSession value) {
            this.twitterSession = value;
        }

        @Override
        protected void onPreExecute() {
           showProgressDialog();
        }

        @Override
        protected String doInBackground(Void... params) {

            TwitterAuthToken authToken = twitterSession.getAuthToken();
            final String[] pictureUrl = new String[2];
            Twitter.getApiClient().getAccountService().verifyCredentials(true, false, new Callback<User>() {
                @Override
                public void success(Result<User> result) {
                    Log.d("twitter url", result.data.profileImageUrl);
                    pictureUrl[0] = result.data.profileImageUrl;
                    pictureUrl[1]=result.data.email;
                }

                @Override
                public void failure(TwitterException exception) {
                  exception.printStackTrace();
                }
            });
            String value = authToken.token + ";" + authToken.secret;
            CognitoClientManager.addLogins("api.twitter.com",
                    value);
            Map<String, String> logins = new HashMap<String, String>();
            logins.put("api.twitter.com", value);
            Aplication.cognitoClientManager.getCredentials().clearCredentials();
            Aplication.cognitoClientManager.getCredentials().withLogins(logins);
            Aplication.cognitoClientManager.getCredentials().refresh();
            try {
                Aplication.cognitoClientManager.getCredentials().refresh();
                //El login fue bien y pasamos a guardar los datos
                SharedPreferences pref= getSharedPreferences(Constants.LOGIN_DATA, MODE_PRIVATE);
                final SharedPreferences.Editor edit=pref.edit();
                CognitoClientManager.init(LoginActivity.this);
                if(CognitoClientManager.isAuthenticated()){
                    Log.d("identiyID", CognitoClientManager.getCredentials().getIdentityId());
                    edit.putString(Constants.LOGIN_DATA_IDENTITY,CognitoClientManager.getCredentials().getIdentityId());
                }
                edit.putString(Constants.LOGIN_DATA_USER_T,Constants.LOGIN_DATA_USER_T);
                edit.putString(Constants.LOGIN_DATA_ACCES_TOKEN,value);
                edit.putString(Constants.LOGIN_DATA_USER,twitterSession.getUserName());
                String url= pictureUrl[0].replace("_normal","_bigger");
                edit.putString(Constants.LOGIN_DATA_PICT,url);
                edit.putString(Constants.LOGIN_DATA_EMAIL,pictureUrl[1]);
                new TwitterAuthClient().requestEmail(twitterSession, new Callback<String>() {
                    @Override
                    public void success(Result<String> result) {
                        String resultMail=result.data;
                        edit.putString(Constants.LOGIN_DATA_EMAIL,resultMail);
                        edit.commit();
                    }

                    @Override
                    public void failure(TwitterException e) {
                        edit.putString(Constants.LOGIN_DATA_EMAIL,"");
                    }
                });
                edit.commit();
                SyncProfile();
                return "1";
            }catch (Exception e){
                StringBuilder sb = new StringBuilder();
                String message = e.getLocalizedMessage();
                StackTraceElement[] traces = e.getStackTrace();
                sb.append("Message: "+message+"\n");
                for(StackTraceElement trace:traces){
                    sb.append("at "+trace.getClassName()+"."+trace.getMethodName()+"("+trace.getFileName()+":"+trace.getLineNumber()+")\n");
                }
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String response) {
           hideProgressDialog();
            if (response != null) {
                //iniciamos activity para la gestion del perfil
                final SharedPreferences pref= getSharedPreferences(Constants.LOGIN_DATA, MODE_PRIVATE);
                new TwitterAuthClient().requestEmail(twitterSession, new Callback<String>() {
                    @Override
                    public void success(Result<String> result) {
                        String resultMail=result.data;
                        SharedPreferences.Editor edit=pref.edit();
                        edit.putString(Constants.LOGIN_DATA_EMAIL,resultMail);
                        edit.commit();
                        startActivity(new Intent(LoginActivity.this,AfterLoginSocialActivity.class));
                        finish();
                    }

                    @Override
                    public void failure(TwitterException e) {
                        SharedPreferences.Editor edit=pref.edit();
                        edit.putString(Constants.LOGIN_DATA_EMAIL,"");
                        startActivity(new Intent(LoginActivity.this,AfterLoginSocialActivity.class));
                        finish();
                    }
                });

            } else {
                Toast.makeText(LoginActivity.this, "Unable to get user name from Twitter",
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    private class LoginTwitterHandler extends Callback<TwitterSession> {
        @Override
        public void success(Result<TwitterSession> twitterSessionResult) {
            TwitterSession session = twitterSessionResult.data;
            new SyncTwitter(session).execute();
            // Note: This overrides any existing logins

        }

        @Override
        public void failure(TwitterException e) {
            Toast.makeText(LoginActivity.this, "An error ocurred",
                    Toast.LENGTH_LONG).show();
        }
    }
}

