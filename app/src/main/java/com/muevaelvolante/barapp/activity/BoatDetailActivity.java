package com.muevaelvolante.barapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.muevaelvolante.Aplication;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.model.boat.Boat_;
import com.muevaelvolante.barapp.model.news.News;
import com.muevaelvolante.barapp.model.sustainability.Topstory;
import com.muevaelvolante.barapp.util.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class BoatDetailActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.tittle)
    TextView tittle;
    Boat_ boat;
    @Bind(R.id.image_header)
    ImageView header_image;
    @Bind(R.id.video_image)
    ImageView image_360;
    @Bind(R.id.container)
    LinearLayout container;

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle=getIntent().getExtras();
        boat =(Boat_) bundle.getSerializable("boat");
        setContentView(R.layout.activity_boat_detail);
        ButterKnife.bind(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(" ");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.back));
        tittle.setText(boat.getTitle());
        tittle.setTypeface(Aplication.robotoL);
        List<Object> texts= boat.getText();
        for(int i=0; i< texts.size();i++){
            Object item=texts.get(i);
            if(item instanceof ArrayList){
                ArrayList data= (ArrayList)item;
                View view_slide= getLayoutInflater().inflate(R.layout.section_sust_slide,container,false);
                ViewPager pager= (ViewPager)view_slide.findViewById(R.id.pager);
                RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams) pager.getLayoutParams();
                        CirclePageIndicator indicator=(CirclePageIndicator)view_slide.findViewById(R.id.indicator);
                int screenWidth= Utils.getScreenWidth(BoatDetailActivity.this);
                lp.width=screenWidth;
                lp.height=screenWidth/2;
                pager.setLayoutParams(lp);
                List<String>  urls= new LinkedList<>();
                for(int j=0; j< data.size();j++){
                    LinkedHashMap<String,String> url= (LinkedHashMap<String, String>) data.get(j);
                    urls.add(url.get("med"));
                }
                pager.setAdapter(new PagerAdapter(urls));
                indicator.setViewPager(pager);
                container.addView(view_slide);
                continue;
            }

            if(item instanceof String){
                if(TextUtils.isEmpty(String.valueOf(item)))
                    continue;
                View string= getLayoutInflater().inflate(R.layout.news_text,null);
                TextView textView=(TextView)string.findViewById(R.id.text);
                textView.setText(Html.fromHtml(String.valueOf(item)));
                textView.setTextColor(Color.parseColor("#ffffff"));
                container.addView(string);
                continue;
            }
            if(item instanceof LinkedHashMap){
                final LinkedHashMap<String,String> map= (LinkedHashMap<String, String>) item;
                if(map.size()==3){ //es una imagen
                    View imagen= getLayoutInflater().inflate(R.layout.news_image,null);
                    final ImageView image= (ImageView) imagen.findViewById(R.id.image);
                    Log.d("image URL", map.get("med"));
                    RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams) image.getLayoutParams();
                    int widthL= Utils.getScreenWidth(this);
                    lp.width=widthL;
                    lp.height= (int) (widthL*0.6);
                    image.setLayoutParams(lp);
                    ImageLoader.getInstance().displayImage(map.get("med"), image, new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String s, View view) {

                        }

                        @Override
                        public void onLoadingFailed(String s, View view, FailReason failReason) {

                        }

                        @Override
                        public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                            float proportion= (float)bitmap.getWidth()/bitmap.getHeight();
                            int alto= (int) (Utils.getScreenWidth(BoatDetailActivity.this)/proportion);
                            RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams) image.getLayoutParams();
                            lp.height=alto;
                            lp.width=Utils.getScreenWidth(BoatDetailActivity.this);
                            image.setLayoutParams(lp);
                        }

                        @Override
                        public void onLoadingCancelled(String s, View view) {

                        }
                    });
                    container.addView(imagen);
                    continue;
                }
                if(map.size()==4) {//es un video o un 360
                    final String video_url= map.get("video");
                    if(video_url != null){
                        View video= getLayoutInflater().inflate(R.layout.news_video,null);
                        final ImageView video_image=((ImageView)video.findViewById(R.id.video_image));
                        Log.d("video_url", video_url);
                        RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams) video_image.getLayoutParams();
                        int widthL= Utils.getScreenWidth(this);
                        lp.width=widthL;
                        lp.height= (int) (widthL*0.6);
                        video_image.setLayoutParams(lp);
                        ImageLoader.getInstance().displayImage(map.get("med"), video_image, new ImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String s, View view) {

                            }

                            @Override
                            public void onLoadingFailed(String s, View view, FailReason failReason) {

                            }

                            @Override
                            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                                float proportion= (float)bitmap.getWidth()/bitmap.getHeight();
                                int alto= (int) (Utils.getScreenWidth(BoatDetailActivity.this)/proportion);
                                RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams) video_image.getLayoutParams();
                                lp.height=alto;
                                lp.width=Utils.getScreenWidth(BoatDetailActivity.this);
                                video_image.setLayoutParams(lp);
                            }

                            @Override
                            public void onLoadingCancelled(String s, View view) {

                            }
                        });
                        video.findViewById(R.id.play).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Utils.playVideoYoutube(BoatDetailActivity.this, video_url);
                            }
                        });
                        container.addView(video);
                    }else{
                        ImageLoader.getInstance().displayImage(map.get("med"), header_image, new ImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String s, View view) {

                            }

                            @Override
                            public void onLoadingFailed(String s, View view, FailReason failReason) {

                            }

                            @Override
                            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                                float proportion= (float)bitmap.getWidth()/bitmap.getHeight();
                                int alto= (int) (Utils.getScreenWidth(BoatDetailActivity.this)/proportion);
                                RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams) header_image.getLayoutParams();
                                lp.height=alto;
                                lp.width=Utils.getScreenWidth(BoatDetailActivity.this);
                                header_image.setLayoutParams(lp);
                            }

                            @Override
                            public void onLoadingCancelled(String s, View view) {

                            }
                        });
                        image_360.setVisibility(View.VISIBLE);
                        image_360.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent video= new Intent(BoatDetailActivity.this, Google360.class);
                                video.putExtra("url",map.get("video360"));
                                startActivity(video);

                            }
                        });
                    }

                }
            }
        }
        //aqui valido si lo que va en el header no es un 360 muestro la imagen de media
        if(image_360.getVisibility() != View.VISIBLE){
            ImageLoader.getInstance().displayImage(boat.getMedia().get(0).getMed(), header_image, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String s, View view) {

                }

                @Override
                public void onLoadingFailed(String s, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                    float proportion= (float)bitmap.getWidth()/bitmap.getHeight();
                    int alto= (int) (Utils.getScreenWidth(BoatDetailActivity.this)/proportion);
                    RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams) header_image.getLayoutParams();
                    lp.height=alto;
                    lp.width=Utils.getScreenWidth(BoatDetailActivity.this);
                    header_image.setLayoutParams(lp);
                }

                @Override
                public void onLoadingCancelled(String s, View view) {

                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
            super.onBackPressed();;
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        String name= "Boat Details";
        Aplication application = (Aplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName(name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    class PagerAdapter extends android.support.v4.view.PagerAdapter {

        @Bind(R.id.image)
        ImageView picture;

        List<String> urls_imges;

        public PagerAdapter(List<String> urls_imges) {
            this.urls_imges = urls_imges;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            String url= urls_imges.get(position);
            View layout= getLayoutInflater().inflate(R.layout.fragment_sust_slide_item,container,false);
            ButterKnife.bind(this, layout);
            RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams) picture.getLayoutParams();
            int screenWidth= Utils.getScreenWidth(BoatDetailActivity.this);
            lp.width=screenWidth;
            lp.height=screenWidth/2;
            picture.setLayoutParams(lp);
            ImageLoader.getInstance().displayImage(url, picture);
            container.addView(layout);
            return layout;
        }


        @Override
        public int getCount() {
            return urls_imges.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }
    }
}
