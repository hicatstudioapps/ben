package com.muevaelvolante.barapp.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.hardware.Camera;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.lacostra.utils.notification.Permission.Permission;
import com.lacostra.utils.notification.Permission.PermissionActivity;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.camera.CameraPreview;
import com.muevaelvolante.barapp.commons.Constants;
import com.muevaelvolante.barapp.util.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class CameraActivity extends PermissionActivity {

    protected static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 0;
    private RelativeLayout SurView;
    private SurfaceHolder camHolder;
    private boolean previewRunning;
    public static Camera camera = null;
    private RelativeLayout CamView;
    private ImageView camera_image, preview;
    private TextView switch_cam;
    View buttons;
    int numOfCam = 0;
    int cameraFront = -1;
    int cameraBack = -1;
    int currentCamera = -1;
    private CameraPreview mPreview;
    RelativeLayout.LayoutParams param;
    RelativeLayout.LayoutParams paramS;
    ImageButton btn;
    private boolean load=false;

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        buttons= findViewById(R.id.buttons);
        buttons.setAnimation(AnimationUtils.loadAnimation(this,R.anim.slide_in_forward));
        String url=getIntent().getStringExtra("url");
        switch_cam = (TextView) findViewById(R.id.button2);
        switch_cam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (numOfCam > 1) {
                    mPreview.stop();
                    SurView.removeAllViews();
                    if (currentCamera == Camera.CameraInfo.CAMERA_FACING_BACK) {
                        currentCamera = cameraFront;
                        Toast.makeText(CameraActivity.this, "Front Camera", Toast.LENGTH_SHORT).show();
                    } else if (currentCamera == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                        currentCamera = cameraBack;
                        Toast.makeText(CameraActivity.this, "Back Camera", Toast.LENGTH_SHORT).show();
                    }
                    createCameraPreview(currentCamera);
                }
            }
        });
        findViewById(R.id.button3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preview.setVisibility(View.INVISIBLE);
                btn.setVisibility(View.VISIBLE);
                buttons.setVisibility(View.GONE);
            }
        });
        findViewById(R.id.button4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File tmpFile = Save(true);
                Intent view = new Intent(Intent.ACTION_SEND);
                view.setType("image/*");
                view.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(tmpFile));
                CameraActivity.this.startActivity(view);
            }
        });
        CamView = (RelativeLayout) findViewById(R.id.camview);//RELATIVELAYOUT OR
        //ANY LAYOUT OF YOUR XML

        SurView = (RelativeLayout) findViewById(R.id.sview);//SURFACEVIEW FOR THE PREVIEW
        camera_image = (ImageView) findViewById(R.id.camera_image);//NEEDED FOR THE PREVIEW
        preview = (ImageView) findViewById(R.id.imageView2);
        int screenSize = getScreenWidth(this);
        param = (RelativeLayout.LayoutParams) camera_image.getLayoutParams();
        param.width = screenSize;
        param.height = screenSize;
            btn = (ImageButton) findViewById(R.id.button1); //THE BUTTON FOR TAKING PICTURE
        btn.setOnClickListener(new View.OnClickListener() {    //THE BUTTON CODE
            public void onClick(View v) {
                if(mPreview.getmCamera()!=null)
                mPreview.getmCamera().takePicture(null, null, mPicture);//TAKING THE PICTURE
            }
        });
        findViewById(R.id.textView16).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File saved= Save(false);
                Toast.makeText(CameraActivity
                .this,"File stored in"+ saved.getAbsolutePath(), Toast.LENGTH_LONG).show();
            }
        });
        //url="http://192.168.101.1:8081/ben/ben.png";
        ImageLoader.getInstance().displayImage(url, (ImageView) findViewById(R.id.camera_image), new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String s, View view) {

            }

            @Override
            public void onLoadingFailed(String s, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                if(!(Configuration.ORIENTATION_LANDSCAPE== getResources().getConfiguration().orientation)){
                    float proportion= (float)bitmap.getWidth()/bitmap.getHeight();
                    int alto= (int) (Utils.getScreenWidth(CameraActivity.this)/proportion);
                    RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams)findViewById(R.id.camera_image).getLayoutParams();
                    lp.height=alto;
                    lp.width=Utils.getScreenWidth(CameraActivity.this);
                    findViewById(R.id.camera_image).setLayoutParams(lp);
                    //CamView.setLayoutParams(lp);
                    paramS = (RelativeLayout.LayoutParams) SurView.getLayoutParams();
                    paramS.height=alto;
                    paramS.width=Utils.getScreenWidth(CameraActivity.this)/2;
                    SurView.setLayoutParams(lp);
                    preview.setLayoutParams(lp);
                    if(Permission.hasPermission(CameraActivity.this, Manifest.permission.CAMERA)) {
                        load();
                    }else{
                        ArrayList<String> perm= new ArrayList<>();
                        perm.add(Manifest.permission.CAMERA);
                        askPermision(perm,1);
                    }
                }
                else{
                    float proportion= (float)bitmap.getHeight()/bitmap.getWidth();
                    int alto= (int) (Utils.getScreenDimensions(CameraActivity.this)[0]/proportion);
                    RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams)findViewById(R.id.camera_image).getLayoutParams();
                    lp.height=Utils.getScreenWidth(CameraActivity.this);
                    lp.width=alto;
                    findViewById(R.id.camera_image).setLayoutParams(lp);
//                    CamView.setLayoutParams(lp);
                    paramS = (RelativeLayout.LayoutParams) SurView.getLayoutParams();
                    paramS.height=alto;
                    paramS.width=Utils.getScreenWidth(CameraActivity.this)/2;
                    SurView.setLayoutParams(lp);
                    preview.setLayoutParams(lp);
                    if(Permission.hasPermission(CameraActivity.this, Manifest.permission.CAMERA)) {
                        load();
                    }else{
                        ArrayList<String> perm= new ArrayList<>();
                        perm.add(Manifest.permission.CAMERA);
                        askPermision(perm,1);
                    }
                }
            }

            @Override
            public void onLoadingCancelled(String s, View view) {

            }
        });
    }

    public  File Save(boolean temp) {
        CamView.destroyDrawingCache();
        CamView.buildDrawingCache();
        File tmpFile;
        if(temp){
            File dir_image2 = new File(Environment.getExternalStorageDirectory() +
                    File.separator + "Ultimate Entity Detector");
            dir_image2.mkdirs();  //AGAIN CHOOSING FOLDER FOR THE PICTURE(WHICH IS LIKE A SURFACEVIEW
            tmpFile = new File(dir_image2, "TempGhost.jpg"); //MAKING A FILE IN THE PATH
////            //dir_image2(SEE RIGHT ABOVE) AND NAMING IT "TempGhost.jpg" OR ANYTHING ELSE
        }else{
            File dir_image2 = new File(Constants.appRootDir+File.separator + "images");
            if(!dir_image2.exists())
            dir_image2.mkdirs();  //AGAIN CHOOSING FOLDER FOR THE PICTURE(WHICH IS LIKE A SURFACEVIEW
            tmpFile = new File(dir_image2, System.currentTimeMillis()+".jpg"); //MAKING A FILE IN THE PATH
        }
        try { //SAVING
            FileOutputStream fos = new FileOutputStream(tmpFile);
            Bitmap result = CamView.getDrawingCache();
            result.compress(Bitmap.CompressFormat.JPEG, 70, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();

        }
        return tmpFile;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(Permission.hasPermission(this,Manifest.permission.CAMERA))
            load();
        else{
            Permission.fail(this);
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(load)
            load();
    }

    private void load() {
        numOfCam = Camera.getNumberOfCameras();
        Camera.CameraInfo info = new Camera.CameraInfo();
        for (int i = 0; i < numOfCam; i++) {
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT)
                cameraFront = i;
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK)
                cameraBack = i;
        }
        if (currentCamera != -1)
            createCameraPreview(currentCamera);
        if (currentCamera == -1 && cameraFront != -1) {
            currentCamera = cameraFront;
            createCameraPreview(cameraFront);
        }else
        if (currentCamera == -1 && cameraBack != -1) {
            currentCamera = cameraBack;
            createCameraPreview(currentCamera);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(Permission.hasPermission(this,Manifest.permission.CAMERA)){
        mPreview.stop();
        SurView.removeAllViews();
            load=true;
        }
    }

    public static int getScreenWidth(Activity activity) {

        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();

        int width;
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB_MR2) {
            display.getSize(size);
            width = size.x;
        } else {
            width = display.getWidth();

        }
        int screenWidth = width;

        return width;
    }

    private void createCameraPreview(int cam) {
        // Set the second argument by your choice.
        // Usually, 0 for back-facing camera, 1 for front-facing camera.
        // If the OS is pre-gingerbreak, this does not have any effect.
        mPreview = new CameraPreview(this, cam, CameraPreview.LayoutMode.FitToParent);
        RelativeLayout.LayoutParams previewLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        SurView.addView(mPreview);

    }

    private Camera.PictureCallback mPicture = new Camera.PictureCallback() {   //THIS METHOD AND THE METHOD BELOW


        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            preview.setVisibility(View.VISIBLE);
            Glide.with(CameraActivity.this).load(data).into(preview);
            buttons.setVisibility(View.VISIBLE);
            btn.setVisibility(View.GONE);
        }
    };
}
