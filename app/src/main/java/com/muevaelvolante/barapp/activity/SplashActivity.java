package com.muevaelvolante.barapp.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.AssetFileDescriptor;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.Toast;

import com.lacostra.utils.notification.Permission.Permission;
import com.lacostra.utils.notification.Permission.PermissionActivity;
import com.muevaelvolante.Aplication;
import com.muevaelvolante.barapp.HomeActivity;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.activity.login.LoginActivity;
import com.muevaelvolante.barapp.commons.Constants;

import com.muevaelvolante.barapp.custom.CustomTextureVideoView;
import com.muevaelvolante.barapp.util.Utils;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import io.fabric.sdk.android.Fabric;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

public class SplashActivity extends PermissionActivity {

     private String token;
    android.widget.VideoView videoView;
    CustomTextureVideoView customTextureVideoView;
    private MediaPlayer mMediaPlayer;

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (Permission.hasPermission(SplashActivity.this, Permission.WRITE_EXTERNAL)) {
            Intent launch = getIntent();
            if (!TextUtils.isEmpty(token) || launch.getExtras() != null) {
                Intent home = new Intent(SplashActivity.this, HomeActivity.class);
                if (launch.getExtras() != null)
                    home.putExtra("section", launch.getExtras().getString("section"));
                startActivity(home);
                finish();
            } else {
                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                finish();
            }
        } else {
            final AlertDialog.Builder diaBuilder = new AlertDialog.Builder(this);
            diaBuilder.setTitle("Warning!")
                    .setMessage("The requested permisson is necesary for the aplication cache system. Please " +
                            "enable the permission on the mobile setting section.")
                    .setPositiveButton("Ask Again", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ArrayList<String> perms = new ArrayList<String>();
                            perms.add(Permission.WRITE_EXTERNAL);
                            askPermision(perms, 1);
                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton("Continue", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (!TextUtils.isEmpty(token)) {
                                startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                                finish();
                            } else {
                                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                                finish();
                            }
                            dialog.dismiss();
                        }
                    }).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
//        try {
//            PackageInfo info = getPackageManager().getPackageInfo(
//                    "com.muevaelvolante.barapp",
//                    PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                String sign = Base64
//                        .encodeToString(md.digest(), Base64.DEFAULT);
//                Log.e("KEYHASH:", sign);
//            }
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        }
        videoView = (android.widget.VideoView) findViewById(R.id.videoView);
//        direccion del video del splash
        final Uri videoURI= Uri.parse("android.resource://"+getPackageName()+"/"+ R.raw.video);
        videoView.setVideoURI(videoURI);
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                launchNext();
            }
        });
        videoView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                launchNext();
                return false;
            }
        });
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setVolume(0,0);
                videoView.setVisibility(View.VISIBLE);
                videoView.requestFocus();
                videoView.bringToFront();
                videoView.setZOrderOnTop(true);
            }
        });
        videoView.start();
        videoView.setZOrderOnTop(true);

    }

    void launchNext() {
        if(mMediaPlayer!=null)
            mMediaPlayer.release();
        SharedPreferences pref = getSharedPreferences(Constants.LOGIN_DATA, MODE_PRIVATE);
        token = pref.getString(Constants.LOGIN_DATA_ACCES_TOKEN, "");
        if (Patterns.EMAIL_ADDRESS.matcher(token).matches())
            Aplication.isLoginFromWeb = true;

            Intent launch = getIntent();
//            if (!TextUtils.isEmpty(token)) {
                Intent home = new Intent(SplashActivity.this, HomeActivity.class);
                if (launch.getExtras() != null)
                    home.putExtra("section", launch.getExtras().getString("section"));
                startActivity(home);
                finish();
//            } else {
//                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
//                finish();
//            }

    }


}
