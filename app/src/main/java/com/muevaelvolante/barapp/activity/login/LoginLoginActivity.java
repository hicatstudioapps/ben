package com.muevaelvolante.barapp.activity.login;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.muevaelvolante.Aplication;
import com.muevaelvolante.barapp.HomeActivity;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.commons.Constants;
import com.muevaelvolante.barapp.interfaces.IRegistrationAndLogin;
import com.muevaelvolante.barapp.model.login.LoginResult;
import com.muevaelvolante.barapp.model.setting.Login;
import com.muevaelvolante.barapp.services.ServiceGenerator;

import org.json.JSONObject;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginLoginActivity extends AppCompatActivity {

    @Bind(R.id.email)
    EditText email;
    @Bind(R.id.passw)
    EditText password;
    @Bind(R.id.login)
    View loginButton;
    @Bind(R.id.email2)
    EditText emailReset;
    @Bind(R.id.button4)
    View resetButton;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_login);
        ButterKnife.bind(this);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //valido campos no nulos
                if (    !TextUtils.isEmpty(email.getText().toString()) ||
                        !TextUtils.isEmpty(password.getText().toString())) {
                    //valido email
                    if(!Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches() ||
                            email.getText().toString().toLowerCase().equals("test@error.com")){
                        Toast.makeText(LoginLoginActivity.this,"Invalid email address!!",Toast.LENGTH_LONG).show();
                        return;
                    }
                    showProgressDialog();
                    IRegistrationAndLogin registrationAndLogin = ServiceGenerator.createService(IRegistrationAndLogin.class);
                    Map<String, String> mapData = new LinkedHashMap<String, String>();
                    mapData.put("tableName", "login");
                    mapData.put("email", email.getText().toString());
                    mapData.put("password", password.getText().toString());
                    Call<LoginResult> call = registrationAndLogin.login("http://mueva.eu/BARapp/index.php", mapData);
//                    Call<LoginResult> call = registrationAndLogin.login("http://192.168.101.1:8081/ben/en/login.json", mapData);
                    call.enqueue(new Callback<LoginResult>() {
                        @Override
                        public void onResponse(Call<LoginResult> call, Response<LoginResult> response) {
                            if (response.isSuccessful()) {
                                try {
                                    LoginResult loginResult= response.body();
                                  if(loginResult.getSuccess()){
                                       String mail= loginResult.getUserData().getEmail();
                                       String name= loginResult.getUserData().getFirstname() +" "+loginResult.getUserData().getLastname();
                                    //como todo fue ok, almaceno el email como identity id
                                    SharedPreferences pref= getSharedPreferences(Constants.LOGIN_DATA, MODE_PRIVATE);
                                    SharedPreferences.Editor edit=pref.edit();
                                    edit.putString(Constants.LOGIN_DATA_ACCES_TOKEN,email.getText().toString()+"");
                                       edit.putString(Constants.LOGIN_DATA_EMAIL,mail);
                                       edit.putString(Constants.LOGIN_DATA_USER,name);
                                    edit.commit();
                                    Aplication.isAnonimous=false;
                                      Aplication.isLoginFromWeb=true;
                                    Intent home= new Intent(LoginLoginActivity.this, HomeActivity.class);
                                    startActivity(home);
                                    if(Aplication.loginActivity!=null)
                                        Aplication.loginActivity.finish();
                                    LoginLoginActivity.this.finish();
                                   }else{
                                      Toast.makeText(getApplicationContext(),"Login with server fails",Toast.LENGTH_SHORT).show();
                                      hideProgressDialog();
                                  }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                hideProgressDialog();
                                showNoConnectionError();
                            }
                        }
                        @Override
                        public void onFailure(Call<LoginResult> call, Throwable t) {
                            hideProgressDialog();
                            showNoConnectionError();
                        }
                    });
                }else{
                    Toast.makeText(LoginLoginActivity.this,"All fields must be filled..",Toast.LENGTH_LONG).show();
                }
            }
        });

        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (    !TextUtils.isEmpty(emailReset.getText().toString())) {
                    //valido email
                    if(!Patterns.EMAIL_ADDRESS.matcher(emailReset.getText().toString()).matches() ||
                            email.getText().toString().toLowerCase().equals("test@error.com")){
                        Toast.makeText(LoginLoginActivity.this,"Invalid email address!!",Toast.LENGTH_LONG).show();
                        return;
                    }
                    showProgressDialog();
                    IRegistrationAndLogin registrationAndLogin = ServiceGenerator.createService(IRegistrationAndLogin.class);
                    Map<String, String> mapData = new LinkedHashMap<String, String>();
                    mapData.put("tableName", "forgotpass");
                    mapData.put("email", email.getText().toString());
                    Call<ResponseBody> call = registrationAndLogin.recoverPassw("http://mueva.eu/BARapp/index.php", mapData);
                    call.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.isSuccessful()) {
                                try {
                                    Log.d("result", response.body().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                hideProgressDialog();
                                showNoConnectionError();
                            }
                        }
                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            hideProgressDialog();
                            showNoConnectionError();
                        }
                    });
                }else{
                    Toast.makeText(LoginLoginActivity.this,"Email field must be filled..",Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Sending data...");
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    public void showNoConnectionError(){
        AlertDialog.Builder dialog= new AlertDialog.Builder(this);
        dialog.setMessage("You have no internet connection!!");
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                LoginLoginActivity.this.finish();
            }
        });
        dialog.show();
    }

}
