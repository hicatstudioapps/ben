package com.muevaelvolante.barapp.fragment;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.amazonaws.com.google.gson.Gson;
import com.muevaelvolante.barapp.HomeActivity;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.adapters.AC.ACAdapter;
import com.muevaelvolante.barapp.adapters.boat.BoatAdapter;
import com.muevaelvolante.barapp.interfaces.IBoat;
import com.muevaelvolante.barapp.interfaces.ISettings;
import com.muevaelvolante.barapp.model.AC.AC;
import com.muevaelvolante.barapp.model.boat.Boat;
import com.muevaelvolante.barapp.services.ServiceGenerator;

import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BoatFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BoatFragment extends RootFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String URL = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ProgressDialog mProgressDialog;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.vRV)
    RecyclerView videosRv;


    public BoatFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static BoatFragment newInstance(String param1, String param2) {
        BoatFragment fragment = new BoatFragment();
        Bundle args = new Bundle();
        args.putString(URL, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            uri = getArguments().getString(URL);
            json_name = getArguments().getString(ARG_PARAM2);
            dialog_text="Loading content...";
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout= inflater.inflate(R.layout.fragment_boat, container, false);
        ButterKnife.bind(this,layout);
        toolbar.setTitle(" ");
        setHasOptionsMenu(true);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.back));
        videosRv.setLayoutManager(new LinearLayoutManager(getActivity()));
        return layout;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        showProgressDialog();
//        IBoat ACService= ServiceGenerator.createService(IBoat.class);
//        Call<Boat> call= ACService.getBoat(mParam1);
//        call.enqueue(new Callback<Boat>() {
//            @Override
//            public void onResponse(Call<Boat> call, Response<Boat> response) {
//                if (response.isSuccessful()) {
//                   try {
//                        Boat b= response.body();
//                      //  Log.d("result",ac);
//                        videosRv.setAdapter(new BoatAdapter(b.getBoats(), (AppCompatActivity) getActivity()));
//                        hideProgressDialog();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Boat> call, Throwable t) {
//                hideProgressDialog();
//                showNoConnectionError();
//            }
//        });
    }

    @Override
    public void proccessData(String data) {
        Boat b= new Gson().fromJson(data,Boat.class);
        //  Log.d("result",ac);
        videosRv.setAdapter(new BoatAdapter(b.getBoats(), (AppCompatActivity) getActivity()));
        hideDialog();
    }

    public void showNoConnectionError(){
        AlertDialog.Builder dialog= new AlertDialog.Builder(getActivity());
        dialog.setMessage("You have no internet connection!!");
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            ((HomeActivity)getContext()).showHome();
        }
        return true;
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage("Loading content...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }
}
