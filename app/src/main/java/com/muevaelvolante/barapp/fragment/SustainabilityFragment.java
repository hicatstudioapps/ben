package com.muevaelvolante.barapp.fragment;


import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.DefaultValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.internal.ObjectConstructor;
import com.muevaelvolante.Aplication;
import com.muevaelvolante.barapp.HomeActivity;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.activity.GalleryActivity;
import com.muevaelvolante.barapp.activity.NewsDetailActivity;
import com.muevaelvolante.barapp.activity.PledgeActivity;
import com.muevaelvolante.barapp.interfaces.ISustainability;
import com.muevaelvolante.barapp.model.home.Gallery;
import com.muevaelvolante.barapp.model.home.HomeNews;
import com.muevaelvolante.barapp.model.home.Medium___;
import com.muevaelvolante.barapp.model.sustainability.Article;
import com.muevaelvolante.barapp.model.sustainability.MoreContent;
import com.muevaelvolante.barapp.model.sustainability.Pledge;
import com.muevaelvolante.barapp.model.sustainability.Sustiantability;
import com.muevaelvolante.barapp.model.sustainability.Topstory;
import com.muevaelvolante.barapp.services.ServiceGenerator;
import com.muevaelvolante.barapp.util.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.viewpagerindicator.CirclePageIndicator;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;

/**
 * A simple {@link Fragment} subclass.
 *
 * create an instance of this fragment.
 */
public class SustainabilityFragment extends RootFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String url;
    private String mParam2;
    private Sustiantability sustainability;
    private ProgressDialog mProgressDialog;
    private String tittle;


    public SustainabilityFragment() {
        // Required empty public constructor
    }

    public static SustainabilityFragment newInstance(String url, String tittle) {
        SustainabilityFragment fragment = new SustainabilityFragment();
        Bundle args = new Bundle();
        args.putString("url", url);
        args.putString("tittle",tittle);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            uri = getArguments().getString("url");
            tittle= getArguments().getString("tittle");
            json_name="sust";
            dialog_text="Loading content...";
        }
    }

    @Override
    public void proccessData(String data) {
        this.sustainability= new Gson().fromJson(data, Sustiantability.class);
        //aqui mando a ejecutar la carga de los demas contenidos
        //primero cargamos los main text de los article
        List<Article> articles = sustainability.getArticles();
        for (int i = 0; i < articles.size(); i++) {
            Article art = articles.get(i);
            if (art.getSection().toLowerCase().equals("main text")) {
                main_text.setText(Html.fromHtml(art.getText()));
                main_text.setTypeface(Aplication.robotoR);
                main_title.setText(art.getTitle());
                main_title.setTypeface(Aplication.robotoB);
            }
        }
        //ahora los sponsor
        sponsor_text.setTypeface(Aplication.robotoL);
        ImageLoader.getInstance().displayImage(sustainability.getSponsors().get(0).getMedia().get(0).getMed(), sponsor1);
        sponsor1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.openURL(getActivity(), sustainability.getSponsors().get(0).getUrl());
            }
        });
        if(sustainability.getSponsors().size()>1)
            ImageLoader.getInstance().displayImage(sustainability.getSponsors().get(1).getMedia().get(0).getMed(), sponsor2);
        sponsor2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.openURL(getActivity(), sustainability.getSponsors().get(2).getUrl());
            }
        });
        //aqui los black feature

//                ImageLoader.getInstance().displayImage(articles.get(1).getMedia().get(0).getMed(), bf_image1);
//                bf_text1.setText(articles.get(1).getTitle());
//                bf_text1.setTypeface(Aplication.robotoR);
//                ImageLoader.getInstance().displayImage(articles.get(2).getMedia().get(0).getMed(), bf_image2);
//                bf_text2.setText(articles.get(2).getTitle());
//                bf_text2.setTypeface(Aplication.robotoR);

        //aqui el grafico
        // creating data values
        ArrayList<Entry> entries = new ArrayList<>();
        ArrayList<String> axis = new ArrayList<String>();
        ArrayList<Integer> colors = new ArrayList<>();
        for (Pledge p : sustainability.getPledge()) {
            entries.add(new Entry(Float.parseFloat(p.getVotes()), Integer.parseInt(p.getID())));
            axis.add(p.getPledge());
            int defColor = Color.parseColor("#456325");
            //aqui voy poniedo la leyenda
            View layout_legend = getActivity().getLayoutInflater().inflate(R.layout.section_sust_graph_leyend, null);
            if (p.getColor().equals("")) {
                layout_legend.findViewById(R.id.leyend_color).setBackgroundColor(defColor);
                colors.add(defColor);
            } else {
                colors.add(Color.parseColor(p.getColor()));
                Drawable d= getResources().getDrawable(R.drawable.item_leyend_circle);
                d.setColorFilter(Color.parseColor(p.getColor()), PorterDuff.Mode.SRC_ATOP);
                layout_legend.findViewById(R.id.leyend_color).setBackgroundDrawable(d);
            }
            ((TextView) layout_legend.findViewById(R.id.leyend_name)).setText(p.getPledge());
            legend.addView(layout_legend);
        }
        mChart.setDescription("");
        ///mChart.setExtraOffsets(5, 10, 5, 5);

        mChart.setDragDecelerationFrictionCoef(0.95f);
        mChart.setDrawHoleEnabled(true);
        mChart.setHoleColor(getActivity().getResources().getColor(R.color.colorPrimary));
        mChart.setHoleRadius(40f);
        mChart.setTransparentCircleRadius(40f);
        mChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        mChart.setRotationEnabled(true);
        mChart.setHighlightPerTapEnabled(true);
        mChart.setDrawSliceText(false);
        setData(3, 100, colors);
        mChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        mChart.getLegend().setEnabled(false);
        //creo el adapter para el pager y su indicator
        int screenWidth = Utils.getScreenWidth((Activity) getContext());
        int height= (int) (screenWidth*0.7);
        RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams) pager.getLayoutParams();
        lp.width=screenWidth;
        lp.height=height;
        pager.setLayoutParams(lp);
        pager.setAdapter(new PagerAdapter());
        slide_text.setText(Html.fromHtml(sustainability.getTopstories().get(0).getText()));
        final Animation out= AnimationUtils.loadAnimation(getContext(),android.R.anim.fade_out);
        final Animation in= AnimationUtils.loadAnimation(getContext(),android.R.anim.fade_in);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(final int position) {
                int from= slide_text.getMeasuredHeight();
                slide_text_temp.setText(sustainability.getTopstories().get(position).getText());
                int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(Utils.getScreenWidth(getActivity()), View.MeasureSpec.AT_MOST);
                int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                slide_text_temp.measure(widthMeasureSpec,heightMeasureSpec);
                int to= slide_text_temp.getMeasuredHeight();
                final ValueAnimator slideAnimator = ValueAnimator
                        .ofInt(from, to)
                        .setDuration(200);
                slideAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        Integer value = (Integer) animation.getAnimatedValue();
                        slide_text.getLayoutParams().height = value.intValue();
                        slide_text.requestLayout();
                    }
                });
                AnimatorSet set = new AnimatorSet();
                set.play(slideAnimator);
                set.setInterpolator(new AccelerateDecelerateInterpolator());
                set.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        slide_text.setAnimation(out);
                        slide_text.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        slide_text.setText(Html.fromHtml(sustainability.getTopstories().get(position).getText()));
                        slide_text.setAnimation(in);
                        slide_text.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
                set.start();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        pager.setOffscreenPageLimit(20);
        indicator.setViewPager(pager);
        pledge_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SustainabilityFragment.this.getActivity(), PledgeActivity.class));
            }
        });
        //los more content
        View article=null;
        View video=null;
        View gallery=null;
        for (int i = 0; i < sustainability.getMoreContents().size(); i++) {
            final MoreContent moreContent= sustainability.getMoreContents().get(i);
            View item = ((LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.sustainability_more_content,null);
            final ImageView image= (ImageView) item.findViewById(R.id.image);
//                    image.setVisibility(View.GONE);
            TextView tittle= (TextView) item.findViewById(R.id.title);
            ImageView play= (ImageView)item.findViewById(R.id.play);
            item.findViewById(R.id.date).setVisibility(View.GONE);
            item.findViewById(R.id.section).setVisibility(View.GONE);
            tittle.setText(moreContent.getTittle());
            final LinkedTreeMap<String,String > map= (LinkedTreeMap<String, String>) moreContent.getMedia().get(0);
            height= (int) ((screenWidth)*0.6);
            lp= (RelativeLayout.LayoutParams) image.getLayoutParams();
            lp.width=screenWidth;
            lp.height=height;
            image.setMinimumHeight(height / 1);
            image.setMaxHeight(height / 1);
            image.setAdjustViewBounds(false);
            image.setLayoutParams(lp);
            image.setMaxHeight(height);
            //item.setLayoutParams(lp);
            ImageLoader.getInstance().displayImage(map.get("med"), image, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String s, View view) {

                }

                @Override
                public void onLoadingFailed(String s, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                    float proportion= (float)bitmap.getWidth()/bitmap.getHeight();
                    int alto= (int) (Utils.getScreenWidth(getActivity())/proportion);
                    RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams) image.getLayoutParams();
                    lp.height=alto;
                    lp.width=Utils.getScreenWidth(getActivity());
                    image.setLayoutParams(lp);
                }

                @Override
                public void onLoadingCancelled(String s, View view) {

                }
            });
            //detect content type
            if(moreContent.getType().toLowerCase().equals("article")){
                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        HomeNews homeNews= new HomeNews();
                        List<Object> text=new ArrayList<Object>();
                        text.add(moreContent.getText());
                        homeNews.setText(text);
                        homeNews.setMedia(moreContent.getMedia());
                        homeNews.setTitle(moreContent.getTittle());
                        Intent intent= new Intent(getContext(), NewsDetailActivity.class);
                        Bundle bundle= new Bundle();
                        bundle.putSerializable("news",homeNews);
                        intent.putExtras(bundle);
                        getContext().startActivity(intent);
                    }
                });
                play.setVisibility(View.GONE);
                article=item;
            }
            else if(moreContent.getType().toLowerCase().equals("video")){
                play.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Utils.playVideoYoutube(getContext(), map.get("video"));
                    }
                });
                video=item;

            }else if(moreContent.getType().toLowerCase().equals("gallery")){
                play.setImageResource(R.drawable.gallery);
                play.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Gallery gal= new Gallery();
                        List<Medium___> medium___s= new ArrayList<Medium___>();
                        for (int i = 0; i < moreContent.getMedia().size(); i++) {
                            LinkedTreeMap<String,String > map= (LinkedTreeMap<String, String>) moreContent.getMedia().get(i);
                            Medium___ temp= new Medium___();
                            temp.setMed(map.get("med"));
                            medium___s.add(temp);
                        }
                        gal.setMedia(medium___s);
                        Intent galery= new Intent(getContext(),GalleryActivity.class);
                        Bundle data= new Bundle();
                        data.putSerializable("gal",gal);
                        galery.putExtras(data);
                        getContext().startActivity(galery);
                    }
                });
                gallery=item;
            }
            // container.addView(item);
        }
        if(article!=null){
            View header= ((LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.section_sust_partner,null);
            ((TextView)header.findViewById(R.id.sponsor_text)).setText("Case Study");
            container.addView(header);
            container.addView(article);}
        if(video!=null){
            View header= ((LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.section_sust_partner,null);
            ((TextView)header.findViewById(R.id.sponsor_text)).setText("Video");
            container.addView(header);
            container.addView(video);
        }
        if(gallery!=null) {
            View header= ((LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.section_sust_partner,null);
            ((TextView)header.findViewById(R.id.sponsor_text)).setText("Photo Gallery");
            container.addView(header);
            container.addView(gallery);
        }
        allView.setAnimation(AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_in));
        allView.setVisibility(View.VISIBLE);
        pledge_button.setVisibility(View.VISIBLE);
        hideDialog();
    }

    private void setData(int count, int range,ArrayList<Integer> colors) {
        //aqui pongo los pladges para poder trabajar desde el dialogo
        Aplication.pledge=sustainability.getPledge();
        ArrayList<Entry> yVals1 = new ArrayList<Entry>();
        ArrayList<String> xVals = new ArrayList<String>();

        float mult = range;
        // IMPORTANT: In a PieChart, no values (Entry) should have the same
        // xIndex (even if from different DataSets), since no values can be
        // drawn above each other.
        for (int i = 0; i < sustainability.getPledge().size(); i++) {
            yVals1.add(new Entry(Float.parseFloat(sustainability.getPledge().get(i).getVotes()), i));
            xVals.add(sustainability.getPledge().get(i).getPledge());
        }

        PieDataSet dataSet = new PieDataSet(yVals1, "");
        dataSet.setSliceSpace(2f);
        dataSet.setSelectionShift(5f);

        // add a lot of colors
        dataSet.setColors(colors);

        PieData data = new PieData(xVals, dataSet);
        data.setValueFormatter(new Formatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);
        data.setDrawValues(true);
        mChart.setData(data);
        // undo all highlights
        mChart.highlightValues(null);

        // mChart.invalidate();
    }

    @Bind(R.id.content)
    LinearLayout container;
    @Bind(R.id.pager)
    ViewPager pager;
    @Bind(R.id.indicator)
    CirclePageIndicator indicator;

    //main text
    @Bind(R.id.main_text)
    TextView main_text;
    @Bind(R.id.main_title)
    TextView main_title;

    //sponsor
    @Bind(R.id.sponsor1)
    ImageView sponsor1;
    @Bind(R.id.sponsor2)
    ImageView sponsor2;
    @Bind(R.id.sponsor_text)
    TextView sponsor_text;

    //black features
//    @Bind(R.id.bf_image1)
//    ImageView bf_image1;
//    @Bind(R.id.bf_image2)
//    ImageView bf_image2;
//@Bind(R.id.bf_text1)
//TextView bf_text1;
//    @Bind(R.id.bf_text2)
//    TextView bf_text2;

//PIE CHART
    @Bind(R.id.chart)
    PieChart mChart;
    @Bind(R.id.graph_title)
    TextView graph_title;
    @Bind(R.id.legend)
    LinearLayout legend;

    //video
//    @Bind(R.id.image_video)
//    ImageView video_image;
//    @Bind(R.id.title_video)
//    TextView title_video;

    @Bind(R.id.pledge_button)
    ImageView pledge_button;

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.scrollView3)
    View allView;
    @Bind(R.id.textView27)
            TextView tittleTv;
    @Bind(R.id.slide_text)
            TextView slide_text;
    @Bind(R.id.slide_text_temp)
    TextView slide_text_temp;

    ISustainability service;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout= inflater.inflate(R.layout.fragment_sustainability, container, false);
        ButterKnife.bind(this,layout);
        tittleTv.setText(tittle);
        setHasOptionsMenu(true);
        toolbar.setTitle(" ");
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.back));

        //showProgressDialog();
        return layout;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            ((HomeActivity)getActivity()).showHome();
        }
        return true;
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage("Loading content...");
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

//    class LoadSlide extends AsyncTask<Void,Void,Sustiantability>{
//
//        @Override
//        protected Sustiantability doInBackground(Void... params) {
//
////            Call<Sustiantability> serviceCall= service.getSust("http://192.168.101.1:8081/ben/en/app/sustainability.json");
//            Call<Sustiantability> serviceCall= service.getSust(url);
//try {
//                Sustiantability sustainability= serviceCall.execute().body();
//                return sustainability;
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(final Sustiantability sustainability) {
//            super.onPostExecute(sustainability);
//            if(sustainability != null) {
//                SustainabilityFragment.this.sustainability = sustainability;
//                //aqui mando a ejecutar la carga de los demas contenidos
//                //primero cargamos los main text de los article
//                List<Article> articles = sustainability.getArticles();
//                for (int i = 0; i < articles.size(); i++) {
//                    Article art = articles.get(i);
//                    if (art.getSection().toLowerCase().equals("main text")) {
//                        main_text.setText(Html.fromHtml(art.getText()));
//                        main_text.setTypeface(Aplication.robotoR);
//                        main_title.setText(art.getTitle());
//                        main_title.setTypeface(Aplication.robotoB);
//                    }
//                }
//                //ahora los sponsor
//                sponsor_text.setTypeface(Aplication.robotoL);
//                ImageLoader.getInstance().displayImage(sustainability.getSponsors().get(0).getMedia().get(0).getMed(), sponsor1);
//                sponsor1.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Utils.openURL(getActivity(), sustainability.getSponsors().get(0).getUrl());
//                    }
//                });
//                if(sustainability.getSponsors().size()>1)
//                ImageLoader.getInstance().displayImage(sustainability.getSponsors().get(1).getMedia().get(0).getMed(), sponsor2);
//                sponsor2.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Utils.openURL(getActivity(), sustainability.getSponsors().get(2).getUrl());
//                    }
//                });
//                //aqui los black feature
//
////                ImageLoader.getInstance().displayImage(articles.get(1).getMedia().get(0).getMed(), bf_image1);
////                bf_text1.setText(articles.get(1).getTitle());
////                bf_text1.setTypeface(Aplication.robotoR);
////                ImageLoader.getInstance().displayImage(articles.get(2).getMedia().get(0).getMed(), bf_image2);
////                bf_text2.setText(articles.get(2).getTitle());
////                bf_text2.setTypeface(Aplication.robotoR);
//
//                //aqui el grafico
//                // creating data values
//                ArrayList<Entry> entries = new ArrayList<>();
//                ArrayList<String> axis = new ArrayList<String>();
//                ArrayList<Integer> colors = new ArrayList<>();
//                for (Pledge p : sustainability.getPledge()) {
//                    entries.add(new Entry(Float.parseFloat(p.getVotes()), Integer.parseInt(p.getID())));
//                    axis.add(p.getPledge());
//                    int defColor = Color.parseColor("#456325");
//                    //aqui voy poniedo la leyenda
//                    View layout_legend = getActivity().getLayoutInflater().inflate(R.layout.section_sust_graph_leyend, null);
//                    if (p.getColor().equals("")) {
//                        layout_legend.findViewById(R.id.leyend_color).setBackgroundColor(defColor);
//                        colors.add(defColor);
//                    } else {
//                        colors.add(Color.parseColor(p.getColor()));
//                        Drawable d= getResources().getDrawable(R.drawable.item_leyend_circle);
//                        d.setColorFilter(Color.parseColor(p.getColor()), PorterDuff.Mode.SRC_ATOP);
//                        layout_legend.findViewById(R.id.leyend_color).setBackgroundDrawable(d);
//                    }
//                    ((TextView) layout_legend.findViewById(R.id.leyend_name)).setText(p.getPledge());
//                    legend.addView(layout_legend);
//                }
//                mChart.setDescription("");
//                ///mChart.setExtraOffsets(5, 10, 5, 5);
//
//                mChart.setDragDecelerationFrictionCoef(0.95f);
//                mChart.setDrawHoleEnabled(true);
//                mChart.setHoleColor(getActivity().getResources().getColor(R.color.colorPrimary));
//                mChart.setHoleRadius(40f);
//                mChart.setTransparentCircleRadius(40f);
//                mChart.setRotationAngle(0);
//                // enable rotation of the chart by touch
//                mChart.setRotationEnabled(true);
//                mChart.setHighlightPerTapEnabled(true);
//                mChart.setDrawSliceText(false);
//                setData(3, 100, colors);
//                mChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);
//                mChart.getLegend().setEnabled(false);
//                //creo el adapter para el pager y su indicator
//                int screenWidth = Utils.getScreenWidth((Activity) getContext());
//                int height= (int) (screenWidth*0.7);
//                RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams) pager.getLayoutParams();
//                lp.width=screenWidth;
//                lp.height=height;
//                pager.setLayoutParams(lp);
//                pager.setAdapter(new PagerAdapter());
//                slide_text.setText(Html.fromHtml(sustainability.getTopstories().get(0).getText()));
//                final Animation out= AnimationUtils.loadAnimation(getContext(),android.R.anim.fade_out);
//                final Animation in= AnimationUtils.loadAnimation(getContext(),android.R.anim.fade_in);
//                pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//                    @Override
//                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//                    }
//
//                    @Override
//                    public void onPageSelected(final int position) {
//                        int from= slide_text.getMeasuredHeight();
//                        slide_text_temp.setText(sustainability.getTopstories().get(position).getText());
//                        int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(Utils.getScreenWidth(getActivity()), View.MeasureSpec.AT_MOST);
//                        int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
//                        slide_text_temp.measure(widthMeasureSpec,heightMeasureSpec);
//                        int to= slide_text_temp.getMeasuredHeight();
//                        final ValueAnimator slideAnimator = ValueAnimator
//                                .ofInt(from, to)
//                                .setDuration(200);
//                        slideAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//                            @Override
//                            public void onAnimationUpdate(ValueAnimator animation) {
//                                Integer value = (Integer) animation.getAnimatedValue();
//                                slide_text.getLayoutParams().height = value.intValue();
//                                slide_text.requestLayout();
//                            }
//                        });
//                        AnimatorSet set = new AnimatorSet();
//                        set.play(slideAnimator);
//                        set.setInterpolator(new AccelerateDecelerateInterpolator());
//                        set.addListener(new Animator.AnimatorListener() {
//                            @Override
//                            public void onAnimationStart(Animator animation) {
//                                slide_text.setAnimation(out);
//                                slide_text.setVisibility(View.INVISIBLE);
//                            }
//
//                            @Override
//                            public void onAnimationEnd(Animator animation) {
//                                slide_text.setText(Html.fromHtml(sustainability.getTopstories().get(position).getText()));
//                                slide_text.setAnimation(in);
//                                slide_text.setVisibility(View.VISIBLE);
//                            }
//
//                            @Override
//                            public void onAnimationCancel(Animator animation) {
//
//                            }
//
//                            @Override
//                            public void onAnimationRepeat(Animator animation) {
//
//                            }
//                        });
//                        set.start();
//                    }
//
//                    @Override
//                    public void onPageScrollStateChanged(int state) {
//
//                    }
//                });
//                pager.setOffscreenPageLimit(20);
//                indicator.setViewPager(pager);
//                pledge_button.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        startActivity(new Intent(SustainabilityFragment.this.getActivity(), PledgeActivity.class));
//                    }
//                });
//                //los more content
//                View article=null;
//                View video=null;
//                View gallery=null;
//                for (int i = 0; i < sustainability.getMoreContents().size(); i++) {
//                    final MoreContent moreContent= sustainability.getMoreContents().get(i);
//                    View item = ((LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.sustainability_more_content,null);
//                    final ImageView image= (ImageView) item.findViewById(R.id.image);
////                    image.setVisibility(View.GONE);
//                    TextView tittle= (TextView) item.findViewById(R.id.title);
//                    ImageView play= (ImageView)item.findViewById(R.id.play);
//                    item.findViewById(R.id.date).setVisibility(View.GONE);
//                    item.findViewById(R.id.section).setVisibility(View.GONE);
//                    tittle.setText(moreContent.getTittle());
//                    final LinkedTreeMap<String,String > map= (LinkedTreeMap<String, String>) moreContent.getMedia().get(0);
//                    height= (int) ((screenWidth)*0.6);
//                    lp= (RelativeLayout.LayoutParams) image.getLayoutParams();
//                    lp.width=screenWidth;
//                    lp.height=height;
//                    image.setMinimumHeight(height / 1);
//                    image.setMaxHeight(height / 1);
//                    image.setAdjustViewBounds(false);
//                    image.setLayoutParams(lp);
//                    image.setMaxHeight(height);
//                    //item.setLayoutParams(lp);
//                    ImageLoader.getInstance().displayImage(map.get("med"), image, new ImageLoadingListener() {
//                        @Override
//                        public void onLoadingStarted(String s, View view) {
//
//                        }
//
//                        @Override
//                        public void onLoadingFailed(String s, View view, FailReason failReason) {
//
//                        }
//
//                        @Override
//                        public void onLoadingComplete(String s, View view, Bitmap bitmap) {
//                            float proportion= (float)bitmap.getWidth()/bitmap.getHeight();
//                            int alto= (int) (Utils.getScreenWidth(getActivity())/proportion);
//                            RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams) image.getLayoutParams();
//                            lp.height=alto;
//                            lp.width=Utils.getScreenWidth(getActivity());
//                            image.setLayoutParams(lp);
//                        }
//
//                        @Override
//                        public void onLoadingCancelled(String s, View view) {
//
//                        }
//                    });
//                    //detect content type
//                    if(moreContent.getType().toLowerCase().equals("article")){
//                        image.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                HomeNews homeNews= new HomeNews();
//                                List<Object> text=new ArrayList<Object>();
//                                text.add(moreContent.getText());
//                                homeNews.setText(text);
//                                homeNews.setMedia(moreContent.getMedia());
//                                homeNews.setTitle(moreContent.getTittle());
//                                Intent intent= new Intent(getContext(), NewsDetailActivity.class);
//                                Bundle bundle= new Bundle();
//                                bundle.putSerializable("news",homeNews);
//                                intent.putExtras(bundle);
//                                getContext().startActivity(intent);
//                            }
//                        });
//                        play.setVisibility(View.GONE);
//                        article=item;
//                    }
//                    else if(moreContent.getType().toLowerCase().equals("video")){
//                       play.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                Utils.playVideoYoutube(getContext(), map.get("video"));
//                            }
//                        });
//                        video=item;
//
//                    }else if(moreContent.getType().toLowerCase().equals("gallery")){
//                       play.setImageResource(R.drawable.gallery);
//                        play.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                Gallery gal= new Gallery();
//                                List<Medium___> medium___s= new ArrayList<Medium___>();
//                                for (int i = 0; i < moreContent.getMedia().size(); i++) {
//                                    LinkedTreeMap<String,String > map= (LinkedTreeMap<String, String>) moreContent.getMedia().get(i);
//                                    Medium___ temp= new Medium___();
//                                    temp.setMed(map.get("med"));
//                                    medium___s.add(temp);
//                                }
//                                gal.setMedia(medium___s);
//                                Intent galery= new Intent(getContext(),GalleryActivity.class);
//                                Bundle data= new Bundle();
//                                data.putSerializable("gal",gal);
//                                galery.putExtras(data);
//                                getContext().startActivity(galery);
//                            }
//                        });
//                        gallery=item;
//                    }
//                   // container.addView(item);
//                }
//                if(article!=null){
//                    View header= ((LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.section_sust_partner,null);
//                    ((TextView)header.findViewById(R.id.sponsor_text)).setText("Case Study");
//                    container.addView(header);
//                    container.addView(article);}
//                if(video!=null){
//                    View header= ((LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.section_sust_partner,null);
//                    ((TextView)header.findViewById(R.id.sponsor_text)).setText("Video");
//                    container.addView(header);
//                    container.addView(video);
//                }
//                if(gallery!=null) {
//                    View header= ((LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.section_sust_partner,null);
//                    ((TextView)header.findViewById(R.id.sponsor_text)).setText("Photo Gallery");
//                    container.addView(header);
//                    container.addView(gallery);
//                }
//                allView.setAnimation(AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_in));
//                allView.setVisibility(View.VISIBLE);
//                pledge_button.setVisibility(View.VISIBLE);
//                hideProgressDialog();
//            }else{
//                hideProgressDialog();
//                showNoConnectionError();
//            }
//        }
//
//        public void showNoConnectionError(){
//            if(!getActivity().isFinishing()){
//            AlertDialog.Builder dialog= new AlertDialog.Builder(getActivity());
//            dialog.setMessage("You have no internet connection!!");
//            dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.dismiss();
//                }
//            });
//            dialog.show();}
//        }
//
//        private void setData(int count, int range,ArrayList<Integer> colors) {
//            //aqui pongo los pladges para poder trabajar desde el dialogo
//            Aplication.pledge=sustainability.getPledge();
//            ArrayList<Entry> yVals1 = new ArrayList<Entry>();
//            ArrayList<String> xVals = new ArrayList<String>();
//
//            float mult = range;
//            // IMPORTANT: In a PieChart, no values (Entry) should have the same
//            // xIndex (even if from different DataSets), since no values can be
//            // drawn above each other.
//            for (int i = 0; i < sustainability.getPledge().size(); i++) {
//                yVals1.add(new Entry(Float.parseFloat(sustainability.getPledge().get(i).getVotes()), i));
//                xVals.add(sustainability.getPledge().get(i).getPledge());
//            }
//
//            PieDataSet dataSet = new PieDataSet(yVals1, "");
//            dataSet.setSliceSpace(2f);
//            dataSet.setSelectionShift(5f);
//
//            // add a lot of colors
//            dataSet.setColors(colors);
//
//            PieData data = new PieData(xVals, dataSet);
//            data.setValueFormatter(new Formatter());
//            data.setValueTextSize(11f);
//            data.setValueTextColor(Color.WHITE);
//            data.setDrawValues(true);
//            mChart.setData(data);
//            // undo all highlights
//            mChart.highlightValues(null);
//
//            // mChart.invalidate();
//        }
//    }

    class PagerAdapter extends android.support.v4.view.PagerAdapter {

        @Bind(R.id.image)
        ImageView picture;
        @Bind(R.id.next)
        View next;
        @Bind(R.id.prev)
        View prev;
        @Bind(R.id.text)
        TextView text;

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            final Topstory topstory= sustainability.getTopstories().get(position);
            View layout= getActivity().getLayoutInflater().inflate(R.layout.fragment_sust_slide_item,container,false);
            ButterKnife.bind(this, layout);
            ImageLoader.getInstance().displayImage(topstory.getMedia().get(0).getMed(), picture);

            if(position< sustainability.getTopstories().size()-1){
                next.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(pager.getCurrentItem()+1<=sustainability.getTopstories().size()-1)
                            pager.setCurrentItem(pager.getCurrentItem()+1);
                    }
                });}else
                next.setVisibility(View.GONE);
            if(position >0 ){
                prev.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(pager.getCurrentItem()-1 >= 0)
                            pager.setCurrentItem(pager.getCurrentItem()-1);
                    }
                });
            }else
                prev.setVisibility(View.GONE);
            text.setText(topstory.getTitle());
            text.setTypeface(Aplication.robotoM);
            container.addView(layout);
            return layout;
        }


        @Override
        public int getCount() {
            return sustainability.getTopstories().size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            super.setPrimaryItem(container, position, object);
            Log.d("ssss", "setPrimaryItem: ");
        }
    }

    class Formatter extends PercentFormatter{

        public Formatter() {
            this.mFormat = new DecimalFormat("###,###,##");
        }

        public Formatter(DecimalFormat format) {
            this.mFormat = format;
        }

        public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
            return this.mFormat.format((double)value);
        }

        public String getFormattedValue(float value, YAxis yAxis) {
            return this.mFormat.format((double)value);
        }
    }
}
