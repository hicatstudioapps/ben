package com.muevaelvolante.barapp.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.muevaelvolante.Aplication;
import com.muevaelvolante.barapp.HomeActivity;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.adapters.results.RaceAdapter;
import com.muevaelvolante.barapp.adapters.results.RegataAdapter;
import com.muevaelvolante.barapp.adapters.results.TablePositionAdapter;
import com.muevaelvolante.barapp.adapters.results.YearAdapter;
import com.muevaelvolante.barapp.cache.JsonCacheWriter;
import com.muevaelvolante.barapp.interfaces.IResults;
import com.muevaelvolante.barapp.model.results.Regattaresults;
import com.muevaelvolante.barapp.model.results.Result;
import com.muevaelvolante.barapp.model.results.ResultRoot;
import com.muevaelvolante.barapp.model.results.ResultsHelp;
import com.muevaelvolante.barapp.services.ServiceGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.joda.time.chrono.StrictChronology;
import org.joda.time.format.DateTimeFormat;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;


public class ResultFragment extends Fragment {


    private ProgressDialog mProgressDialog;
    private List<Result> regatasForYear;
    private Regattaresults regataResultsActive;
    private RegataAdapter regataAdapter;
    private RaceAdapter raceAdapter;
    private TablePositionAdapter tableAdapter;
    private YearAdapter yearAdapter;
    private String url;


    public ResultFragment() {
        // Required empty public constructor
    }

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.yearRV)
    RecyclerView yearRecyclerView;
    @Bind(R.id.raceRV)
    RecyclerView raceRecyclerView;
    @Bind(R.id.regataRV)
    RecyclerView reagtaRecyclerView;
    @Bind(R.id.tableRV)
    RecyclerView tableRecyclerView;
    @Bind(R.id.overall)
    View overalView;
    @Bind(R.id.selected)
    View selectedView;
    List<String> years;
    @Bind(R.id.tableContainaer)
    LinearLayout tableContainer;
    @Bind(R.id.root)
            LinearLayout root;
    List<Result> results;
    Result overallResult;
    ResultsHelp resultsHelp;

    LoadResults loadResult;

    public static ResultFragment newInstance(String url) {
        ResultFragment fragment = new ResultFragment();
        Bundle args = new Bundle();
        args.putString("url", url);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            url = getArguments().getString("url");

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_result, container, false);
        ButterKnife.bind(this, layout);
        toolbar.setTitle(" ");
        setHasOptionsMenu(true);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.back));

        //aqui cargo y preparo todos los datos
        LinearLayoutManager linearLayoutManagerY = new LinearLayoutManager(getActivity());
        linearLayoutManagerY.setOrientation(0);
        LinearLayoutManager linearLayoutManagerR = new LinearLayoutManager(getActivity());
        linearLayoutManagerR.setOrientation(0);
        LinearLayoutManager linearLayoutManagerRc = new LinearLayoutManager(getActivity());
        linearLayoutManagerRc.setOrientation(0);
        LinearLayoutManager linearLayoutManagerT = new LinearLayoutManager(getActivity());
        yearRecyclerView.setLayoutManager(linearLayoutManagerY);
        reagtaRecyclerView.setLayoutManager(linearLayoutManagerR);
        raceRecyclerView.setLayoutManager(linearLayoutManagerRc);
        tableRecyclerView.setLayoutManager(linearLayoutManagerT);
        overalView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFullOverall();
            }
        });
        return layout;
    }

    private void showFullOverall() {
        if (tableAdapter == null) {
            tableAdapter = new TablePositionAdapter(((AppCompatActivity) getActivity()), regataResultsActive.getRaces()
                    .get(0).getCompetitors(), regatasForYear.get(0).getCompetitors(), regataResultsActive.getTotal(), this);
        }
                    selectedView.setVisibility(View.VISIBLE);
                tableAdapter.setTypeRace(false);
                tableAdapter.setWhiteLetter(true);
                tableContainer.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                tableAdapter.setTotal(overallResult.getRegattaresults().getTotal());
        if(tableRecyclerView.getAdapter()== null)
            tableRecyclerView.setAdapter(tableAdapter);
        else
                tableAdapter.notifyDataSetChanged();
                unselectYear();
                reagtaRecyclerView.setVisibility(View.GONE);
                raceRecyclerView.setVisibility(View.GONE);
    }

    private void unselectYear() {
        yearAdapter.unSelectYear();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        if (results == null)
//            new LoadResults().execute();
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage("Loading content...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    //obtengo los años
    public List<String> getYears() {
        List<String> years = new LinkedList<>();
        for (Result r : results) {
            int reagataYear = DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime(r.getDate()).getYear();
            if (!years.contains(reagataYear + ""))
                years.add(reagataYear + "");
        }
        return years;
    }

    @Override
    public void onResume() {
        super.onResume();
//        ((HomeActivity)getActivity()).disableMore();
        if (results == null || !new File(Aplication.getAppFilepath(getActivity())+"/resultRoot.json").exists()){
        loadResult = new LoadResults();
        loadResult.execute();}
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(loadResult!=null && loadResult.getStatus().equals(AsyncTask.Status.RUNNING))
            loadResult.cancel(true);
    }

    //pongo los datos en el adapter de years
    public void setYearsAdapterData(List<String> years) {
        if (yearAdapter == null) {
            yearAdapter = new YearAdapter((AppCompatActivity) getActivity(), years, this);
            yearRecyclerView.setAdapter(yearAdapter);
        }

    }

    //obtengo las regatas de un año
    public List<Result> getResultsForYear(int year) {
        if (selectedView.getVisibility() == View.VISIBLE) {
            reagtaRecyclerView.setVisibility(View.VISIBLE);
            raceRecyclerView.setVisibility(View.VISIBLE);
            tableAdapter.setTypeRace(true);
            selectedView.setVisibility(View.INVISIBLE);
        }
        List<Result> regattaresultses = new LinkedList<>();
        for (Result r : results) {
            int reagataYear = DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime(r.getDate()).getYear();
            if (reagataYear == year) {
                regattaresultses.add(r);
            }
        }
        return regattaresultses;
    }

    //pongo los datos del adapter de regata
    public void setRegataAdapterData(List<Result> data) {
        if (regataAdapter == null) {
            regataAdapter = new RegataAdapter((AppCompatActivity) getActivity(), data, this);
            reagtaRecyclerView.setAdapter(regataAdapter);
        } else {
            regataAdapter.setRegatas(data);
            regataAdapter.setFirstMarked(false);
            regataAdapter.notifyDataSetChanged();
        }
    }

    //pongo los datos en el adapter de Races
    public void setRacesAdapterData(Regattaresults regattaresults) {
        if (raceAdapter == null) {
            raceAdapter = new RaceAdapter((AppCompatActivity) getActivity(), regattaresults, this);
            raceRecyclerView.setAdapter(raceAdapter);
        } else {
            raceAdapter.setRegataResult(regattaresults);
            raceAdapter.setFirstMarked(false);
            raceAdapter.notifyDataSetChanged();
        }
    }
    //lleno la tabla de posiciones

    public void setTablePositionData(int race) {
        if (tableAdapter == null) {
            tableAdapter = new TablePositionAdapter(((AppCompatActivity) getActivity()), regataResultsActive.getRaces()
                    .get(race).getCompetitors(), regatasForYear.get(0).getCompetitors(), regataResultsActive.getTotal(), this);
            tableContainer.setBackgroundColor(getResources().getColor(android.R.color.white));
            tableRecyclerView.setAdapter(tableAdapter);
        } else {
            tableContainer.setBackgroundColor(getResources().getColor(android.R.color.white));
            tableAdapter.setWhiteLetter(false);
            tableAdapter.setTypeRace(true);
            tableAdapter.setCompetitors(regataResultsActive.getRaces()
                    .get(race).getCompetitors(), regatasForYear.get(0).getCompetitors());
            tableAdapter.setTotal(regataResultsActive.getTotal());
            tableAdapter.notifyDataSetChanged();
        }
    }

    //aqui mando a refrescar los adapters en dependencia de lo que seleccione el usuario
    //comenzamos cargando las regatas del año que selecciono
    public void loadDataForYear(int year) {
        regatasForYear = getResultsForYear(year);
        setRegataAdapterData(regatasForYear);
        regataResultsActive = regatasForYear.get(0).getRegattaresults();
        setRacesAdapterData(regataResultsActive);
        setTablePositionData(0);
    }

    //aqui mando a refrescar los adapters en dependencia de lo que seleccione el usuario
    //comenzamos cargando las races de la regata que selecciono
    public void loadDataForRegata(Regattaresults regattaresults) {
        regataResultsActive = regattaresults;
        setRacesAdapterData(regataResultsActive);
        setTablePositionData(0);
    }

    public void loadDataForRace(int race) {
        setTablePositionData(race);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            ((HomeActivity)getActivity()).showHome();
        }
        return true;
    }

    public void showRaceOverall() {
        tableAdapter.setTypeRace(false);
        tableAdapter.notifyDataSetChanged();
    }

    class LoadResults extends AsyncTask<Void, Void, List<Result>> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }

        @Override
        protected void onPostExecute(List<Result> results) {
            super.onPostExecute(results);
            if (results != null) {
                ResultFragment.this.results = results;
                //elimino overall de los resultados
                for (Result r : results)
                    if (r.getTitle().equals("Overall Series")) {
                        overallResult = r;
                        results.remove(r);
                    }
                //primero los años
                setYearsAdapterData(getYears());
                yearAdapter.notifyDataSetChanged();
                yearRecyclerView.scrollToPosition(getYears().size() - 1);
                //desues las regatas
                regatasForYear = getResultsForYear(Integer.parseInt(getYears().get(getYears().size() - 1)));
                setRegataAdapterData(regatasForYear);
                //despues las races
                regataResultsActive = regatasForYear.get(0).getRegattaresults();
                setRacesAdapterData(regataResultsActive);
//              aqui muestro la tabla para la posiscion x defecto
                //setTablePositionData(0);

                //aqui agrego la ayuda
                View view_titulo = getActivity().getLayoutInflater().inflate(R.layout.result_help_text, null);
                TextView titulo = (TextView) view_titulo.findViewById(R.id.textView10);
                titulo.setTypeface(Aplication.robotoL);
                titulo.setTextColor(Color.parseColor("#ffffff"));
                titulo.setText(Html.fromHtml(resultsHelp.getTitle()));
                root.addView(view_titulo);
                //aqui procesamos el resto de los elementos de la ayuda
                List<Object> text = resultsHelp.getText();
                for (int i = 0; i < text.size(); i++) {
                    Object data = text.get(i);
                    if (data instanceof String) {//si es texto
                        View view_text = getActivity().getLayoutInflater().inflate(R.layout.result_help_text, null);
                        TextView textView = (TextView) view_text.findViewById(R.id.textView10);
                        textView.setTypeface(Aplication.robotoL);
                        textView.setTextColor(Color.parseColor("#ffffff"));
                        textView.setText(Html.fromHtml(String.valueOf(data)));
                        root.addView(view_text);
                        continue;
                    }
                    if (data instanceof LinkedTreeMap) {
                        View view_text = getActivity().getLayoutInflater().inflate(R.layout.result_help_image, null);
                        ImageLoader.getInstance().displayImage(((LinkedTreeMap<String, String>) data).get("med"), (ImageView) view_text.findViewById(R.id.imageView20));
                        root.addView(view_text);
                        continue;
                    }
                }
                if (Aplication.result_order != null && !TextUtils.isEmpty(Aplication.result_order) && Aplication.result_order.toLowerCase().equals("lastrace")) {
                    //marcamos el ano
                    yearRecyclerView.setVisibility(View.INVISIBLE);
                    reagtaRecyclerView.setVisibility(View.INVISIBLE);
                    raceRecyclerView.setVisibility(View.INVISIBLE);
                    tableRecyclerView.setVisibility(View.INVISIBLE);
                    mProgressDialog.setMessage("Loading Last Race...");
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            View view = yearRecyclerView.getChildAt(getYears().size() - 1);
                            view.performClick();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    reagtaRecyclerView.scrollToPosition(regatasForYear.size() - 1);
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            View view = reagtaRecyclerView.getChildAt(reagtaRecyclerView.getChildCount() - 1);
                                            view.performClick();
                                            new Handler().postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    raceRecyclerView.scrollToPosition(raceAdapter.getItemCount() - 1);
                                                    new Handler().postDelayed(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            raceRecyclerView.getChildAt(raceRecyclerView.getChildCount() - 1).performClick();
                                                            yearRecyclerView.setVisibility(View.VISIBLE);
                                                            reagtaRecyclerView.setVisibility(View.VISIBLE);
                                                            raceRecyclerView.setVisibility(View.VISIBLE);
                                                            tableRecyclerView.setVisibility(View.VISIBLE);
                                                            mProgressDialog.dismiss();
                                                        }
                                                    }, 500);
                                                }
                                            }, 500);
                                        }
                                    }, 1000);
                                }
                            }, 1000);
                        }
                    }, 1000);
                } else {
                    showFullOverall();
                    hideProgressDialog();
                }
            } else {
                hideProgressDialog();
                showNoConnectionError();
            }
        }

        public void showNoConnectionError(){
            try{
                if(!getActivity().isFinishing()){
            AlertDialog.Builder dialog= new AlertDialog.Builder(getActivity());
            dialog.setMessage("Problem reading data from server.");
            dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            dialog.show();}}catch (Exception ex){
                return;
            }
        }

        @Override
        protected List<Result> doInBackground(Void... params) {
            IResults resultsApi = ServiceGenerator.createService(IResults.class);
            Call<ResultRoot> methodCall = resultsApi.getResultsRoot(url);
//            Call<ResultRoot> methodCall= resultsApi.getResultsRoot("http://192.168.101.1:8081/ben/en/app/result.txt");
            try {
                ResultRoot root= methodCall.execute().body();
                String resultRoot= new Gson().toJson(root, ResultRoot.class);
                JsonCacheWriter.writeCache(Aplication.getAppFilepath(getActivity())+"/resultRoot.json",resultRoot);
                Aplication.result_order=root.getResultsRaceOrders().get(0).getRace_default_order();
                ArrayList<Result> result = resultsApi.getResults(root.getResultsLink().get(0).getURL()).execute().body();
                String dataList= new Gson().toJson(result, ArrayList.class);
                JsonCacheWriter.writeCache(Aplication.getAppFilepath(getActivity())+"/result.json",dataList);
                if(result!=null)
                Collections.sort(result);
                //primero el titulo
                ResultFragment.this.resultsHelp=root.getResultsHelp().get(0);
                if(isCancelled())
                    return null;
                return result;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
