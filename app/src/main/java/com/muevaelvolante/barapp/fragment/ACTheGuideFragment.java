package com.muevaelvolante.barapp.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.amazonaws.com.google.gson.Gson;
import com.muevaelvolante.barapp.HomeActivity;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.adapters.AC.ACAdapter;
import com.muevaelvolante.barapp.commons.Constants;
import com.muevaelvolante.barapp.interfaces.IACTheGuide;
import com.muevaelvolante.barapp.model.AC.AC;
import com.muevaelvolante.barapp.services.ServiceGenerator;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ACTheGuideFragment extends RootFragment {

    private ProgressDialog mProgressDialog;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.acRV)
    RecyclerView AC_RecyclerView;
    private String url;

    public ACTheGuideFragment() {
        // Required empty public constructor
    }

    public static ACTheGuideFragment newInstance(String url, String name) {
        ACTheGuideFragment fragment = new ACTheGuideFragment();
        Bundle args = new Bundle();
        args.putString("url", url);
        args.putString("name",name);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void proccessData(String data) {
        AC ac = new Gson().fromJson(data, AC.class);
        AC_RecyclerView.setAdapter(new ACAdapter(ac.getAcTheGuide(), (AppCompatActivity) getActivity()));
        hideDialog();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            uri = getArguments().getString("url");
            json_name= getArguments().getString("url");
            dialog_text="Loading content...";
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View layout= inflater.inflate(R.layout.fragment_acthe_guide, container, false);
        ButterKnife.bind(this, layout);
        toolbar.setTitle(" ");
        setHasOptionsMenu(true);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.back));
        AC_RecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        return layout;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        showProgressDialog();
//        IACTheGuide ACService= ServiceGenerator.createService(IACTheGuide.class);
//        Call<AC> call= ACService.getAc(url);
//        call.enqueue(new Callback<AC>() {
//            @Override
//            public void onResponse(Call<AC> call, Response<AC> response) {
//                if (response.isSuccessful()) {
//                    AC ac = response.body();
//                    AC_RecyclerView.setAdapter(new ACAdapter(ac.getAcTheGuide(), (AppCompatActivity) getActivity()));
//                    hideProgressDialog();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<AC> call, Throwable t) {
//                    hideProgressDialog();
//                showNoConnectionError();
//            }
//        });
    }



    public void showNoConnectionError(){
        AlertDialog.Builder dialog= new AlertDialog.Builder(getActivity());
        dialog.setMessage("You have no internet connection!!");
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            ((HomeActivity)getContext()).showHome();
        }
        return true;
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage("Loading content...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }


}
