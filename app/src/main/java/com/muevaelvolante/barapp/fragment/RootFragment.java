package com.muevaelvolante.barapp.fragment;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.muevaelvolante.Aplication;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.cache.JsonCacheReader;
import com.muevaelvolante.barapp.cache.JsonCacheWriter;
import com.muevaelvolante.barapp.interfaces.ISettings;
import com.muevaelvolante.barapp.services.ServiceGenerator;

import org.json.JSONObject;

import java.io.File;

import okhttp3.ResponseBody;
import retrofit2.Call;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public abstract class RootFragment extends Fragment {

    private MaterialDialog mProgressDialog;
    public String uri;
    public String json_name;
    public String title_name;
    public String dialog_text;
    public int load_cache=0;
    LayoutInflater inflater;

    public RootFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void showProgressDialog(String text) {
        mProgressDialog.show();
    }

    public void hideDialog(){
        mProgressDialog.dismiss();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mProgressDialog =new MaterialDialog.Builder(getActivity())
                .progress(true, 0)
                .widgetColor(Color.parseColor("#454553"))
                .content(dialog_text)
                .cancelable(false)
                .build();
        getData();
        //new LoadData().execute();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    public void getData() {
        mProgressDialog.show();
        new Handler().postDelayed(() -> {
            Observable.create(new Observable.OnSubscribe<String>() {
                @Override
                public void call(Subscriber<? super String> subscriber) {
                    try {
                        File cache = new File(Aplication.getAppFilepath(getActivity()) + "/" + json_name + ".json");
                        if (cache.exists()) {
                            subscriber.onNext(loadCache());
                            subscriber.onCompleted();
                            return;
                        } else {
                            Call<ResponseBody> result = ServiceGenerator.createService(ISettings.class).getUrl(uri);
                            String data = result.execute().body().string();
                            JSONObject o = new JSONObject(data);
                            JsonCacheWriter.writeCache(Aplication.getAppFilepath(getActivity()) + "/" + json_name + ".json", (data));
                            subscriber.onNext(data);
                            subscriber.onCompleted();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        String jsonOfflineData = JsonCacheReader.getCacheJson(Aplication.getAppFilepath(getActivity()) + "/" + json_name + ".json");
                        if (jsonOfflineData != null) {
                            subscriber.onNext(jsonOfflineData);
                            subscriber.onCompleted();
                        }
                        subscriber.onError(e);
                        subscriber.onCompleted();
                    }
                }
            }).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<String>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            Log.d("ROOR FRAGMENT ERROR", json_name);
                            showErrorLoadDialog();
                            hideDialog();
                        }

                        @Override
                        public void onNext(String gson) {
                            proccessData(gson);
                        }
                    });
        }, 200);
    }

    private void showErrorLoadDialog() {
        new MaterialDialog.Builder(getActivity())
                .title(R.string.error)
                .content(R.string.load_content_error)
                .positiveText(R.string.tryy)
                .negativeText(R.string.cancel)
                .positiveColor(getResources().getColor(R.color.colorPrimary))
                .negativeColor(getResources().getColor(R.color.colorPrimary))
                .onPositive(((dialog, which) -> {
                    getData();
                }))
                .onNegative((dialog, which) -> dialog.dismiss()).build().show();
    }

    private String loadCache() {
        return JsonCacheReader.getCacheJson(Aplication.getAppFilepath(getActivity()) + "/"+json_name.toLowerCase()+".json");
    }


    public abstract void proccessData(String data);

//
}
