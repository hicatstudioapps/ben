package com.muevaelvolante.barapp.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.com.google.gson.Gson;
import com.muevaelvolante.Aplication;
import com.muevaelvolante.barapp.HomeActivity;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.activity.SailActivity;
import com.muevaelvolante.barapp.interfaces.ITeam;
import com.muevaelvolante.barapp.model.team.Sailing;
import com.muevaelvolante.barapp.model.team.Team;
import com.muevaelvolante.barapp.services.ServiceGenerator;
import com.muevaelvolante.barapp.util.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.rey.material.widget.ProgressView;

import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;

public class TeamFragment extends RootFragment {

    @Bind(R.id.loading)
    ProgressView progressView;
    @Bind(R.id.pager)
    ViewPager viewPager;
    PagerAdapter adapter;
    @Bind(R.id.toolbar)
          Toolbar toolbar;
    int actionBarHeight;

    Team team;

    public static TeamFragment newInstance(String url,String title) {
        TeamFragment fragment = new TeamFragment();
        Bundle args = new Bundle();
        args.putString("url", url);
        args.putString("title",title);
        args.putString("text","Loading teams...");
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            uri = getArguments().getString("url");
            json_name= getArguments().getString("title");
            dialog_text=getArguments().getString("text");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout= inflater.inflate(R.layout.activity_team,null);
        ButterKnife.bind(this, layout);
        setHasOptionsMenu(true);
        toolbar.setTitle(" ");
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        actionBarHeight=((AppCompatActivity) getActivity()).getSupportActionBar().getHeight();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.back));
        return layout;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        actionBarHeight=((AppCompatActivity) getActivity()).getSupportActionBar().getHeight();

    }

    @Override
    public void onResume() {
        super.onResume();
        //((HomeActivity)getActivity()).disableMore();
//        if(adapter==null || adapter.getCount()==0){
//            new LoadSailing().execute();
//            progressView.start();}
    }

    @Override
    public void proccessData(String data) {
        team= new Gson().fromJson(data,Team.class);
        adapter= new PagerAdapter();
        if(team!= null) {
            viewPager.setOffscreenPageLimit(20);
            viewPager.setAdapter(adapter);
            viewPager.setClipToPadding(false);
            viewPager.setPadding(getResources().getInteger(R.integer.padding_vp_team),0,getResources().getInteger(R.integer.padding_vp_team),0);
//                viewPager.setPageMargin(-130);
            hideDialog();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            ((HomeActivity)getActivity()).showHome();
        }
        return true;

    }

    class PagerAdapter extends android.support.v4.view.PagerAdapter {

        @Bind(R.id.picture)
        ImageView picture;
        @Bind(R.id.position)
        TextView position;
        @Bind(R.id.name)
        TextView name;
        @Bind(R.id.root)
        RelativeLayout root;
        private String url;

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            final Sailing sail= team.getSailing().get(position);
            View layout= getActivity().getLayoutInflater().inflate(R.layout.sailing_item,container,false);
            ButterKnife.bind(this, layout);
            name.setText(sail.getName());
            this.position.setText(sail.getPosition());
            int [] res= Utils.getScreenDimensions(getContext());
            //calculamos las dimensiones de los componentes de la pantalla
            float marginText= getResources().getDimension(R.dimen.text_margin);
            float bottomTextSize= getResources().getDimension(R.dimen.text__size);
            float topTextSize= getResources().getDimension(R.dimen.text___size);
            float menuHeight= getResources().getDimension(R.dimen.bar_height);
            float actionBarSize= getResources().getDimension(R.dimen.toolbar_height);
            final float bottomFullSize= marginText+bottomTextSize+marginText+topTextSize+marginText*2+menuHeight+
                    actionBarSize;
            final float finalHeight= res[0]- bottomFullSize;
           layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent= new Intent(getContext(),SailActivity.class);
                    Bundle bundle= new Bundle();
                    bundle.putSerializable("sail",sail);
                    Aplication.sailing=sail;
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            });
            container.addView(layout);
            ImageLoader.getInstance().displayImage(sail.getMedia().get(0).getMed(), picture, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String s, View view) {

                }

                @Override
                public void onLoadingFailed(String s, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                    ImageView image= (ImageView)view;
                    float proportion= (float)bitmap.getHeight()/bitmap.getWidth();
                    int width= (int) (finalHeight/proportion);
                    RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams) image.getLayoutParams();
                    lp.width=width;
                    lp.height= (int) finalHeight;
                    image.setLayoutParams(lp);
//                    Toast.makeText(TeamFragment.this.getActivity(), "sdfj;;lkfsd", Toast.LENGTH_SHORT).show();
                    //  picture.invalidate();
                }

                @Override
                public void onLoadingCancelled(String s, View view) {

                }
            });
            return layout;
        }

        @Override
        public int getCount() {
            return team.getSailing().size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }
    }

//    class LoadSailing extends AsyncTask<Void,Void,Void>{
//        @Override
//        protected Void doInBackground(Void... params) {
//            ITeam team= ServiceGenerator.createService(ITeam.class);
//            final Team[] result = new Team[1];
//            Call<Team> call= team.getTeam(url);
//            try {
//                TeamFragment.this.team= call.execute().body();
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            return null;
// }
//
//        @Override
//        protected void onPostExecute(Void aVoid) {
//            super.onPostExecute(aVoid);
//            progressView.stop();
//            adapter= new PagerAdapter();
//            if(team!= null) {
//                viewPager.setOffscreenPageLimit(20);
//                viewPager.setAdapter(adapter);
//                viewPager.setClipToPadding(false);
//                viewPager.setPadding(getResources().getInteger(R.integer.padding_vp_team),0,getResources().getInteger(R.integer.padding_vp_team),0);
////                viewPager.setPageMargin(-130);
//            }else{
//               showNoConnectionError();
//            }
//        }
//
//        public void showNoConnectionError(){
//            if(!getActivity().isFinishing()){
//            AlertDialog.Builder dialog= new AlertDialog.Builder(getActivity());
//            dialog.setMessage("You have no internet connection!!");
//            dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.dismiss();
//                }
//            });
//            dialog.show();}
//        }
//    }
}
