package com.muevaelvolante.barapp.fragment;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.ServiceCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.amazonaws.com.google.gson.Gson;
import com.muevaelvolante.barapp.HomeActivity;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.activity.CameraActivity;
import com.muevaelvolante.barapp.adapters.community.CommunityAdapter;
import com.muevaelvolante.barapp.interfaces.ISettings;
import com.muevaelvolante.barapp.model.social.Social;
import com.muevaelvolante.barapp.services.ServiceGenerator;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ComunityFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ComunityFragment extends RootFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.Rv)
    RecyclerView recycler;
    private ProgressDialog mProgressDialog;

    public ComunityFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static ComunityFragment newInstance(String param1, String param2) {
        ComunityFragment fragment = new ComunityFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            uri  = getArguments().getString(ARG_PARAM1);
            json_name = getArguments().getString(ARG_PARAM2);
            dialog_text="Loading content...";
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);
        View layout= inflater.inflate(R.layout.fragment_comunity, container, false);
        ButterKnife.bind(this,layout);
        toolbar.setTitle(" ");
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.back));
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), CameraActivity.class));
            }
        });
        recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        return layout;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        ISettings settings= ServiceGenerator.createService(ISettings.class);
//        final Call<Social> social= settings.getSocial(mParam1);
//        showProgressDialog();
//        social.enqueue(new Callback<Social>() {
//            @Override
//            public void onResponse(Call<Social> call, Response<Social> response) {
//                if(response.isSuccessful()){
//                Social social1= response.body();
//                    recycler.setAdapter(new CommunityAdapter(getActivity(),social1.getSocial(),social1.getSocialBanner().get(0),
//                            social1.getFollowUs(),social1.getPhotoShare()));
//                }
//                else
//                    Toast.makeText(getActivity(),"A network error ocurred",Toast.LENGTH_SHORT).show();hideProgressDialog();
//            }
//
//            @Override
//            public void onFailure(Call<Social> call, Throwable t) {
//                showNoConnectionError();
//                hideProgressDialog();
//            }
//        });
    }

    @Override
    public void proccessData(String data) {
        Social social1= new Gson().fromJson(data,Social.class);
        recycler.setAdapter(new CommunityAdapter(getActivity(),social1.getSocial(),social1.getSocialBanner().get(0),
                social1.getFollowUs(),social1.getPhotoShare()));
        hideDialog();
    }

    public void showNoConnectionError(){
        AlertDialog.Builder dialog= new AlertDialog.Builder(getActivity());
        dialog.setMessage("You have no internet connection!!");
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            ((HomeActivity)getContext()).showHome();
        }
        return true;
    }
    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage("Loading content...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }
}
