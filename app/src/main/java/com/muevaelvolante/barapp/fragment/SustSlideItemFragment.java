package com.muevaelvolante.barapp.fragment;


import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.model.sustainability.Topstory;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.rey.material.widget.ProgressView;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SustSlideItemFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SustSlideItemFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    @Bind(R.id.image)
    ImageView image;
    @Bind(R.id.loading)
    ProgressView loading;
    Topstory topstory;
    public SustSlideItemFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static SustSlideItemFragment newInstance(Topstory topstory1) {
        SustSlideItemFragment fragment = new SustSlideItemFragment();
        Bundle args = new Bundle();
        args.putSerializable("story",topstory1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            topstory= (Topstory) getArguments().getSerializable("story");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout= inflater.inflate(R.layout.fragment_sust_slide_item, container, false);
        ButterKnife.bind(this,layout);
        loading.start();
        //por ahora muestro solo la primera imagen
        ImageLoader.getInstance().displayImage(topstory.getMedia().get(0).getMed(), image, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String s, View view) {
                loading.start();
            }

            @Override
            public void onLoadingFailed(String s, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
               // loading.stop();
            }

            @Override
            public void onLoadingCancelled(String s, View view) {

            }
        });
        return layout;
    }

}
