package com.muevaelvolante.barapp.fragment;


import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.activity.GalleryActivity;
import com.muevaelvolante.barapp.util.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link GalleryItem#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GalleryItem extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String url;
    private String mParam2;
    int total;
    int position;

    @Bind(R.id.image)
    ImageView image;
    @Bind(R.id.next)
    View next;
    @Bind(R.id.prev)
    View prev;
    private ToolBarControl toolBarControl;

    public GalleryItem() {
        // Required empty public constructor
    }

    public static GalleryItem newInstance(String url, int total, int position) {
        GalleryItem fragment = new GalleryItem();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, url);
        args.putInt("total",total);
        args.putInt("position",position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            url = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            total= getArguments().getInt("total");
            position= getArguments().getInt("position");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        toolBarControl= (ToolBarControl)(GalleryActivity)context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_gallery_item, container, false);
        ButterKnife.bind(this,view);
//        url="http://192.168.101.1:8081/ben/gallery.jpg";
        ImageLoader.getInstance().displayImage(url, image, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String s, View view) {

            }

            @Override
            public void onLoadingFailed(String s, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                if (getActivity() != null && isAdded()) {
                    if (!(Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation)) {
                        float proportion = (float) bitmap.getWidth() / bitmap.getHeight();
                        int alto = (int) (Utils.getScreenWidth(GalleryItem.this.getActivity()) / proportion);
                        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) image.getLayoutParams();
                        lp.height = alto;
                        lp.width = Utils.getScreenWidth(GalleryItem.this.getActivity());
                        image.setLayoutParams(lp);
                    } else {
                        float proportion = (float) bitmap.getHeight() / bitmap.getWidth();
                        int alto = (int) (Utils.getScreenWidth(GalleryItem.this.getActivity()) / proportion);
                        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) image.getLayoutParams();
                        lp.width = alto;
                        lp.height = Utils.getScreenDimensions(GalleryItem.this.getActivity())[0];
                        image.setLayoutParams(lp);
                    }
                }
            }

            @Override
            public void onLoadingCancelled(String s, View view) {

            }
        });
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((Configuration.ORIENTATION_LANDSCAPE==getResources().getConfiguration().orientation))
                toolBarControl.toogleToolBar();
            }
        });
        if(position< total-1){
            next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(((GalleryActivity)getActivity()).getPager().getCurrentItem()+1<=total-1)
                        ((GalleryActivity)getActivity()).getPager().setCurrentItem(((GalleryActivity)getActivity()).getPager().getCurrentItem()+1);
                }
            });
        }else
            next.setVisibility(View.GONE);
        if(position >0 ){
            prev.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(((GalleryActivity)getActivity()).getPager().getCurrentItem()-1 >= 0)
                        ((GalleryActivity)getActivity()).getPager().setCurrentItem(((GalleryActivity)getActivity()).getPager().getCurrentItem()-1);
                }
            });
        }else
            prev.setVisibility(View.GONE);
        return view;
    }

   public interface ToolBarControl{
        void toogleToolBar();
    }
}
