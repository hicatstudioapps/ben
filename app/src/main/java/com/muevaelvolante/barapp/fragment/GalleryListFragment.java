package com.muevaelvolante.barapp.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.amazonaws.com.google.gson.Gson;
import com.muevaelvolante.barapp.HomeActivity;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.activity.CameraActivity;
import com.muevaelvolante.barapp.adapters.community.CommunityAdapter;
import com.muevaelvolante.barapp.adapters.photos.PhotosRecyclerView;
import com.muevaelvolante.barapp.interfaces.IPhotos;
import com.muevaelvolante.barapp.interfaces.ISettings;
import com.muevaelvolante.barapp.model.photos.Photos;
import com.muevaelvolante.barapp.model.social.Social;
import com.muevaelvolante.barapp.services.ServiceGenerator;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class GalleryListFragment extends RootFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.Rv)
    RecyclerView recycler;
    private ProgressDialog mProgressDialog;

    public GalleryListFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static GalleryListFragment newInstance(String param1, String param2) {
        GalleryListFragment fragment = new GalleryListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            uri  = getArguments().getString(ARG_PARAM1);
            json_name= getArguments().getString(ARG_PARAM2);
            dialog_text="Loading content...";
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);
        View layout= inflater.inflate(R.layout.fragment_gallery_list, container, false);
        ButterKnife.bind(this,layout);
        toolbar.setTitle(" ");
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.back));

        recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        return layout;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        IPhotos photos= ServiceGenerator.createService(IPhotos.class);
//        final Call<Photos> social= photos.getPhotos(mParam1);
//        showProgressDialog();
//        social.enqueue(new Callback<Photos>() {
//            @Override
//            public void onResponse(Call<Photos> call, Response<Photos> response) {
//                if(response.isSuccessful()){
//                    Photos social1= response.body();
//                    recycler.setAdapter(new PhotosRecyclerView(getActivity(),social1.getGalleries()));
//                }
//                else
//                    Toast.makeText(getActivity(),"A network error ocurred",Toast.LENGTH_SHORT).show();hideProgressDialog();
//            }
//
//            @Override
//            public void onFailure(Call<Photos> call, Throwable t) {
//                showNoConnectionError();
//                hideProgressDialog();
//            }
//        });
    }

    @Override
    public void proccessData(String data) {
        Photos social1= new Gson().fromJson(data,Photos.class);
        recycler.setAdapter(new PhotosRecyclerView(getActivity(),social1.getGalleries()));
        hideDialog();
    }

    public void showNoConnectionError(){
        AlertDialog.Builder dialog= new AlertDialog.Builder(getActivity());
        dialog.setMessage("You have no internet connection!!");
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

     /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            ((HomeActivity)getContext()).showHome();
        }
        return true;
    }
    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage("Loading content...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }
}
