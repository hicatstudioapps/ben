package com.muevaelvolante.barapp.adapters.results;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.muevaelvolante.Aplication;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.fragment.ResultFragment;
import com.muevaelvolante.barapp.model.results.Competitor;
import com.muevaelvolante.barapp.model.results.Competitor_;
import com.muevaelvolante.barapp.model.results.Regattaresults;
import com.muevaelvolante.barapp.model.results.Total;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by CQ on 14/04/2016.
 */
public class TablePositionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    AppCompatActivity context;
    ResultFragment fragment;

    boolean whiteLetter= false;

    public boolean isWhiteLetter() {
        return whiteLetter;
    }

    public void setWhiteLetter(boolean whiteLetter) {
        this.whiteLetter = whiteLetter;
    }

    public void setTypeRace(boolean typeRace) {
        this.typeRace = typeRace;
    }

    boolean typeRace=true;

    public void setTotal(List<Total> total) {
        this.total = total;
    }

    List<Total> total;
    public void setCompetitors(List<Competitor_> competitors,  List<Competitor> competitorList) {
        this.competitors = competitors;
        this.competitorList=competitorList;
    }

    List<Competitor_> competitors;
    List<Competitor> competitorList;
    View previousSelected;

    public TablePositionAdapter(AppCompatActivity context,  List<Competitor_> raceCompetitor,List<Competitor> competitorList,List<Total> total,ResultFragment fragment) {
        this.context = context;
        this.competitors = raceCompetitor;
        this.competitorList=competitorList;
        this.fragment=fragment;
        this.total=total;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(context.getLayoutInflater().inflate(R.layout.race_result, null));
    }

    Competitor findById(String id){
        for(Competitor c: competitorList){
            if(c.getCompetitorid().equals(id))
                return c;
        }
        return null;
    }
    public SpannableStringBuilder setBold(String text){
        final SpannableStringBuilder sb = new SpannableStringBuilder(text);

//// Span to set text color to some RGB value
//        final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.WHITE);

// Span to make text bold
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);

//// Set the text color for first 4 characters
//        sb.setSpan(fcs, 0, getString(R.string.app_name).length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

// make them also bold
        sb.setSpan(bss, 0,text.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        return sb;
    }
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (typeRace) {
            Competitor competitorData = findById(competitors.get(position).getCompetitorid());
            Log.d("image", competitorData.getMedia());
            Competitor_ competitorPosition = competitors.get(position);
            if (position == 0) {
                if (whiteLetter)
                    ((ViewHolder) holder).position.setText(setBold(competitorPosition.getRank() + ""));
                else
                    ((ViewHolder) holder).position.setText(competitorPosition.getRank() + "");
                ((ViewHolder) holder).position.setTypeface(Aplication.robotoM);
                ((ViewHolder) holder).team.setText(setBold(competitorData.getName()));
                ((ViewHolder) holder).team.setTypeface(Aplication.robotoM);
                ImageLoader.getInstance().displayImage(competitorData.getMedia(), ((ViewHolder) holder).flag);
                ((ViewHolder) holder).score.setText(setBold(competitorPosition.getPoints()));
                ((ViewHolder) holder).score.setTypeface(Aplication.robotoM);
            } else {
                ((ViewHolder) holder).position.setText(competitorPosition.getRank() + "");
                ((ViewHolder) holder).position.setTypeface(Aplication.robotoR);
                ((ViewHolder) holder).team.setText(competitorData.getName());
                ((ViewHolder) holder).team.setTypeface(Aplication.robotoR);
                ImageLoader.getInstance().displayImage(competitorData.getMedia(), ((ViewHolder) holder).flag);
                ((ViewHolder) holder).score.setText(competitorPosition.getPoints());
                ((ViewHolder) holder).score.setTypeface(Aplication.robotoR);
                ((ViewHolder) holder).divider.setVisibility(View.VISIBLE);
            }
            if (position==competitors.size()-1)
                ((ViewHolder) holder).divider.setVisibility(View.GONE);
        } else {
            Competitor competitorData = findById(total.get(position).getCompetitorid());
            Log.d("image", competitorData.getMedia());
            Total competitorPosition = total.get(position);
            if (position == 0) {
                ((ViewHolder) holder).position.setText(setBold(competitorPosition.getRank() + ""));
                ((ViewHolder) holder).position.setTypeface(Aplication.robotoM);
                ((ViewHolder) holder).team.setText(setBold(competitorData.getName()));
                ((ViewHolder) holder).team.setTypeface(Aplication.robotoM);
                ImageLoader.getInstance().displayImage(competitorData.getMedia(), ((ViewHolder) holder).flag);
                ((ViewHolder) holder).score.setText(setBold(competitorPosition.getPoints()));
                ((ViewHolder) holder).score.setTypeface(Aplication.robotoM);
            } else {
                ((ViewHolder) holder).position.setText(competitorPosition.getRank() + "");
                ((ViewHolder) holder).position.setTypeface(Aplication.robotoR);
                ((ViewHolder) holder).team.setText(competitorData.getName());
                ((ViewHolder) holder).team.setTypeface(Aplication.robotoR);
                ImageLoader.getInstance().displayImage(competitorData.getMedia(), ((ViewHolder) holder).flag);
                ((ViewHolder) holder).score.setText(competitorPosition.getPoints());
                ((ViewHolder) holder).score.setTypeface(Aplication.robotoR);
                ((ViewHolder) holder).divider.setVisibility(View.VISIBLE);
            }
            if (position==total.size()-1)
                ((ViewHolder) holder).divider.setVisibility(View.GONE);
        }
        if (whiteLetter) {
            ((ViewHolder) holder).position.setTextColor(Color.parseColor("#ffffff"));
            ((ViewHolder) holder).team.setTextColor(Color.parseColor("#ffffff"));
            ((ViewHolder) holder).score.setTextColor(Color.parseColor("#ffffff"));
            ((ViewHolder) holder).divider.setBackgroundColor(Color.parseColor("#ffffff"));
        } else {
            ((ViewHolder) holder).position.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            ((ViewHolder) holder).team.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            ((ViewHolder) holder).score.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            ((ViewHolder) holder).divider.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        }

    }

    @Override
    public int getItemCount() {
        if(typeRace)
        return competitors.size();
        else
            return total.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.position)
        TextView position;
        @Bind(R.id.team)
        TextView team;
        @Bind(R.id.score)
        TextView score;
        @Bind(R.id.flag)
        ImageView flag;
        @Bind(R.id.divider)
        View divider;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}