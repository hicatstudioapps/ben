package com.muevaelvolante.barapp.adapters.results;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.muevaelvolante.Aplication;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.fragment.ResultFragment;
import com.muevaelvolante.barapp.model.results.Regattaresults;
import com.muevaelvolante.barapp.model.results.Result;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by CQ on 14/04/2016.
 */
public class RaceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    AppCompatActivity context;

    public void setRegataResult(Regattaresults regataResult) {
        this.regataResult = regataResult;
    }

    ViewHolder lastHolder;
    int lastPosition;
    ResultFragment fragment;
    public void setFirstMarked(boolean firstMarked) {
        this.firstMarked = firstMarked;
    }

    boolean firstMarked=false;
    Regattaresults regataResult;
    View previousSelected;

    public RaceAdapter(AppCompatActivity context, Regattaresults regatas,ResultFragment fragment) {
        this.context = context;
        this.regataResult = regatas;
        this.fragment=fragment;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(context.getLayoutInflater().inflate(R.layout.race_item,null));
    }

    public SpannableStringBuilder setBold(String text){
        final SpannableStringBuilder sb = new SpannableStringBuilder(text);

//// Span to set text color to some RGB value
//        final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.WHITE);

// Span to make text bold
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);

//// Set the text color for first 4 characters
//        sb.setSpan(fcs, 0, getString(R.string.app_name).length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

// make them also bold
        sb.setSpan(bss, 0,text.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        return sb;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder hold=(ViewHolder)holder;
        if(position==0){
            ((ViewHolder)holder).year.setText("Overall");
            hold.year.setTypeface(Aplication.robotoL);
            hold.selected.setVisibility(View.INVISIBLE);
        }else if(position==1 && ! firstMarked){
            hold.selected.setVisibility(View.VISIBLE);
            hold.year.setText(setBold(regataResult.getRaces().get(position - 1).getName()));
            hold.year.setTypeface(Aplication.robotoM);
            lastHolder=hold;
            lastPosition=position;
            firstMarked=true;
        }
        else{
            hold.selected.setVisibility(View.INVISIBLE);
            hold.year.setText(setBold(regataResult.getRaces().get(position - 1).getName()));
            hold.year.setTypeface(Aplication.robotoL);
        }
    }

    @Override
    public int getItemCount() {
        return regataResult.getRaces().size()+1;
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        @Bind(R.id.year)
        TextView year;
        @Bind(R.id.selected)
        View selected;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getAdapterPosition() != lastPosition) {
                        if(lastPosition-1==-1){
                            lastHolder.selected.setVisibility(View.INVISIBLE);
                            lastHolder.year.setText(setBold("Overall"));
                            lastHolder.year.setTypeface(Aplication.robotoL);
                        }
                        else {
                            lastHolder.selected.setVisibility(View.INVISIBLE);
                            lastHolder.year.setText(setBold(regataResult.getRaces().get(lastPosition - 1).getName()));
                            lastHolder.year.setTypeface(Aplication.robotoL);
                        }
                        lastPosition = getAdapterPosition();
                        if(lastPosition-1==-1){
                            selected.setVisibility(View.VISIBLE);
                            year.setText(setBold("Overall"));
                            year.setTypeface(Aplication.robotoM);
                            lastHolder = ViewHolder.this;
                        }else{
                        selected.setVisibility(View.VISIBLE);
                        year.setText(setBold(regataResult.getRaces().get(getAdapterPosition() - 1).getName()));
                        year.setTypeface(Aplication.robotoM);}
                        lastHolder = ViewHolder.this;
                        if(lastPosition-1==-1){
                        fragment.showRaceOverall();
                        }
                        else
                            fragment.loadDataForRace(lastPosition-1);
                    }
                }
            });
        }
    }
}
