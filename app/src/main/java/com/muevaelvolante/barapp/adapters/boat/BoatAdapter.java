package com.muevaelvolante.barapp.adapters.boat;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.muevaelvolante.Aplication;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.activity.BoatDetailActivity;
import com.muevaelvolante.barapp.model.boat.Boat_;
import com.muevaelvolante.barapp.util.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.LinkedList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by CQ on 23/05/2016.
 */
public class BoatAdapter extends RecyclerView.Adapter<BoatAdapter.ViewHolder> {

    private List<Boat_> boats= new LinkedList<>();
    private Context context;

    public BoatAdapter(List<Boat_> boats, Context context) {
        this.boats = boats;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder= new ViewHolder(((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.boat_item, null));
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Boat_ item = boats.get(position);
        holder.textLeft.setText(item.getTitle());
        holder.textLeft.setTypeface(Aplication.robotoL);
        holder.textRight.setText(item.getTitle());
        holder.textRight.setTypeface(Aplication.robotoL);
        RelativeLayout.LayoutParams lpr= (RelativeLayout.LayoutParams) holder.container_right.getLayoutParams();
        int width= Utils.getScreenWidth((Activity) context);
        lpr.width=width/2;
        holder.container_right.setLayoutParams(lpr);
        RelativeLayout.LayoutParams lpl= (RelativeLayout.LayoutParams) holder.container_left.getLayoutParams();
        int widthL= Utils.getScreenWidth((Activity) context);
        lpl.width=widthL/2;
        holder.container_left.setLayoutParams(lpl);
        if(position % 2==0){
            holder.textLeft.setVisibility(View.GONE);
            holder.textRight.setVisibility(View.VISIBLE);
        }
        else{
            holder.textLeft.setVisibility(View.VISIBLE);
            holder.textRight.setVisibility(View.GONE);
        }
        ImageLoader.getInstance().displayImage(item.getMedia().get(0).getMed(),holder.image);
     }

    @Override
    public int getItemCount() {
        return boats.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @Bind(R.id.textView6)
        TextView textRight;
        @Bind(R.id.imageView4)
        ImageView image;
        @Bind(R.id.textView5)
        TextView textLeft;
        @Bind(R.id.container_left)
        View container_left;
        @Bind(R.id.container_right)
        View container_right;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            int screenWidth = Utils.getScreenWidth((AppCompatActivity)context);
            int finalHeight = (int) (screenWidth / 1.5);
            image.setMinimumHeight(finalHeight / 1);
            image.setMaxHeight(finalHeight / 1);
            image.setAdjustViewBounds(false);
            //set height as layoutParameter too
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) image.getLayoutParams();
            lp.height = finalHeight / 1;
            lp.width= screenWidth;
            image.setLayoutParams(lp);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

                Intent intent= new Intent(context, BoatDetailActivity.class);
                Bundle data= new Bundle();
                data.putSerializable("boat", boats.get(getAdapterPosition()));
                intent.putExtras(data);
                context.startActivity(intent);

        }
    }
}
