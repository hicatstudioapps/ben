package com.muevaelvolante.barapp.adapters.results;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.muevaelvolante.Aplication;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.fragment.ResultFragment;
import com.muevaelvolante.barapp.model.results.Competitor;
import com.muevaelvolante.barapp.model.results.Competitor_;
import com.muevaelvolante.barapp.model.results.Total;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by CQ on 15/04/2016.
 */
public class RaceOverallAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    AppCompatActivity context;
    ResultFragment fragment;
    public void setCompetitors(List<Total> competitors,  List<Competitor> competitorList) {
        this.total = competitors;
        this.competitorList=competitorList;
    }

    List<Total> total;
    List<Competitor> competitorList;
    View previousSelected;

    public RaceOverallAdapter(AppCompatActivity context,  List<Total> raceCompetitor,List<Competitor> competitorList,ResultFragment fragment) {
        this.context = context;
        this.total = raceCompetitor;
        this.competitorList=competitorList;
        this.fragment=fragment;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(context.getLayoutInflater().inflate(R.layout.race_result, null));
    }

    Competitor findById(String id){
        for(Competitor c: competitorList){
            if(c.getCompetitorid().equals(id))
                return c;
        }
        return null;
    }
    public SpannableStringBuilder setBold(String text){
        final SpannableStringBuilder sb = new SpannableStringBuilder(text);

//// Span to set text color to some RGB value
//        final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.WHITE);

// Span to make text bold
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);

//// Set the text color for first 4 characters
//        sb.setSpan(fcs, 0, getString(R.string.app_name).length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

// make them also bold
        sb.setSpan(bss, 0,text.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        return sb;
    }
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Competitor competitorData=findById(total.get(position).getCompetitorid());
        Log.d("image", competitorData.getMedia());
        Total competitorPosition= total.get(position);
        if (position == 0){
            ((ViewHolder) holder).position.setText(setBold(competitorPosition.getRank() + ""));
            ((ViewHolder) holder).position.setTypeface(Aplication.robotoM);
            ((ViewHolder) holder).team.setText(setBold(competitorData.getName()));
            ((ViewHolder) holder).team.setTypeface(Aplication.robotoM);
            ImageLoader.getInstance().displayImage(competitorData.getMedia(), ((ViewHolder) holder).flag);
            ((ViewHolder) holder).score.setText(setBold(competitorPosition.getPoints()));
            ((ViewHolder) holder).score.setTypeface(Aplication.robotoM);
        }
        else{
            ((ViewHolder) holder).position.setText(competitorPosition.getRank()+"");
            ((ViewHolder) holder).position.setTypeface(Aplication.robotoR);
            ((ViewHolder) holder).team.setText(competitorData.getName());
            ((ViewHolder) holder).team.setTypeface(Aplication.robotoR);
            ImageLoader.getInstance().displayImage(competitorData.getMedia(), ((ViewHolder) holder).flag);
            ((ViewHolder) holder).score.setText(competitorPosition.getPoints());
            ((ViewHolder) holder).score.setTypeface(Aplication.robotoR);
        }
    }

    @Override
    public int getItemCount() {
        return total.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.position)
        TextView position;
        @Bind(R.id.team)
        TextView team;
        @Bind(R.id.score)
        TextView score;
        @Bind(R.id.flag)
        ImageView flag;

       public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
