package com.muevaelvolante.barapp.adapters.results;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.muevaelvolante.Aplication;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.fragment.ResultFragment;
import com.muevaelvolante.barapp.model.results.Regattaresults;
import com.muevaelvolante.barapp.model.results.Result;
import com.muevaelvolante.barapp.util.Utils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by CQ on 14/04/2016.
 */
public class RegataAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    AppCompatActivity context;
    ViewHolder lastHolder;
    int lastPosition;
    ResultFragment fragment;

    public void setFirstMarked(boolean firstMarked) {
        this.firstMarked = firstMarked;
    }

    boolean firstMarked=false;

    public void setRegatas(List<Result> regatas) {
        this.regatas = regatas;
    }

    List<Result> regatas;
    View previousSelected;

    public RegataAdapter(AppCompatActivity context, List<Result> regatas,  ResultFragment fragment) {
        this.context = context;
        this.regatas = regatas;
        this.fragment=fragment;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=context.getLayoutInflater().inflate(R.layout.regata_item,null);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        view.setLayoutParams(lp);
        return new ViewHolder(view);
    }
    public SpannableStringBuilder setBold(String text){
        final SpannableStringBuilder sb = new SpannableStringBuilder(text);

//// Span to set text color to some RGB value
//        final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.WHITE);

// Span to make text bold
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);

//// Set the text color for first 4 characters
//        sb.setSpan(fcs, 0, getString(R.string.app_name).length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

// make them also bold
        sb.setSpan(bss, 0,text.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        return sb;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder hold=(ViewHolder)holder;
        hold.year.setText(setBold(regatas.get(position).getTitle()));
        if(position==0 && !firstMarked){
            lastPosition=0;
            hold.selected.setVisibility(View.VISIBLE);
            hold.year.setTypeface(Aplication.robotoM);
            lastHolder=hold;
            firstMarked=true;
        }else{
            hold.selected.setVisibility(View.INVISIBLE);
            hold.year.setText(setBold(regatas.get(position).getTitle()));
            hold.year.setTypeface(Aplication.robotoL);
        }
    }

    @Override
    public int getItemCount() {
        return regatas.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        @Bind(R.id.year)
        TextView year;
        @Bind(R.id.selected)
        View selected;
        View root;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);root=itemView;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(getAdapterPosition()!=lastPosition){
                        lastHolder.selected.setVisibility(View.INVISIBLE);
                        lastHolder.year.setText(setBold(regatas.get(lastPosition).getTitle()));
                        lastHolder.year.setTypeface(Aplication.robotoL);

                        lastPosition=getAdapterPosition();
                        selected.setVisibility(View.VISIBLE);
                        year.setText(setBold(regatas.get(getAdapterPosition()).getTitle()));
                        year.setTypeface(Aplication.robotoM);
                        lastHolder=ViewHolder.this;
                        fragment.loadDataForRegata(regatas.get(getAdapterPosition()).getRegattaresults());
                    }
                }
            });
        }
    }
}
