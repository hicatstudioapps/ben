package com.muevaelvolante.barapp.adapters.AC;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.muevaelvolante.Aplication;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.activity.ACDetail;
import com.muevaelvolante.barapp.activity.AcGalleryActivity;
import com.muevaelvolante.barapp.model.AC.AcTheGuide;
import com.muevaelvolante.barapp.util.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by CQ on 18/04/2016.
 */
public class ACAdapter extends RecyclerView.Adapter<ACAdapter.ViewHolder> {

    List<AcTheGuide> guideList;
    AppCompatActivity context;

    public ACAdapter(List<AcTheGuide> guideList, AppCompatActivity context) {
        this.guideList = guideList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(context.getLayoutInflater().inflate(R.layout.ac_item,parent,false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        AcTheGuide item= guideList.get(position);
        ImageLoader.getInstance().displayImage(item.getMedia().get(0).getMed(),holder.image);
        holder.title.setText(item.getTitle());
        holder.title.setTypeface(Aplication.robotoB);
        holder.sub_title.setTypeface(Aplication.robotoL);
        holder.sub_title.setText(item.getShortText());
    }

    @Override
    public int getItemCount() {
        return guideList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.imageView2)
        ImageView image;
        @Bind(R.id.title)
        TextView title;
        @Bind(R.id.sub_title)
        TextView sub_title;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            int screenWidth = Utils.getScreenWidth((AppCompatActivity) context);
            int finalHeight = (int) (screenWidth / 2);
            image.setMinimumHeight(finalHeight / 1);
            image.setMaxHeight(finalHeight / 1);
            image.setAdjustViewBounds(false);
            //set height as layoutParameter too
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) image.getLayoutParams();
            lp.height = finalHeight / 1;
            lp.width=screenWidth;
            image.setLayoutParams(lp);
            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(getAdapterPosition()!=0){
                        Intent intent= new Intent(context, ACDetail.class);
                        Bundle data= new Bundle();
                        data.putSerializable("news",(AcTheGuide)guideList.get(getAdapterPosition()));
                        intent.putExtras(data);
                        context.startActivity(intent);
                    }else{
                        Intent intent= new Intent(context, AcGalleryActivity.class);
                        Bundle data= new Bundle();
                        data.putSerializable("gal",(AcTheGuide)guideList.get(getAdapterPosition()));
                        intent.putExtras(data);
                        context.startActivity(intent);
                    }

                }
            });
        }
    }
}
