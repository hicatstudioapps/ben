package com.muevaelvolante.barapp.adapters.community;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.muevaelvolante.barapp.HomeActivity;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.activity.CameraActivity;
import com.muevaelvolante.barapp.model.social.FollowU;
import com.muevaelvolante.barapp.model.social.PhotoShare;
import com.muevaelvolante.barapp.model.social.SocialBanner;
import com.muevaelvolante.barapp.model.social.Social_;
import com.muevaelvolante.barapp.util.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by CQ on 22/04/2016.
 */
public class CommunityAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public void setSocial_list(List<Social_> social_list) {
        this.social_list = social_list;
    }

    List<Social_> social_list;
    List<FollowU> follows;
    List<PhotoShare> photo_share;
    Context context;
    SocialBanner banner;
    ViewPager photo_pager;

    public CommunityAdapter(Context context,List<Social_> social_list,SocialBanner banner,
                            List<FollowU> follows,List<PhotoShare> photo_share) {
        this.social_list = social_list;
        this.context = context;
        this.banner=banner;
        this.follows=follows;
        this.photo_share=photo_share;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case -1:
                View view = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.social_item_face, null);
                return new ViewHolder(view);
            case 1://ben
                View viewV = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.ben_content, null);
                return new BannerTopViewHolder(viewV);
            case 2://photo
                View viewS = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.section_follow, null);
                return new FollowViewHolder(viewS);
            case 3://support
                View viewG = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.photo_pager, null);
                return new PhotoViewHolder(viewG);

        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        if(position>=3)
            return -1;
        if(position==0)
            return 1;
        if (position==1)
            return 2;
        if (position==2)
            return 3;
        if(position==3)
            return 4;
        return -1;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {
            case -1:
                final Social_ social = social_list.get(position - 3);
                if (social.getType().toLowerCase().equals("facebook")) {
                    ((ViewHolder) holder).fab.setImageResource(R.drawable.f_indicator);
                    ((ViewHolder) holder).fab2.setImageResource(R.drawable.f_indicator);
                } else if (social.getType().toLowerCase().equals("twitter")) {
                    ((ViewHolder) holder).fab.setImageResource(R.drawable.t_indicator);
                    ((ViewHolder) holder).fab2.setImageResource(R.drawable.t_indicator);
                } else {
                    ((ViewHolder) holder).fab.setImageResource(R.drawable.inst_indicator);
                    ((ViewHolder) holder).fab2.setImageResource(R.drawable.inst_indicator);
                }

                ((ViewHolder) holder).tittle.setText(social.getTitle());
                ((ViewHolder) holder).subtitle.setText(social.getTitle());
                if (social.getMedia().equals("")) {
                    ((ViewHolder) holder).image.setVisibility(View.GONE);
                    ((ViewHolder) holder).tittle_container.setVisibility(View.GONE);
                    ((ViewHolder) holder).subtittle_container.setVisibility(View.VISIBLE);
                } else {
                    if (social.getTitle().equals(""))
                        ((ViewHolder) holder).tittle_container.setVisibility(View.GONE);
                    else
                        ((ViewHolder) holder).tittle_container.setVisibility(View.VISIBLE);
                        ImageLoader.getInstance().displayImage(social.getMedia(), ((ViewHolder) holder).image, new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String s, View view) {

                        }

                        @Override
                        public void onLoadingFailed(String s, View view, FailReason failReason) {

                        }

                        @Override
                        public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                            float proportion= (float)bitmap.getWidth()/bitmap.getHeight();
                            int alto= (int) (((Utils.getScreenWidth((Activity) context)))/proportion);
                            RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams) ((ViewHolder) holder).image.getLayoutParams();
                            lp.height=alto;
                            lp.width=Utils.getScreenWidth((Activity) context);
                            ((ViewHolder) holder).image.setLayoutParams(lp);
                        }

                        @Override
                        public void onLoadingCancelled(String s, View view) {

                        }
                    });
                    ((ViewHolder) holder).image.setVisibility(View.VISIBLE);
                    ((ViewHolder) holder).subtittle_container.setVisibility(View.GONE);
                }
                //aqui va la fecha
                ((ViewHolder) holder).date.setText(Utils.getDateString(Utils.convertStringToDate(social.getDate())));
                ((ViewHolder) holder).date2.setText(Utils.getDateString(Utils.convertStringToDate(social.getDate())));
                ((ViewHolder) holder).root.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        context.startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse(social.getUrl())));
                    }
                });
                break;

            case 1:
                final BannerTopViewHolder bannerTopViewHolder =(BannerTopViewHolder)holder;
                ImageLoader.getInstance().displayImage(banner.getMedia().get(0).getMed(), bannerTopViewHolder.image, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String s, View view) {

                    }

                    @Override
                    public void onLoadingFailed(String s, View view, FailReason failReason) {

                    }

                    @Override
                    public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                        float proportion= (float)bitmap.getWidth()/bitmap.getHeight();
                        int alto= (int) (Utils.getScreenWidth((Activity) context)/proportion);
                        RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams) bannerTopViewHolder.image.getLayoutParams();
                        lp.height=alto;
                        lp.width=Utils.getScreenWidth((Activity) context);
                        bannerTopViewHolder.image.setLayoutParams(lp);
                    }

                    @Override
                    public void onLoadingCancelled(String s, View view) {

                    }
                });
                break;

            case 2:
                FollowViewHolder followViewHolder= (FollowViewHolder)holder;
                for(int i=0; i< follows.size();i++){
                    switch (i){
                        case 0:
                            ImageLoader.getInstance().displayImage(follows.get(i).getMedia().get(0).getMed(),followViewHolder.fi0);
                            followViewHolder.f0.setVisibility(View.VISIBLE);
                            followViewHolder.f0.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(follows.get(0).getUrl())));
                                }
                            });
                            break;
                        case 1:
                            ImageLoader.getInstance().displayImage(follows.get(i).getMedia().get(0).getMed(),followViewHolder.fi1);
                            followViewHolder.f1.setVisibility(View.VISIBLE);
                            followViewHolder.f1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(follows.get(1).getUrl())));
                                }
                            });
                            break;
                        case 2:
                            ImageLoader.getInstance().displayImage(follows.get(i).getMedia().get(0).getMed(),followViewHolder.fi2);
                            followViewHolder.f2.setVisibility(View.VISIBLE);
                            followViewHolder.f2.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(follows.get(2).getUrl())));
                                }
                            });
                            break;
                        case 3:
                            ImageLoader.getInstance().displayImage(follows.get(i).getMedia().get(0).getMed(),followViewHolder.fi3);
                            followViewHolder.f3.setVisibility(View.VISIBLE);
                            followViewHolder.f3.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(follows.get(3).getUrl())));
                                }
                            });
                            break;
                    }
                }
                break;
            case 3:
                PhotoViewHolder photoViewHolder=(PhotoViewHolder)holder;
                photoViewHolder.pager.setAdapter(new PhotoAdapter());
                photoViewHolder.indicator.setViewPager(photoViewHolder.pager);
                photoViewHolder.tittle.setText(photo_share.get(0).getTitle());
                photoViewHolder.extra_text.setText(photo_share.get(0).getExtraText());
                photoViewHolder.short_text.setText(photo_share.get(0).getShortText());
                break;
        }
    }




    @Override
    public int getItemCount() {
        return social_list.size()+3;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.root)
        View root;
        @Bind(R.id.fab)
        ImageView fab;
        @Bind(R.id.date)
        TextView date;
        @Bind(R.id.image)
        ImageView image;
        @Bind(R.id.title)
        TextView tittle;
        @Bind(R.id.subTitle)
        TextView subtitle;
        @Bind(R.id.tittle_container)
                View tittle_container;
        @Bind(R.id.subtittle_container)
                View subtittle_container;
        @Bind(R.id.date2)
                TextView date2;
        @Bind(R.id.fab2)
        ImageView fab2;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            int screenWidth = Utils.getScreenWidth((AppCompatActivity) context);
            int finalHeight = (int) (screenWidth / 2);
            image.setMinimumHeight(finalHeight / 1);
            image.setMaxHeight(finalHeight / 1);
            image.setAdjustViewBounds(false);
            //set height as layoutParameter too
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) image.getLayoutParams();
            lp.height = finalHeight / 1;
            image.setLayoutParams(lp);
//            image.setImageDrawable(context.getResources().getDrawable(R.drawable.ben));
        }
    }

    public class BannerTopViewHolder extends RecyclerView.ViewHolder{

        @Bind(R.id.imageButton)
        ImageView image;
        public BannerTopViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            int screenWidth = (int) (Utils.getScreenWidth((AppCompatActivity) context));
            image.setAdjustViewBounds(false);
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) image.getLayoutParams();
            lp.width=screenWidth;
            lp.height= (int) (screenWidth*0.7);
            image.setLayoutParams(lp);
        }

    }

    public class PhotoViewHolder extends RecyclerView.ViewHolder{
        @Bind(R.id.pager)
        ViewPager pager;
        @Bind(R.id.tittle)
        TextView tittle;
        @Bind(R.id.short_text)
        TextView short_text;
        @Bind(R.id.extra_text)
        TextView extra_text;
        @Bind(R.id.indicator)
        CirclePageIndicator indicator;

        public PhotoViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            pager=(ViewPager)itemView.findViewById(R.id.pager);
            pager.setOffscreenPageLimit(50);
            //photo_pager=pager;
            itemView.findViewById(R.id.imageButton).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent= new Intent(context, CameraActivity.class);
                    intent.putExtra("url",photo_share.get(0).getMedia().get(pager.getCurrentItem()).getMed());
                    context.startActivity(intent);
                }
            });

        }
    }

    public class FollowViewHolder extends RecyclerView.ViewHolder{

        @Bind(R.id.root)
        LinearLayout root;
        @Bind(R.id.f0)
        View f0;
        @Bind(R.id.f1)
        View f1;
        @Bind(R.id.f2)
        View f2;
        @Bind(R.id.f3)
        View f3;
        @Bind(R.id.fi0)
        ImageView fi0;
        @Bind(R.id.fi1)
        ImageView fi1;
        @Bind(R.id.fi2)
        ImageView fi2;
        @Bind(R.id.fi3)
        ImageView fi3;


        public FollowViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            int with= Utils.getScreenWidth((HomeActivity)context);
            LinearLayout.LayoutParams lp= new LinearLayout.LayoutParams(with, LinearLayout.LayoutParams.WRAP_CONTENT);
            lp.width=with;
            root.setLayoutParams(lp);

        }

    }

    class PhotoAdapter extends PagerAdapter {

        @Bind(R.id.imageView6)
        ImageView image;
        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
           View view= ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.photo_pager_item,container,false);
            ButterKnife.bind(this,view);
            RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams) image.getLayoutParams();
            int screenWidth = Utils.getScreenWidth((AppCompatActivity) context);
            lp.width=screenWidth;
            image.setLayoutParams(lp);
            ImageLoader.getInstance().displayImage(photo_share.get(0).getMedia().get(position).getMed(),image);
            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent= new Intent(context, CameraActivity.class);
                    intent.putExtra("url",photo_share.get(0).getMedia().get(position).getMed());
                    context.startActivity(intent);
                }
            });
            container.addView(view);
            return view;
        }

        @Override
        public int getCount() {
            return photo_share.get(0).getMedia().size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

        @Override
        public void destroyItem(View container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }

    }
}
