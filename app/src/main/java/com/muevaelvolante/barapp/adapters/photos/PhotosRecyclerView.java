package com.muevaelvolante.barapp.adapters.photos;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.muevaelvolante.Aplication;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.activity.GalleryActivity;
import com.muevaelvolante.barapp.model.home.Medium___;
import com.muevaelvolante.barapp.model.photos.Gallery;
import com.muevaelvolante.barapp.util.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.LinkedList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by CQ on 15/06/2016.
 */
public class PhotosRecyclerView extends RecyclerView.Adapter<PhotosRecyclerView.ViewHolder> {


    List<Gallery> galleries;
    Context context;

    public PhotosRecyclerView(Context context, List<Gallery> galleries) {
        this.galleries = galleries;
        this.context=context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout= ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.fragment_gallery_list_item,null);
        return new ViewHolder(layout);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ViewHolder gal_holder=holder;
        Gallery gallery= galleries.get(position);
        gal_holder.tittle.setText(gallery.getTitle());
        ImageLoader.getInstance().displayImage(Uri.parse(gallery.getMedia().get(0).getLo()).toString(), gal_holder.image);
        gal_holder.date.setText(Utils.getDateString(Utils.convertStringToDate(gallery.getDate())));
        if(!TextUtils.isEmpty(gallery.getLocked()) && Aplication.isAnonimous){
            gal_holder.unlock.setVisibility(View.VISIBLE);
            gal_holder.buttons.setVisibility(View.VISIBLE);
        }else
            //buttons.setVisibility(View.GONE);
            gal_holder.play.setImageResource(R.drawable.gallery);
    }

    @Override
    public int getItemCount() {
        return galleries.size();
    }

    public class ViewHolder extends  RecyclerView.ViewHolder{
        @Bind(R.id.date)
        TextView date;
        @Bind(R.id.image)
        ImageView image;
        @Bind(R.id.title)
        TextView tittle;
        @Bind(R.id.buttons)
        View buttons;
        @Bind(R.id.play)
        ImageView play;
        @Bind(R.id.unlock)
        View unlock;
        @Bind(R.id.next)
        View next;
        @Bind(R.id.prev)
        View prev;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            int screenWidth = Utils.getScreenWidth((AppCompatActivity)context);
            int finalHeight = (int) (screenWidth / 2);
            image.setMinimumHeight(finalHeight / 1);
            image.setMaxHeight(finalHeight / 1);
            image.setAdjustViewBounds(false);
            //set height as layoutParameter too
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) image.getLayoutParams();
            lp.height = finalHeight / 1;
            image.setLayoutParams(lp);
            buttons.setLayoutParams(lp);
            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent galery= new Intent(context,GalleryActivity.class);
                    Bundle data= new Bundle();
                    com.muevaelvolante.barapp.model.home.Gallery gallery= new com.muevaelvolante.barapp.model.home.Gallery();
                    List<Medium___> media= new LinkedList<Medium___>();
                    for (int i = 0; i < galleries.get(getAdapterPosition()).getMedia().size(); i++) {
                        Medium___ med= new Medium___();
                        med.setHi(galleries.get(getAdapterPosition()).getMedia().get(i).getHi());
                        med.setMed(galleries.get(getAdapterPosition()).getMedia().get(i).getMed());
                        media.add(med);
                    }
                    gallery.setMedia(media);
                    data.putSerializable("gal",gallery);
                    galery.putExtras(data);
                    context.startActivity(galery);
                }
            });
        }
    }
}
