package com.muevaelvolante.barapp.adapters.videos;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.muevaelvolante.Aplication;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.activity.BoatA360ctivity;
import com.muevaelvolante.barapp.activity.GalleryActivity;
import com.muevaelvolante.barapp.activity.Google360;
import com.muevaelvolante.barapp.model.home.Medium___;
import com.muevaelvolante.barapp.model.photos.Gallery;
import com.muevaelvolante.barapp.model.videos.Video;
import com.muevaelvolante.barapp.util.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.LinkedList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by CQ on 15/06/2016.
 */
public class VideosRecyclerView extends RecyclerView.Adapter<VideosRecyclerView.ViewHolder> {


    List<Video> videos;
    Context context;

    public VideosRecyclerView(Context context, List<Video> videos) {
        this.videos = videos;
        this.context=context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout= ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.fragment_gallery_list_item,null);
        return new ViewHolder(layout);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ViewHolder vi_holder=holder;
        Video video= videos.get(position);
        vi_holder.tittle.setText(Html.fromHtml(video.getTitle()));
        //ImageLoader.getInstance().displayImage(Uri.parse(video.getMedia().get(0).getLo()).toString(), vi_holder.image);
        vi_holder.date.setText(Utils.getDateString(Utils.convertStringToDate(video.getDate())));
        final com.amazonaws.com.google.gson.internal.LinkedTreeMap<String,String> map= (com.amazonaws.com.google.gson.internal.LinkedTreeMap) video.getMedia().get(0);
        ImageLoader.getInstance().displayImage(Uri.parse(map.get("med")).toString(), vi_holder.image);
        if(!TextUtils.isEmpty(video.getLocked()) && Aplication.isAnonimous){
            vi_holder.unlock.setVisibility(View.VISIBLE);
            vi_holder.buttons.setVisibility(View.VISIBLE);
        }else
            //buttons.setVisibility(View.GONE);
            vi_holder.play.setImageResource(R.drawable.video);
        if(video.getType().toLowerCase().equals("video360")){
            vi_holder.play.setImageResource(R.drawable.video360);
            vi_holder.play.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent video360= new Intent(context, Google360.class);
                    video360.putExtra("url",map.get("video360"));
                    context.startActivity(video360);
                    //Utils.playVideoYoutube(context, media.getVideo());
                }
            });
        }else
            vi_holder.play.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Medium__ media = video.getMedia().get(0);
                    Utils.playVideoYoutube(context, map.get("video"));
                }
            });
    }

    @Override
    public int getItemCount() {
        return videos.size();
    }

    public class ViewHolder extends  RecyclerView.ViewHolder{
        @Bind(R.id.date)
        TextView date;
        @Bind(R.id.image)
        ImageView image;
        @Bind(R.id.title)
        TextView tittle;
        @Bind(R.id.buttons)
        View buttons;
        @Bind(R.id.play)
        ImageView play;
        @Bind(R.id.unlock)
        View unlock;
        @Bind(R.id.next)
        View next;
        @Bind(R.id.prev)
        View prev;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            int screenWidth = Utils.getScreenWidth((AppCompatActivity)context);
            int finalHeight = (int) (screenWidth / 2);
            image.setMinimumHeight(finalHeight / 1);
            image.setMaxHeight(finalHeight / 1);
            image.setAdjustViewBounds(false);
            //set height as layoutParameter too
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) image.getLayoutParams();
            lp.height = finalHeight / 1;
//            container.setMinimumHeight(finalHeight);
//            container.setLayoutParams(lp);
            image.setLayoutParams(lp);
            buttons.setLayoutParams(lp);
//            image.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent galery= new Intent(context,GalleryActivity.class);
//                    Bundle data= new Bundle();
//                    com.muevaelvolante.barapp.model.home.Gallery gallery= new com.muevaelvolante.barapp.model.home.Gallery();
//                    List<Medium___> media= new LinkedList<Medium___>();
//                    for (int i = 0; i < videos.get(getAdapterPosition()).getMedia().size(); i++) {
//                        Medium___ med= new Medium___();
//                        med.setHi(videos.get(getAdapterPosition()).getMedia().get(i).getHi());
//                        med.setMed(videos.get(getAdapterPosition()).getMedia().get(i).getMed());
//                        media.add(med);
//                    }
//                    gallery.setMedia(media);
//                    data.putSerializable("gal",gallery);
//                    galery.putExtras(data);
//                    context.startActivity(galery);
//                }
//            });
        }
    }
}
