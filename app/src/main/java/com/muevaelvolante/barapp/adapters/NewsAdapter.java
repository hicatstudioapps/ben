package com.muevaelvolante.barapp.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.media.Image;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.facebook.internal.Logger;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.internal.ObjectConstructor;
import com.muevaelvolante.Aplication;
import com.muevaelvolante.barapp.HomeActivity;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.activity.BoatA360ctivity;
import com.muevaelvolante.barapp.activity.GalleryActivity;
import com.muevaelvolante.barapp.activity.Google360;
import com.muevaelvolante.barapp.activity.NewsDetailActivity;
import com.muevaelvolante.barapp.custom.CustomTextureVideoView;
import com.muevaelvolante.barapp.model.GenericModel;
import com.muevaelvolante.barapp.model.home.HomeHighlight;
import com.muevaelvolante.barapp.model.home.HomeNews;
import com.muevaelvolante.barapp.model.social.Social;
import com.muevaelvolante.barapp.model.social.Social_;
import com.muevaelvolante.barapp.util.TimberUtils;
import com.muevaelvolante.barapp.util.Utils;
import com.muevaelvolante.timely.TimelyView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.viewpagerindicator.CirclePageIndicator;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import jp.wasabeef.blurry.Blurry;

/**
 * Created by CQ on 24/03/2016.
 */
public class NewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    static int NEWS_ARTICLE=0;
    static int NEWS_VIDEO=1;
    static int NEWS_GALLERY=2;
    int unActiveColor=Color.parseColor("#7FA7A9AC");
    int activeColor=Color.parseColor("#a7a9ac");
    int newsActive;
    private CountDownAdapter countDownAdapter;
    private VideoPageAdapter videoPageAdapter;

    public void setNews(List<GenericModel> news) {
        this.news = news;
    }

    List<GenericModel> news;
    List<Object> data;
    List<com.muevaelvolante.barapp.model.home.Video> videos;
    List<com.muevaelvolante.barapp.model.home.Gallery> galleries;
    List<com.muevaelvolante.barapp.model.home.HomeHighlight> homeHighlights;
    List<Social_> socials;
    List<HomeNews> homenews;



    ViewPager highLightViewPager;

    public NewsAdapter(Context context, List<Object> news, List<com.muevaelvolante.barapp.model.home.Video> videos,
                       List<com.muevaelvolante.barapp.model.home.Gallery> galleries, List<HomeHighlight> homeHighlights,
                       List<Social_> socials, List<HomeNews> homenews, int newsActive) {
        this.context = context;
        this.data = news;
        this.videos=videos;
        this.galleries=galleries;
        this.homeHighlights=homeHighlights;
        this.socials= socials;
        this.homenews=homenews;
        this.newsActive=newsActive;
    }

    @Override
    public int getItemViewType(int position) {
        /*
        * el tipo de vista lo doy en dependencia del atributo type
        * countdow:0
        * goto:1
        * video360:2
        * video:3
        * article:4
        * gallery:5
        * --Dividers--
        * 6-divider article
        * 7-divider video
        * 8-divider gallery
        * 9-video slide
        * */
        String type="";
        if(data.get(position) instanceof HomeHighlight)
            type=((HomeHighlight)data.get(position)).getType().toLowerCase();
        if(data.get(position) instanceof com.muevaelvolante.barapp.model.home.Video)
            type=((com.muevaelvolante.barapp.model.home.Video)data.get(position)).getType().toLowerCase();
        if(data.get(position) instanceof com.muevaelvolante.barapp.model.home.Gallery)
            type=((com.muevaelvolante.barapp.model.home.Gallery)data.get(position)).getType().toLowerCase();
        if(data.get(position) instanceof com.muevaelvolante.barapp.model.home.HomeNews)
            type=((com.muevaelvolante.barapp.model.home.HomeNews)data.get(position)).getType().toLowerCase();
        if(data.get(position) instanceof Social_)
            return 11;
        if(data.get(position) instanceof String)
            type= String.valueOf(data.get(position));
        if(type.equals("hl"))
            return 0;
        if(type.equals("goto"))
            return 1;
        if(type.equals("video360"))
            return 2;
        if(type.equals("video"))
            return 3;
        if(type.equals("article"))
            return 4;
        if(type.equals("gallery"))
            return 5;
        if(type.equals("0"))//divider
            return 6;
        if(type.equals("1"))
            return 7;
        if(type.equals("2"))
            return 8;
        if(type.equals("video_slide"))
            return 9;
        if(type.equals("gallery_slide"))
            return 10;
        return NEWS_GALLERY;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
               case 0:
                   View viewC = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.countdown_item_video_pager, null);
                return new CountDownViewHolder(viewC);
            case 1:
                View viewGT = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.new_item, null);
                return new ViewHolder(viewGT);
            case 2:
                View view360 = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.new_item, null);
                return new ViewHolder(view360);
            case 3:
                View viewVV = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.new_item, null);
                return new ViewHolder(viewVV);
            case 4:
                View view = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.new_item, null);
                return new ViewHolder(view);
            case 6:
                View viewD = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.home_tap_section, null);
                return new DividerViewHolder(viewD);
            case 9:
                View viewV = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.news_item_video_pager, null);
                return new VideoViewHolder(viewV);
            case 10://            case 2:
                View viewG = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.news_item_gallery_pager, null);
                return new GalleryViewHolder(viewG);
            case 11:
                View viewS = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.social_item_face, null);
                return new SocialViewHolder(viewS);

        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()){
            case 4:
                HomeNews news= (HomeNews) data.get(position);
                //pongo un tag para saber si la noticia esta bloqueada
                ((ViewHolder)holder).root.setTag(news.getLocked());
                ((ViewHolder)holder).fab.setImageResource(R.drawable.a_indicator);
                ((ViewHolder)holder).tittle.setText(news.getTitle());
                ((ViewHolder)holder).section.setText(news.getLabel());
                if(news.getMedia()!=null && news.getMedia().size()>0)
                    ImageLoader.getInstance().displayImage(((LinkedTreeMap<String, String>) news.getMedia().get(0)).get("med"), ((ViewHolder) holder).image, new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String s, View view) {

                        }

                        @Override
                        public void onLoadingFailed(String s, View view, FailReason failReason) {

                        }

                        @Override
                        public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                            float proportion= (float)bitmap.getWidth()/bitmap.getHeight();
                            int alto= (int) (Utils.getScreenWidth((Activity) context)/proportion);
                            RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams) ((ViewHolder) holder).image.getLayoutParams();
                            lp.height=alto;
                            lp.width=Utils.getScreenWidth((Activity) context);
                            ((ViewHolder) holder).image.setLayoutParams(lp);
                        }

                        @Override
                        public void onLoadingCancelled(String s, View view) {

                        }
                    });
                else
                    ((ViewHolder)holder).image.setVisibility(View.GONE);
                //escondo los iconos de bloqueo
                if(news.getLocked().equals("1")&& Aplication.isAnonimous){
                    ((ViewHolder)holder).unlock.setVisibility(View.VISIBLE);
                    ((ViewHolder)holder).buttons.setVisibility(View.VISIBLE);
                    ((ViewHolder)holder).buttons.setBackgroundColor(Color.parseColor("#C8000000"));
                }
                else{
                    ((ViewHolder)holder).unlock.setVisibility(View.GONE);
                    ((ViewHolder)holder).buttons.setVisibility(View.GONE);
                    ((ViewHolder)holder).buttons.setBackgroundColor(Color.TRANSPARENT);
                }
                ((ViewHolder)holder).play.setVisibility(View.GONE);
                //aqui va la fecha
                ((ViewHolder)holder).date.setText(Utils.getDateString(Utils.convertStringToDate(news.getDate())));
                //aqui lo del microclip
                ((ViewHolder)holder).videoUrl=news.getMicroclip();
                ((ViewHolder)holder).microclip.setTag(news.getMicroclip());
                break;
            case 1:
                HomeHighlight highlight= (HomeHighlight) data.get(position);
                ViewHolder gotoHolder=(ViewHolder)holder;
                gotoHolder.section.setText(highlight.getLabel());
                gotoHolder.tittle.setText(highlight.getTitle());
                final LinkedTreeMap<String,String> map= (LinkedTreeMap<String, String>) highlight.getMedia().get(0);
                ImageLoader.getInstance().displayImage(map.get("med"),gotoHolder.image);
                gotoHolder.date.setText(Utils.getDateString(Utils.convertStringToDate(highlight.getDate())));
                gotoHolder.play.setVisibility(View.GONE);
                break;
            case 2:
                final HomeHighlight highlightVideo360= (HomeHighlight) data.get(position);
                ViewHolder videoHolder360=(ViewHolder)holder;
                videoHolder360.section.setText(highlightVideo360.getLabel());
                videoHolder360.tittle.setText(highlightVideo360.getTitle());
                final LinkedTreeMap<String,String> map360= (LinkedTreeMap<String, String>) highlightVideo360.getMedia().get(0);
                ImageLoader.getInstance().displayImage(map360.get("med"),videoHolder360.image);
                videoHolder360.date.setText(Utils.getDateString(Utils.convertStringToDate(highlightVideo360.getDate())));
                videoHolder360.play.setImageResource(R.drawable.video360);
                videoHolder360.buttons.setVisibility(View.VISIBLE);
                videoHolder360.image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent video360= new Intent(context,Google360.class);
                        video360.putExtra("url",map360.get("video360"));
                        context.startActivity(video360);
                   }
                });
                break;
            case 3:
                final HomeHighlight highlightVideo= (HomeHighlight) data.get(position);
                ViewHolder videoHolder=(ViewHolder)holder;
                videoHolder.section.setText(highlightVideo.getLabel());
                videoHolder.tittle.setText(highlightVideo.getTitle());
                final LinkedTreeMap<String,String> mapvideo= (LinkedTreeMap<String, String>) highlightVideo.getMedia().get(0);
                ImageLoader.getInstance().displayImage(mapvideo.get("med"),videoHolder.image);
                videoHolder.date.setText(Utils.getDateString(Utils.convertStringToDate(highlightVideo.getDate())));
                videoHolder.play.setImageResource(R.drawable.video);
                videoHolder.buttons.setVisibility(View.VISIBLE);
                videoHolder.image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Utils.playVideoYoutube(context,mapvideo.get("video"));
                    }
                });
                break;
//
            case 9:
                VideoViewHolder holderV= (VideoViewHolder) holder;
                if(videoPageAdapter ==null){
                    videoPageAdapter = new VideoPageAdapter(holderV.pager);
                    holderV.pager.setAdapter(videoPageAdapter);
                    holderV.indicator.setViewPager(holderV.pager);
                }
                break;
            case 10:
                GalleryViewHolder holderG= (GalleryViewHolder) holder;
                holderG.pager.setAdapter(new GAdapter(holderG.pager));
                holderG.indicator.setViewPager(holderG.pager);
                break;
            case 6:
                DividerViewHolder dividerViewHolder= (DividerViewHolder)holder;
                if(newsActive==0){
                    dividerViewHolder.news_tap_text.setTextColor(activeColor);
                    dividerViewHolder.social_tap_text.setTextColor(unActiveColor);
                    dividerViewHolder.video_tap_text.setTextColor(unActiveColor);
                }else if(newsActive==1){
                    dividerViewHolder.news_tap_text.setTextColor(unActiveColor);
                    dividerViewHolder.social_tap_text.setTextColor(activeColor);
                    dividerViewHolder.video_tap_text.setTextColor(unActiveColor);
                }
                break;
            case 0://slide de noticias,goto,etc
                CountDownViewHolder countDownViewHolder= (CountDownViewHolder) holder;
                if(countDownAdapter ==null){
                    countDownAdapter = new CountDownAdapter(((CountDownViewHolder) holder).pager);
                    countDownViewHolder.pager.setAdapter(countDownAdapter);
                    countDownViewHolder.indicator.setViewPager(countDownViewHolder.pager);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            startAutoSlide(0);
                        }
                    },5000);
                }
                break;
            case 11:
                final Social_ social = (Social_) data.get(position);
                final SocialViewHolder socialViewHolder=(SocialViewHolder)holder;
                if (social.getType().toLowerCase().equals("facebook")) {
                    socialViewHolder.fab.setImageResource(R.drawable.f_indicator);
                    socialViewHolder.fab2.setImageResource(R.drawable.f_indicator);
                   } else if (social.getType().toLowerCase().equals("twitter")) {
                    socialViewHolder.fab.setImageResource(R.drawable.t_indicator);
                    socialViewHolder.fab2.setImageResource(R.drawable.t_indicator);
                  } else {
                    socialViewHolder.fab.setImageResource(R.drawable.inst_indicator);
                    socialViewHolder.fab2.setImageResource(R.drawable.inst_indicator);
                  }

                socialViewHolder.tittle.setText(social.getTitle());
                socialViewHolder.subtitle.setText(social.getTitle());
                if (social.getMedia().equals("")) {
                    socialViewHolder.image.setVisibility(View.GONE);
                    socialViewHolder.tittle_container.setVisibility(View.GONE);
                    socialViewHolder.subtittle_container.setVisibility(View.VISIBLE);
                } else {
                    if (social.getTitle().equals(""))
                        socialViewHolder.tittle_container.setVisibility(View.GONE);
                    else
                        socialViewHolder.tittle_container.setVisibility(View.VISIBLE);
                    ImageLoader.getInstance().displayImage(social.getMedia(), socialViewHolder.image, new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String s, View view) {

                        }

                        @Override
                        public void onLoadingFailed(String s, View view, FailReason failReason) {

                        }

                        @Override
                        public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                            float proportion= (float)bitmap.getWidth()/bitmap.getHeight();
                            int alto= (int) (((Utils.getScreenWidth((Activity) context)))/proportion);
                            RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams) socialViewHolder.image.getLayoutParams();
                            lp.height=alto;
                            lp.width=Utils.getScreenWidth((Activity) context);
                          socialViewHolder.image.setLayoutParams(lp);
                        }

                        @Override
                        public void onLoadingCancelled(String s, View view) {

                        }
                    }); socialViewHolder.image.setVisibility(View.VISIBLE);
                    socialViewHolder.subtittle_container.setVisibility(View.GONE);
                }
                //aqui va la fecha
                socialViewHolder.date.setText(Utils.getDateString(Utils.convertStringToDate(social.getDate())));
                socialViewHolder.date2.setText(Utils.getDateString(Utils.convertStringToDate(social.getDate())));
                socialViewHolder.root.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        context.startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse(social.getUrl())));
                    }
                });
                break;
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    ViewHolder currentVideoViewHolder;

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.root)
        View root;
        @Bind(R.id.fab)
        ImageView fab;
        @Bind(R.id.date)
        TextView date;
        @Bind(R.id.image)
        ImageView image;
        @Bind(R.id.title)
        TextView tittle;
        @Bind(R.id.buttons)
        View buttons;
        @Bind(R.id.play)
        ImageView play;
        @Bind(R.id.unlock)
        View unlock;
        @Bind(R.id.microclip)
        CustomTextureVideoView microclip;
                @Bind(R.id.section)
                TextView section;


        String videoUrl;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            int screenWidth = Utils.getScreenWidth((AppCompatActivity)context);
            int finalHeight = (int) (screenWidth / 2);
            image.setMinimumHeight(finalHeight / 1);
            image.setMaxHeight(finalHeight / 1);
            image.setAdjustViewBounds(false);
            //set height as layoutParameter too
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) image.getLayoutParams();
            lp.height = finalHeight / 1;
            image.setLayoutParams(lp);
           // imageContainer.setLayoutParams(lp);
            buttons.setLayoutParams(lp);
            ViewGroup.LayoutParams layout = microclip.getLayoutParams();
            layout.width = finalHeight / 1;
            layout.height = finalHeight / 1;
            microclip.setLayoutParams(layout);
            microclip.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(final MediaPlayer mp) {
                    //   Log.v("Video", "onPrepared" + videoView.getVideoPath());
                    int width = mp.getVideoWidth();
                    int height = mp.getVideoHeight();
                    microclip.setIsPrepared(true);
                    // UIUtils.resizeView(videoView, UIUtils.getScreenWidth(getActivity()), UIUtils.getScreenWidth(getActivity()) * height / width);
                    if (currentVideoViewHolder == ViewHolder.this) {
                        play.setVisibility(View.GONE);
                        //  imageLoaderProgressBar.setVisibility(View.INVISIBLE);
                        microclip.setVisibility(View.VISIBLE);
                        mp.setLooping(true);
                        microclip.start();
                        Blurry.with(context)
                                .radius(25)
                                .sampling(1)
                                .color(Color.argb(200, 32, 20, 28))
                                .async()
                                .capture(image)
                                .into((ImageView) image);
                    }
                }
            });
            microclip.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    Log.v("Video", "onFocusChange" + hasFocus);
                    if (!hasFocus && currentVideoViewHolder == ViewHolder.this) {
                        stopVideo();
                    }

                }
            });
            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (data.get(getAdapterPosition()) instanceof HomeNews) {
                        if(((HomeNews)data.get(getAdapterPosition())).getLocked().equals("1") && Aplication.isAnonimous){
                            Toast.makeText(context,"You must login to access these content",Toast.LENGTH_SHORT).show();
                            return;}
                        Intent intent= new Intent(context, NewsDetailActivity.class);
                        Bundle bundle= new Bundle();
                        bundle.putSerializable("news",(HomeNews)data.get(getAdapterPosition()));
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                        return;
                    }

                }
            });
            tittle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (data.get(getAdapterPosition()) instanceof HomeNews) {
                        if(((HomeNews)data.get(getAdapterPosition())).getLocked().equals("1") && Aplication.isAnonimous){
                            Toast.makeText(context,"You must login to access these content",Toast.LENGTH_SHORT).show();
                            return;}
                        Intent intent= new Intent(context, NewsDetailActivity.class);
                        Bundle bundle= new Bundle();
                        bundle.putSerializable("news",(HomeNews)data.get(getAdapterPosition()));
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                        return;
                    }

                }
            });

        }

        public void stopVideo() {
            Log.v("Video", "stopVideo");

            //imageView is within the visible window
            microclip.pause();
            if (microclip.getVisibility() == View.VISIBLE) {
                microclip.setVisibility(View.INVISIBLE);
            }
            play.setVisibility(View.VISIBLE);
//            videoPlayImageButton.setVisibility(View.VISIBLE);
//            imageLoaderProgressBar.setVisibility(View.INVISIBLE);
            currentVideoViewHolder = null;
        }

        public void onScrolled(RecyclerView recyclerView) {
            if (isViewNotVisible(play, recyclerView)) {
                //imageView is within the visible window
                stopVideo();
            }
        }

        public boolean isViewNotVisible(View view, RecyclerView recyclerView) {
            Rect scrollBounds = new Rect();
            recyclerView.getHitRect(scrollBounds);
            return view.getVisibility() == View.VISIBLE && !view.getLocalVisibleRect(scrollBounds);
        }
    }

    public class DividerViewHolder extends RecyclerView.ViewHolder{

        @Bind(R.id.news_tap)
        View news_tap;
        @Bind(R.id.social_tap)
        View social_tap;
        @Bind(R.id.video_tap)
        View video_tap;
        @Bind(R.id.news_tap_text)
        TextView news_tap_text;
        @Bind(R.id.social_tap_text)
        TextView social_tap_text;
        @Bind(R.id.video_tap_text)
        TextView video_tap_text;


        public DividerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            int screenWidth = Utils.getScreenWidth((AppCompatActivity)context);
            int finalHeight = (int) (screenWidth / 2);
            RelativeLayout.LayoutParams lp= new RelativeLayout.LayoutParams(screenWidth,screenWidth/7);
            itemView.setLayoutParams(lp);
            social_tap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    List<Object> temp= new ArrayList<Object>();
                    temp.add(data.get(0));
                    temp.add(data.get(1));
                    temp.addAll(socials);
                    temp.add("video_slide");
                    temp.add("gallery_slide");
                    NewsAdapter.this.data= temp;
                    newsActive=1;
                    news_tap_text.setTextColor(unActiveColor);
                    social_tap_text.setTextColor(activeColor);
                    video_tap_text.setTextColor(unActiveColor);
                    notifyItemRangeRemoved(2,socials.size());
                }
            });
            news_tap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    List<Object> temp= new ArrayList<Object>();
                    temp.add(data.get(0));
                    temp.add(data.get(1));
                    temp.addAll(homenews);
                    temp.add("video_slide");
                    temp.add("gallery_slide");
                    NewsAdapter.this.data= temp;
                    news_tap_text.setTextColor(activeColor);
                    social_tap_text.setTextColor(unActiveColor);
                    video_tap_text.setTextColor(unActiveColor);
                    newsActive=0;
                    if(newsActive==1){
                    notifyItemRangeRemoved(2,homenews.size());}
                    ((HomeActivity)context).getNewsrecycler().scrollToPosition(0);
                }
            });
            video_tap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    List<Object> temp= new ArrayList<Object>();
                    temp.add(data.get(0));
                    temp.add(data.get(1));
                    temp.addAll(homenews);
                    temp.add("video_slide");
                    temp.add("gallery_slide");
                    NewsAdapter.this.data= temp;
                    news_tap_text.setTextColor(activeColor);
                    social_tap_text.setTextColor(unActiveColor);
                   // video_tap_text.setTextColor(activeColor);
                    if(newsActive==1){
                    notifyItemRangeRemoved(2,homenews.size());
                     }
                    newsActive=2;
                    ((HomeActivity)context).getNewsrecycler().scrollToPosition(temp.size()-2);
                }
            });
        }
    }

    public class SocialViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.root)
        View root;
        @Bind(R.id.fab)
        ImageView fab;
        @Bind(R.id.date)
        TextView date;
        @Bind(R.id.image)
        ImageView image;
        @Bind(R.id.title)
        TextView tittle;
        @Bind(R.id.subTitle)
        TextView subtitle;
        @Bind(R.id.tittle_container)
        View tittle_container;
        @Bind(R.id.subtittle_container)
        View subtittle_container;
        @Bind(R.id.date2)
        TextView date2;
        @Bind(R.id.fab2)
        ImageView fab2;
//        @Bind(R.id.buttons)
//        View buttons;
//        @Bind(R.id.play)
//        View play;
//        @Bind(R.id.unlock)
//        View unlock;
//        @Bind(R.id.microclip)
//        CustomTextureVideoView microclip;

        String videoUrl;

        public SocialViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            int screenWidth = Utils.getScreenWidth((AppCompatActivity) context);
            int finalHeight = (int) (screenWidth / 2);
            image.setMinimumHeight(finalHeight / 1);
            image.setMaxHeight(finalHeight / 1);
            image.setAdjustViewBounds(false);
            //set height as layoutParameter too
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) image.getLayoutParams();
            lp.height = finalHeight / 1;
            image.setLayoutParams(lp);
//            image.setImageDrawable(context.getResources().getDrawable(R.drawable.ben));
        }
    }

    public class GalleryViewHolder extends RecyclerView.ViewHolder{

        @Bind(R.id.pager)
        ViewPager pager;
        @Bind(R.id.indicator)
        CirclePageIndicator indicator;
        @Bind(R.id.section_text)
        TextView section_text;
        public GalleryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            pager=(ViewPager)itemView.findViewById(R.id.pager);
            pager.setOffscreenPageLimit(50);
            section_text.setText("Latest Photos");

        }
    }

    class GAdapter extends PagerAdapter {

        @Bind(R.id.date)
        TextView date;
        @Bind(R.id.image)
        ImageView image;
        @Bind(R.id.title)
        TextView tittle;
        @Bind(R.id.buttons)
        View buttons;
        @Bind(R.id.play)
        ImageView play;
        @Bind(R.id.unlock)
        View unlock;
        @Bind(R.id.next)
        View next;
        @Bind(R.id.prev)
        View prev;
        ViewPager pager;

        public GAdapter(ViewPager pager) {
            this.pager=pager;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            final com.muevaelvolante.barapp.model.home.Gallery gallery= galleries.get(position);
            View view= ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.news_item_gallery_pager_item,container,false);
            ButterKnife.bind(this,view);
            int screenWidth = Utils.getScreenWidth((AppCompatActivity)context);
            int finalHeight = (int) (screenWidth / 2);
            image.setMinimumHeight(finalHeight / 1);
            image.setMaxHeight(finalHeight / 1);
            image.setAdjustViewBounds(false);
            //set height as layoutParameter too
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) image.getLayoutParams();
            lp.height = finalHeight / 1;
//            container.setMinimumHeight(finalHeight);
//            container.setLayoutParams(lp);
            image.setLayoutParams(lp);
            buttons.setLayoutParams(lp);
            tittle.setText(gallery.getTitle());
            ImageLoader.getInstance().displayImage(Uri.parse(gallery.getMedia().get(0).getLo()).toString(), image);
            date.setText(Utils.getDateString(Utils.convertStringToDate(gallery.getDate())));
            if(!TextUtils.isEmpty(gallery.getLocked()) && Aplication.isAnonimous){
                unlock.setVisibility(View.VISIBLE);
                buttons.setVisibility(View.VISIBLE);
            }else
            //buttons.setVisibility(View.GONE);
            play.setImageResource(R.drawable.gallery);
            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent galery= new Intent(context,GalleryActivity.class);
                    Bundle data= new Bundle();
                    data.putSerializable("gal",gallery);
                    galery.putExtras(data);
                    context.startActivity(galery);
                }
            });
            if(position< galleries.size()-1){
                next.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(pager.getCurrentItem()+1<=galleries.size()-1)
                            pager.setCurrentItem(pager.getCurrentItem()+1);
                    }
                });
            }else
                next.setVisibility(View.GONE);
            if(position >0 ){
                prev.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(pager.getCurrentItem()-1 >= 0)
                            pager.setCurrentItem(pager.getCurrentItem()-1);
                    }
                });
            }else
                prev.setVisibility(View.GONE);
            container.addView(view);
            return view;
        }

        @Override
        public int getCount() {
            return galleries.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

        @Override
        public void destroyItem(View container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }

    }

    public class VideoViewHolder extends RecyclerView.ViewHolder{
        @Bind(R.id.pager)
        ViewPager pager;
        @Bind(R.id.indicator)
        CirclePageIndicator indicator;

        public VideoViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            pager=(ViewPager)itemView.findViewById(R.id.pager);
            pager.setOffscreenPageLimit(50);
            int screenWidth = Utils.getScreenWidth((AppCompatActivity)context);
            int finalHeight = (int) (screenWidth *0.7);
            RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams) pager.getLayoutParams();
            lp.height=finalHeight;
            pager.setLayoutParams(lp);
        }
    }

    class VideoPageAdapter extends PagerAdapter {

        @Bind(R.id.date)
        TextView date;
        @Bind(R.id.image)
        ImageView image;
        @Bind(R.id.title)
        TextView tittle;
        @Bind(R.id.buttons)
        View buttons;
        @Bind(R.id.play)
        ImageView play;
        @Bind(R.id.unlock)
        View unlock;
        @Bind(R.id.next)
        View next;
        @Bind(R.id.prev)
        View prev;
        ViewPager pager;

        public VideoPageAdapter(ViewPager pager) {
            this.pager = pager;
        }

        @Override
        public Object instantiateItem(final ViewGroup container, int position) {
            final com.muevaelvolante.barapp.model.home.Video video= videos.get(position);
            View view= ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.news_item_video_pager_item,container,false);
            ButterKnife.bind(this,view);
            int screenWidth = Utils.getScreenWidth((AppCompatActivity)context);
            int finalHeight = (int) (screenWidth *0.5);
            image.setMinimumHeight(finalHeight / 1);
            image.setMaxHeight(finalHeight / 1);
            image.setAdjustViewBounds(false);
            //set height as layoutParameter too
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) image.getLayoutParams();
            lp.height = finalHeight / 1;
            image.setLayoutParams(lp);
         //   buttons.setLayoutParams(lp);
            tittle.setText(video.getTitle());
            tittle.setVisibility(View.VISIBLE);
            final com.google.gson.internal.LinkedTreeMap<String,String> map= (com.google.gson.internal.LinkedTreeMap) video.getMedia().get(0);
            //String url= map.get("med");
            ImageLoader.getInstance().displayImage(Uri.parse(map.get("med")).toString(), image);
            date.setText(Utils.getDateString(Utils.convertStringToDate(video.getDate())));
            if(!TextUtils.isEmpty(video.getLocked()) && Aplication.isAnonimous){
                unlock.setVisibility(View.VISIBLE);
                play.setVisibility(View.INVISIBLE);
            }else
                play.setVisibility(View.VISIBLE);
            if(video.getType().toLowerCase().equals("video360")){
                play.setImageResource(R.drawable.video360);
                play.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent video360= new Intent(context, Google360.class);
                        video360.putExtra("url",map.get("video360"));
                        context.startActivity(video360);
                        //Utils.playVideoYoutube(context, media.getVideo());
                    }
                });
            }else
                play.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                    Medium__ media = video.getMedia().get(0);
                        if(map.get("video").contains("youtube"))
                        Utils.playVideoYoutube(context, map.get("video"));
                        else{
                            context.startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse(map.get("video"))));
                        }
                    }
                });
            if(position< videos.size()-1){
            next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(pager.getCurrentItem()+1<=videos.size()-1)
                    pager.setCurrentItem(pager.getCurrentItem()+1);
                }
            });}else
            next.setVisibility(View.GONE);
            if(position >0 ){
                prev.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(pager.getCurrentItem()-1 >= 0)
                            pager.setCurrentItem(pager.getCurrentItem()-1);
                    }
                });
            }else
                prev.setVisibility(View.GONE);
            container.addView(view);
            return view;
        }

        @Override
        public int getCount() {
            return videos.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

        @Override
        public void destroyItem(View container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }


    }

    public void startAutoSlide(int position){
       new Handler().postDelayed(nextPageRunnable,5000);
    }

    private void nextPage() {
        if(highLightViewPager.getCurrentItem()+1 <= homeHighlights.size()-1)
            highLightViewPager.setCurrentItem(highLightViewPager.getCurrentItem()+1);
        else
            highLightViewPager.setCurrentItem(0);
        new Handler().postDelayed(nextPageRunnable,5000);
    }

    Runnable nextPageRunnable= new Runnable() {
        @Override
        public void run() {
            nextPage();
        }
    };

    class CountDownViewHolder extends RecyclerView.ViewHolder{
        @Bind(R.id.pager)
        ViewPager pager;
        @Bind(R.id.indicator)
        CirclePageIndicator indicator;

        public CountDownViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            pager=(ViewPager)itemView.findViewById(R.id.pager);
            pager.setOffscreenPageLimit(50);
            highLightViewPager=pager;
        }
    }

    class CountDownAdapter extends PagerAdapter{

        private static final String DATE = "param1";
        private static final String TEXT = "param2";
        private static final String BUTTON = "param3";
        private static final String URL = "param4";

        // TODO: Rename and change types of parameters
        private String dateC;
        private String text;
        private String button;
        private String url;

        /*
        * Atributos de la vista countdonw
        * */
        @Bind(R.id.countDonw)
        View countDonw;
        @Bind(R.id.tittle)
        TextView title;
        @Bind(R.id.button)
        Button buttonAction;
        @Bind(R.id.days1)
        TimelyView days;
        @Bind(R.id.days2)
        TimelyView days2;
        @Bind(R.id.hours)
        TimelyView hours;
        @Bind(R.id.hours2)
        TimelyView hours2;
        @Bind(R.id.minutes)
        TimelyView minutes;
        @Bind(R.id.minutes1)
        TimelyView minutes2;
        @Bind(R.id.seconds)
        TimelyView seconds;
        @Bind(R.id.seconds2)
        TimelyView seconds2;
        @Bind(R.id.imageView21)
        ImageView imageView;
        int [] number;
        private ImageView icon_live;
        private boolean isPagerResized=false;

        public CountDownAdapter(ViewPager pager) {
            this.pager = pager;
        }

        ViewPager pager;

        Runnable updateTick= new Runnable() {
            @Override
            public void run() {
                long time= TimberUtils.isCountDonwOver(context,timeFinal);
                if(time>=0)
                    fillTime(TimberUtils.makeShortTimeString(context, timeFinal));
                else {
                    stopCD=true;
                    countDonw.setVisibility(View.GONE);
                    icon_live.setVisibility(View.VISIBLE);
                    buttonAction.setText("LIVE NOW");
                    buttonAction.setVisibility(View.VISIBLE);
                    buttonAction.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            context.startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse("https://play.google.com/store/apps/details?id=com.americascup.acplus")));

                        }
                    });
                }

            }
        };
        Handler handler= new Handler();
        private DateTime timeFinal;
        private boolean stopCD=false;

        @Override
        public Object instantiateItem(final ViewGroup container, int position) {
            final HomeHighlight highlight=(HomeHighlight)homeHighlights.get(position);
            if(highlight.getType().toLowerCase().equals("countdown")){
                View view=((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.fragment_count_down, container, false);
                countDonw= view.findViewById(R.id.countDonw);
                title=(TextView)view.findViewById(R.id.tittle);
                buttonAction=(Button)view.findViewById(R.id.button);
                days=(TimelyView) view.findViewById(R.id.days1);
                days2=(TimelyView) view.findViewById(R.id.days2);
                hours=(TimelyView) view.findViewById(R.id.hours);
                hours2=(TimelyView) view.findViewById(R.id.hours2);
                minutes=(TimelyView) view.findViewById(R.id.minutes);
                minutes2=(TimelyView) view.findViewById(R.id.minutes1);
                seconds=(TimelyView) view.findViewById(R.id.seconds);
                seconds2=(TimelyView) view.findViewById(R.id.seconds2);
                imageView=(ImageView)view.findViewById(R.id.imageView21);
                icon_live=(ImageView)view.findViewById(R.id.icon_live);
                View next= view.findViewById(R.id.next);
                View prev= view.findViewById(R.id.prev);
                if(position< homeHighlights.size()-1){
                    next.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(pager.getCurrentItem()+1<=galleries.size()-1)
                                pager.setCurrentItem(pager.getCurrentItem()+1);
                        }
                    });
                }else
                    next.setVisibility(View.GONE);
                if(position >0 ){
                    prev.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(pager.getCurrentItem()-1 >= 0)
                                pager.setCurrentItem(pager.getCurrentItem()-1);
                        }
                    });
                }else
                    prev.setVisibility(View.GONE);
                int screenWidth = Utils.getScreenWidth((AppCompatActivity)context);
                int finalHeight = (int) (screenWidth * 0.8);
                imageView.setMinimumHeight(finalHeight / 1);
                imageView.setMaxHeight(finalHeight / 1);
                imageView.setAdjustViewBounds(false);
                //set height as layoutParameter too
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) imageView.getLayoutParams();
                lp.height = finalHeight ;
                container.setLayoutParams(lp);
                com.google.gson.internal.LinkedTreeMap<String,String> map= (LinkedTreeMap<String, String>) highlight.getMedia().get(0);
                ImageLoader.getInstance().displayImage(map.get("med"), imageView, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String s, View view) {

                    }

                    @Override
                    public void onLoadingFailed(String s, View view, FailReason failReason) {

                    }

                    @Override
                    public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                        float proportion= (float)bitmap.getWidth()/bitmap.getHeight();
                        int alto= (int) (((Utils.getScreenWidth((Activity) context)))/proportion);
                        RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams) view.getLayoutParams();
                        lp.height=alto;
                        lp.width=Utils.getScreenWidth((Activity) context);
                        view.setLayoutParams(lp);
                        if(!isPagerResized){
                            pager.setLayoutParams(lp);
                            isPagerResized=true;
                        }
                    }

                    @Override
                    public void onLoadingCancelled(String s, View view) {

                    }
                });
                dateC = highlight.getDate();
                text = highlight.getTitle();
                button = highlight.getLabel();
                url = highlight.getUrl();
                long countElapsed= TimberUtils.isCountDonwOver(context,DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").parseDateTime(dateC));
                if(countElapsed > 0){//es countDonw
                    icon_live.setVisibility(View.GONE);
                    countDonw.setVisibility(View.VISIBLE);
                    buttonAction.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.button_count_dow));
                    buttonAction.setText(button);
                    title.setText(text);
                }else{
                    stopCD=true;
                    title.setText(text);
                    countDonw.setVisibility(View.GONE);
                    icon_live.setVisibility(View.VISIBLE);
                    buttonAction.setText("LIVE NOW");
                    buttonAction.setVisibility(View.VISIBLE);
                    buttonAction.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            context.startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse("https://play.google.com/store/apps/details?id=com.americascup.acplus")));

                        }
                    });
                    }
                if(button.toString().toLowerCase().equals("see results")){//es countDonw
                    stopCD=true;
                    countDonw.setVisibility(View.GONE);
                    buttonAction.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.button_count_dow));
                }
                number= new int[]{-1,-1,-1,-1,-1,-1,-1,-1};
                timeFinal= DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").parseDateTime(dateC);
                timeFinal=timeFinal.withZone(DateTimeZone.UTC);
                timeFinal=timeFinal.toLocalDateTime().toDateTime();
                String time = TimberUtils.makeShortTimeString(context, timeFinal);
                Log.d("time", time);
                if(!stopCD)
                    fillTime(time);
                container.addView(view);
                return view;
            }
            if(highlight.getType().toLowerCase().equals("goto")){
                View view=((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.highlight_item, container, false);
                ImageView play= (ImageView) view.findViewById(R.id.play);
                ImageView image=(ImageView)view.findViewById(R.id.image);
                TextView tittle=(TextView)view.findViewById(R.id.title);
                TextView section=(TextView)view.findViewById(R.id.section);
                View next= view.findViewById(R.id.next);
                View prev= view.findViewById(R.id.prev  );
                if(position< homeHighlights.size()-1){
                    next.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(pager.getCurrentItem()+1<=galleries.size()-1)
                                pager.setCurrentItem(pager.getCurrentItem()+1);
                        }
                    });
                }else
                    next.setVisibility(View.GONE);
                if(position >0 ){
                    prev.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(pager.getCurrentItem()-1 >= 0)
                                pager.setCurrentItem(pager.getCurrentItem()-1);
                        }
                    });
                }else
                    prev.setVisibility(View.GONE);
                View button=view.findViewById(R.id.buttons);
                int screenWidth = Utils.getScreenWidth((AppCompatActivity)context);
                int finalHeight = (int) (screenWidth *0.8);
                image.setMinimumHeight(finalHeight / 1);
                image.setMaxHeight(finalHeight / 1);
                image.setAdjustViewBounds(false);
                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String action= highlight.getAction();
                        if(action.toLowerCase().equals("go-team")){
                            ((HomeActivity)context).gotoSection("teams");
                            return;
                        }
                        if(action.toLowerCase().equals("go-results")){
                            ((HomeActivity)context).gotoSection("results");
                            return;
                        }
                        if(action.toLowerCase().equals("go-boat")){
                            ((HomeActivity)context).gotoSection("boat");
                            return;
                        }
                        if(action.toLowerCase().equals("go-actheguide")){
                            ((HomeActivity)context).gotoSection("actheguide");
                            return;
                        }
                        if(action.toLowerCase().equals("go-community")){
                            ((HomeActivity)context).gotoSection("community");
                            return;
                        }
                        if(action.toLowerCase().equals("go-raisethebar") || action.toLowerCase().equals("go-sustainability")){
                            ((HomeActivity)context).gotoSection("raisethebar");
                            return;
                        }
                        if(action.toLowerCase().equals("go-team")){

                            return;
                        }
                    }
                });
                //set height as layoutParameter too
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) image.getLayoutParams();
                lp.height = finalHeight / 1;
                image.setLayoutParams(lp);
                // imageContainer.setLayoutParams(lp);
                button.setLayoutParams(lp);
                if(!TextUtils.isEmpty(highlight.getLabel()))
                section.setText(highlight.getLabel());
                else
                section.setVisibility(View.GONE);
                tittle.setText(highlight.getTitle());
                final LinkedTreeMap<String,String> map= (LinkedTreeMap<String, String>) highlight.getMedia().get(0);
                ImageLoader.getInstance().displayImage(map.get("med"), image, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String s, View view) {

                    }

                    @Override
                    public void onLoadingFailed(String s, View view, FailReason failReason) {

                    }

                    @Override
                    public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                        float proportion= (float)bitmap.getWidth()/bitmap.getHeight();
                        int alto= (int) (((Utils.getScreenWidth((Activity) context)))/proportion);
                        RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams) view.getLayoutParams();
                        lp.height=alto;
                        lp.width=Utils.getScreenWidth((Activity) context);
                        view.setLayoutParams(lp);
                        if(!isPagerResized){
                            pager.setLayoutParams(lp);
                            isPagerResized=true;
                        }
                    }

                    @Override
                    public void onLoadingCancelled(String s, View view) {

                    }
                });
                //date.setText(Utils.getDateString(Utils.convertStringToDate(highlight.getDate())));
                play.setVisibility(View.GONE);
                String tittle_position=highlight.getTitlePosition();
                RelativeLayout.LayoutParams tlp= (RelativeLayout.LayoutParams) tittle.getLayoutParams();
                if(tittle_position.toLowerCase().equals("center center")){
                    tlp.addRule(RelativeLayout.CENTER_IN_PARENT);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("center left") || tittle_position.toLowerCase().equals("left center")){
                    tlp.addRule(RelativeLayout.CENTER_VERTICAL);
                    tlp.addRule(RelativeLayout.ALIGN_LEFT);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("center right") || tittle_position.toLowerCase().equals("right center")){
                    tlp.addRule(RelativeLayout.CENTER_VERTICAL);
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("top center")||tittle_position.toLowerCase().equals("center top")){
                    tlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("top left")||tittle_position.toLowerCase().equals("left top")){
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("top right")||tittle_position.toLowerCase().equals("right top")){
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("bottom center") || tittle_position.toLowerCase().equals("center bottom")){
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    tlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("bottom left")||tittle_position.toLowerCase().equals("left bottom")){
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("bottom right")||tittle_position.toLowerCase().equals("right bottom")){
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    tittle.setLayoutParams(tlp);
                }
                container.addView(view);
                return view;
            }
            if(highlight.getType().toLowerCase().equals("video")){
                View view=((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.highlight_item, container, false);
                ImageView play= (ImageView) view.findViewById(R.id.play);
                ImageView image=(ImageView)view.findViewById(R.id.image);
                TextView tittle=(TextView)view.findViewById(R.id.title);
                TextView section=(TextView)view.findViewById(R.id.section);
                View next= view.findViewById(R.id.next);
                View prev= view.findViewById(R.id.prev  );
                if(position< homeHighlights.size()-1){
                    next.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(pager.getCurrentItem()+1<=galleries.size()-1)
                                pager.setCurrentItem(pager.getCurrentItem()+1);
                        }
                    });
                }else
                    next.setVisibility(View.GONE);
                if(position >0 ){
                    prev.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(pager.getCurrentItem()-1 >= 0)
                                pager.setCurrentItem(pager.getCurrentItem()-1);
                        }
                    });
                }else
                    prev.setVisibility(View.GONE);
                View button=view.findViewById(R.id.buttons);
                int screenWidth = Utils.getScreenWidth((AppCompatActivity)context);
                int finalHeight = (int) (screenWidth * 0.8);
                image.setMinimumHeight(finalHeight / 1);
                image.setMaxHeight(finalHeight / 1);
                image.setAdjustViewBounds(false);
                //set height as layoutParameter too
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) image.getLayoutParams();
                lp.height = finalHeight / 1;
                image.setLayoutParams(lp);
                button.setLayoutParams(lp);
                section.setText(highlight.getLabel());
                tittle.setText(highlight.getTitle());
                final LinkedTreeMap<String,String> map= (LinkedTreeMap<String, String>) highlight.getMedia().get(0);
                ImageLoader.getInstance().displayImage(map.get("med"),image, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String s, View view) {

                    }

                    @Override
                    public void onLoadingFailed(String s, View view, FailReason failReason) {

                    }

                    @Override
                    public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                        float proportion= (float)bitmap.getWidth()/bitmap.getHeight();
                        int alto= (int) (((Utils.getScreenWidth((Activity) context)))/proportion);
                        RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams) view.getLayoutParams();
                        lp.height=alto;
                        lp.width=Utils.getScreenWidth((Activity) context);
                        view.setLayoutParams(lp);
                        if(!isPagerResized){
                            pager.setLayoutParams(lp);
                            isPagerResized=true;
                        }
                    }

                    @Override
                    public void onLoadingCancelled(String s, View view) {

                    }
                });
                play.setImageResource(R.drawable.video);
                final LinkedTreeMap<String,String> mapvideo= (LinkedTreeMap<String, String>) highlight.getMedia().get(0);
                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Utils.playVideoYoutube(context,mapvideo.get("video"));
                    }
                });
                String tittle_position=highlight.getTitlePosition();
                RelativeLayout.LayoutParams tlp= (RelativeLayout.LayoutParams) tittle.getLayoutParams();
                if(tittle_position.toLowerCase().equals("center center")){
                    tlp.addRule(RelativeLayout.CENTER_IN_PARENT);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("center left") || tittle_position.toLowerCase().equals("left center")){
                    tlp.addRule(RelativeLayout.CENTER_VERTICAL);
                    tlp.addRule(RelativeLayout.ALIGN_LEFT);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("center right") || tittle_position.toLowerCase().equals("right center")){
                    tlp.addRule(RelativeLayout.CENTER_VERTICAL);
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("top center")||tittle_position.toLowerCase().equals("center top")){
                    tlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("top left")||tittle_position.toLowerCase().equals("left top")){
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("top right")||tittle_position.toLowerCase().equals("right top")){
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("bottom center") || tittle_position.toLowerCase().equals("center bottom")){
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    tlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("bottom left")||tittle_position.toLowerCase().equals("left bottom")){
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("bottom right")||tittle_position.toLowerCase().equals("right bottom")){
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    tittle.setLayoutParams(tlp);
                }
                container.addView(view);
                return view;
            }
            if(highlight.getType().toLowerCase().equals("video360")){
                View view=((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.highlight_item, container, false);
                ImageView play= (ImageView) view.findViewById(R.id.play);
                ImageView image=(ImageView)view.findViewById(R.id.image);
                TextView tittle=(TextView)view.findViewById(R.id.title);
                TextView section=(TextView)view.findViewById(R.id.section);
                View next= view.findViewById(R.id.next);
                View prev= view.findViewById(R.id.prev  );
                if(position< homeHighlights.size()-1){
                    next.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(pager.getCurrentItem()+1<=galleries.size()-1)
                                pager.setCurrentItem(pager.getCurrentItem()+1);
                        }
                    });
                }else
                    next.setVisibility(View.GONE);
                if(position >0 ){
                    prev.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(pager.getCurrentItem()-1 >= 0)
                                pager.setCurrentItem(pager.getCurrentItem()-1);
                        }
                    });
                }else
                    prev.setVisibility(View.GONE);
                View button=view.findViewById(R.id.buttons);
                int screenWidth = Utils.getScreenWidth((AppCompatActivity)context);
                int finalHeight = (int) (screenWidth * 0.8);
                image.setMinimumHeight(finalHeight / 1);
                image.setMaxHeight(finalHeight / 1);
                image.setAdjustViewBounds(false);
                //set height as layoutParameter too
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) image.getLayoutParams();
                lp.height = finalHeight / 1;
                image.setLayoutParams(lp);
                button.setLayoutParams(lp);
                section.setText(highlight.getLabel());
                tittle.setText(highlight.getTitle());
                final LinkedTreeMap<String,String> map= (LinkedTreeMap<String, String>) highlight.getMedia().get(0);
                ImageLoader.getInstance().displayImage(map.get("med"), image, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String s, View view) {

                    }

                    @Override
                    public void onLoadingFailed(String s, View view, FailReason failReason) {

                    }

                    @Override
                    public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                        float proportion= (float)bitmap.getWidth()/bitmap.getHeight();
                        int alto= (int) (((Utils.getScreenWidth((Activity) context)))/proportion);
                        RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams) view.getLayoutParams();
                        lp.height=alto;
                        lp.width=Utils.getScreenWidth((Activity) context);
                        view.setLayoutParams(lp);
                        if(!isPagerResized){
                            pager.setLayoutParams(lp);
                            isPagerResized=true;
                        }
                    }

                    @Override
                    public void onLoadingCancelled(String s, View view) {

                    }
                });
                play.setImageResource(R.drawable.video360);
                final LinkedTreeMap<String,String> mapvideo= (LinkedTreeMap<String, String>) highlight.getMedia().get(0);
                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent video360= new Intent(context,Google360.class);
                        video360.putExtra("url",mapvideo.get("video360"));
                        context.startActivity(video360);
                    }
                });
                String tittle_position=highlight.getTitlePosition();
                RelativeLayout.LayoutParams tlp= (RelativeLayout.LayoutParams) tittle.getLayoutParams();
                if(tittle_position.toLowerCase().equals("center center")){
                    tlp.addRule(RelativeLayout.CENTER_IN_PARENT);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("center left") || tittle_position.toLowerCase().equals("left center")){
                    tlp.addRule(RelativeLayout.CENTER_VERTICAL);
                    tlp.addRule(RelativeLayout.ALIGN_LEFT);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("center right") || tittle_position.toLowerCase().equals("right center")){
                    tlp.addRule(RelativeLayout.CENTER_VERTICAL);
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("top center")||tittle_position.toLowerCase().equals("center top")){
                    tlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("top left")||tittle_position.toLowerCase().equals("left top")){
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("top right")||tittle_position.toLowerCase().equals("right top")){
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("bottom center") || tittle_position.toLowerCase().equals("center bottom")){
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    tlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("bottom left")||tittle_position.toLowerCase().equals("left bottom")){
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("bottom right")||tittle_position.toLowerCase().equals("right bottom")){
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    tittle.setLayoutParams(tlp);
                }
                container.addView(view);
                return view;
            }
            if(highlight.getType().toLowerCase().equals("article")){
                View view=((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.highlight_item, container, false);
                ImageView image=(ImageView)view.findViewById(R.id.image);
                TextView tittle=(TextView)view.findViewById(R.id.title);
                TextView section=(TextView)view.findViewById(R.id.section);
                View next= view.findViewById(R.id.next);
                View prev= view.findViewById(R.id.prev  );
                if(position< homeHighlights.size()-1){
                    next.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(pager.getCurrentItem()+1<=galleries.size()-1)
                                pager.setCurrentItem(pager.getCurrentItem()+1);
                        }
                    });
                }else
                    next.setVisibility(View.GONE);
                if(position >0 ){
                    prev.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(pager.getCurrentItem()-1 >= 0)
                                pager.setCurrentItem(pager.getCurrentItem()-1);
                        }
                    });
                }else
                    prev.setVisibility(View.GONE);
                int screenWidth = Utils.getScreenWidth((AppCompatActivity)context);
                int finalHeight = (int) (screenWidth * 0.8);
                image.setMinimumHeight(finalHeight / 1);
                image.setMaxHeight(finalHeight / 1);
                image.setAdjustViewBounds(false);
                //set height as layoutParameter too
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) image.getLayoutParams();
                lp.height = finalHeight / 1;
                image.setLayoutParams(lp);
                section.setText(highlight.getLabel());
                tittle.setText(highlight.getTitle());
                final LinkedTreeMap<String,String> map= (LinkedTreeMap<String, String>) highlight.getMedia().get(0);
                ImageLoader.getInstance().displayImage(map.get("med"),image, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String s, View view) {

                    }

                    @Override
                    public void onLoadingFailed(String s, View view, FailReason failReason) {

                    }

                    @Override
                    public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                        float proportion= (float)bitmap.getWidth()/bitmap.getHeight();
                        int alto= (int) (((Utils.getScreenWidth((Activity) context)))/proportion);
                        RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams) view.getLayoutParams();
                        lp.height=alto;
                        lp.width=Utils.getScreenWidth((Activity) context);
                        view.setLayoutParams(lp);
                        if(!isPagerResized){
                            pager.setLayoutParams(lp);
                            isPagerResized=true;
                        }
                    }

                    @Override
                    public void onLoadingCancelled(String s, View view) {

                    }
                });
                final LinkedTreeMap<String,String> mapvideo= (LinkedTreeMap<String, String>) highlight.getMedia().get(0);
                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //aqui tenemos que convertir el highlight a homenews
                        HomeNews homeNews= new HomeNews();
                        homeNews.setDate(highlight.getDate());
                        homeNews.setText(highlight.getText());
                        homeNews.setMedia(highlight.getMedia());
                        homeNews.setTitle(highlight.getTitle());
                        Intent intent= new Intent(context, NewsDetailActivity.class);
                        Bundle bundle= new Bundle();
                        bundle.putSerializable("news",homeNews);
                        intent.putExtras(bundle);
                        context.startActivity(intent);

                    }
                });
                String tittle_position=highlight.getTitlePosition();
                RelativeLayout.LayoutParams tlp= (RelativeLayout.LayoutParams) tittle.getLayoutParams();
                if(tittle_position.toLowerCase().equals("center center")){
                    tlp.addRule(RelativeLayout.CENTER_IN_PARENT);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("center left") || tittle_position.toLowerCase().equals("left center")){
                    tlp.addRule(RelativeLayout.CENTER_VERTICAL);
                    tlp.addRule(RelativeLayout.ALIGN_LEFT);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("center right") || tittle_position.toLowerCase().equals("right center")){
                    tlp.addRule(RelativeLayout.CENTER_VERTICAL);
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("top center")||tittle_position.toLowerCase().equals("center top")){
                    tlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("top left")||tittle_position.toLowerCase().equals("left top")){
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("top right")||tittle_position.toLowerCase().equals("right top")){
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("bottom center") || tittle_position.toLowerCase().equals("center bottom")){
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    tlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("bottom left")||tittle_position.toLowerCase().equals("left bottom")){
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("bottom right")||tittle_position.toLowerCase().equals("right bottom")){
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    tittle.setLayoutParams(tlp);
                }
                container.addView(view);
                return view;
            }
            if(highlight.getType().toLowerCase().equals("weburl")){
                View view=((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.highlight_item, container, false);
                ImageView play= (ImageView) view.findViewById(R.id.play);
                ImageView image=(ImageView)view.findViewById(R.id.image);
                TextView tittle=(TextView)view.findViewById(R.id.title);
                TextView section=(TextView)view.findViewById(R.id.section);
                View next= view.findViewById(R.id.next);
                View prev= view.findViewById(R.id.prev  );
                if(position< homeHighlights.size()-1){
                    next.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(pager.getCurrentItem()+1<=galleries.size()-1)
                                pager.setCurrentItem(pager.getCurrentItem()+1);
                        }
                    });
                }else
                    next.setVisibility(View.GONE);
                if(position >0 ){
                    prev.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(pager.getCurrentItem()-1 >= 0)
                                pager.setCurrentItem(pager.getCurrentItem()-1);
                        }
                    });
                }else
                    prev.setVisibility(View.GONE);
                View button=view.findViewById(R.id.buttons);
                int screenWidth = Utils.getScreenWidth((AppCompatActivity)context);
                int finalHeight = (int) (screenWidth *0.8);
                image.setMinimumHeight(finalHeight / 1);
                image.setMaxHeight(finalHeight / 1);
                image.setAdjustViewBounds(false);
                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        context.startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse(highlight.getUrl())));
                    }
                });
                //set height as layoutParameter too
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) image.getLayoutParams();
                lp.height = finalHeight / 1;
                image.setLayoutParams(lp);
                // imageContainer.setLayoutParams(lp);
                button.setLayoutParams(lp);
                if(!TextUtils.isEmpty(highlight.getLabel()))
                    section.setText(highlight.getLabel());
                else
                    section.setVisibility(View.GONE);
                tittle.setText(highlight.getTitle());
                final LinkedTreeMap<String,String> map= (LinkedTreeMap<String, String>) highlight.getMedia().get(0);
                ImageLoader.getInstance().displayImage(map.get("med"), image, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String s, View view) {

                    }

                    @Override
                    public void onLoadingFailed(String s, View view, FailReason failReason) {

                    }

                    @Override
                    public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                        float proportion= (float)bitmap.getWidth()/bitmap.getHeight();
                        int alto= (int) (((Utils.getScreenWidth((Activity) context)))/proportion);
                        RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams) view.getLayoutParams();
                        lp.height=alto;
                        lp.width=Utils.getScreenWidth((Activity) context);
                        view.setLayoutParams(lp);
                        if(!isPagerResized){
                            pager.setLayoutParams(lp);
                            isPagerResized=true;
                        }
                    }

                    @Override
                    public void onLoadingCancelled(String s, View view) {

                    }
                });
                //date.setText(Utils.getDateString(Utils.convertStringToDate(highlight.getDate())));
                play.setVisibility(View.GONE);
                String tittle_position=highlight.getTitlePosition();
                RelativeLayout.LayoutParams tlp= (RelativeLayout.LayoutParams) tittle.getLayoutParams();
                if(tittle_position.toLowerCase().equals("center center")){
                    tlp.addRule(RelativeLayout.CENTER_IN_PARENT);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("center left") || tittle_position.toLowerCase().equals("left center")){
                    tlp.addRule(RelativeLayout.CENTER_VERTICAL);
                    tlp.addRule(RelativeLayout.ALIGN_LEFT);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("center right") || tittle_position.toLowerCase().equals("right center")){
                    tlp.addRule(RelativeLayout.CENTER_VERTICAL);
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("top center")||tittle_position.toLowerCase().equals("center top")){
                    tlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("top left")||tittle_position.toLowerCase().equals("left top")){
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("top right")||tittle_position.toLowerCase().equals("right top")){
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("bottom center") || tittle_position.toLowerCase().equals("center bottom")){
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    tlp.addRule(RelativeLayout.CENTER_HORIZONTAL);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("bottom left")||tittle_position.toLowerCase().equals("left bottom")){
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                    tittle.setLayoutParams(tlp);
                }
                if(tittle_position.toLowerCase().equals("bottom right")||tittle_position.toLowerCase().equals("right bottom")){
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    tlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    tittle.setLayoutParams(tlp);
                }
                container.addView(view);
                return view;
            }
            return null;
        }

        @Override
        public int getCount() {
            return homeHighlights.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

        @Override
        public void destroyItem(View container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }

        public void fillTime(String time){
//            Log.d("ENtre","Entre");
            if (!(time.charAt(0)+"").equals("-") && number[0] != Integer.parseInt(time.charAt(0) + "")) {
                days.animate(number[0],time.charAt(0) - '0').setDuration(500).start();
                number[0] = Integer.parseInt(time.charAt(0) + "");
            }
            if (!(time.charAt(1)+"").equals("-") && number[1] != Integer.parseInt(time.charAt(1) + "")) {
                days2.animate(number[1],time.charAt(1) - '0').setDuration(500).start();
                number[1] = Integer.parseInt(time.charAt(1) + "");
            }
            if (!(time.charAt(2)+"").equals("-") && number[2] != Integer.parseInt(time.charAt(3) + "")) {
                hours.animate(number[2],time.charAt(3) - '0').setDuration(500).start();
                number[2] = Integer.parseInt(time.charAt(3) + "");
            }
            if (!(time.charAt(3)+"").equals("-") && number[3] != Integer.parseInt(time.charAt(4) + "")) {
                hours2.animate(number[3],time.charAt(4) - '0').setDuration(500).start();
                number[3] = Integer.parseInt(time.charAt(4) + "");
            }
            if (!(time.charAt(4)+"").equals("-") && number[4] != Integer.parseInt(time.charAt(6) + "")) {
                minutes.animate(number[4],time.charAt(6) - '0').setDuration(500).start();
                number[4] = Integer.parseInt(time.charAt(6) + "");
            }
            if (!(time.charAt(5)+"").equals("-") && number[5] != Integer.parseInt(time.charAt(7) + "")) {
                minutes2.animate(number[5],time.charAt(7) - '0').setDuration(500).start();
                number[5] = Integer.parseInt(time.charAt(7) + "");
            }
            if (!(time.charAt(6)+"").equals("-") && number[6] != Integer.parseInt(time.charAt(9) + "")) {
                seconds.animate(number[6],time.charAt(9) - '0').setDuration(500).start();
                number[6] = Integer.parseInt(time.charAt(9) + "");
            }
            if (!(time.charAt(7)+"").equals("-") && number[7] != Integer.parseInt(time.charAt(10) + "")) {
                seconds2.animate(number[7],time.charAt(10) - '0').setDuration(500).start();
                number[7] = Integer.parseInt(time.charAt(10) + "");
            }
            handler.postDelayed(updateTick, 1000);
        }


    }

    public   interface HomeAdapterGoto{
        void gotoSection(String section);
    }

}
