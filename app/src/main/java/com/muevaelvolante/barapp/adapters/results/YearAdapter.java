package com.muevaelvolante.barapp.adapters.results;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.muevaelvolante.Aplication;
import com.muevaelvolante.barapp.R;
import com.muevaelvolante.barapp.fragment.ResultFragment;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by CQ on 14/04/2016.
 */
public class YearAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    AppCompatActivity context;
    List<String> years;
    int previousSelected=-1;

    public void setPreviousSelected(int previousSelected) {
        this.previousSelected = previousSelected;
    }

    public ViewHolder getLastHolder() {
        return lastHolder;
    }

    ViewHolder lastHolder;
    ResultFragment fragment;

    public YearAdapter(AppCompatActivity context, List<String> years,Fragment fragment) {
        this.context = context;
        this.years = years;
        this.fragment= (ResultFragment) fragment;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(context.getLayoutInflater().inflate(R.layout.year_item,null));
    }

    public SpannableStringBuilder setBold(String text){
        final SpannableStringBuilder sb = new SpannableStringBuilder(text);

// Span to make text bold
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);

// make them also bold
        sb.setSpan(bss, 0,text.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        return sb;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder holder1=(ViewHolder)holder;
        holder1.year.setText(years.get(position));
        ((ViewHolder) holder).year.setTypeface(Aplication.robotoL);
            ((ViewHolder) holder).selected.setVisibility(View.INVISIBLE);
    }

    @Override
    public int getItemCount() {
        return years.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        @Bind(R.id.year)
        TextView year;
        @Bind(R.id.selected)
        View selected;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // if(getAdapterPosition()!=previousSelected){
                    if (getAdapterPosition() != previousSelected) {
                        if (lastHolder != null) {
                            lastHolder.selected.setVisibility(View.INVISIBLE);
                            lastHolder.year.setTypeface(Aplication.robotoL);
                            lastHolder.year.setText(lastHolder.year.getText());
                            lastHolder = ViewHolder.this;
                        } else
                            lastHolder = ViewHolder.this;
                    }
                    selected.setVisibility(View.VISIBLE);
                    ViewHolder.this.year.setTypeface(Aplication.robotoM);
                    ViewHolder.this.year.setText(setBold(years.get(getAdapterPosition())));
                    previousSelected = getAdapterPosition();
                    fragment.loadDataForYear(Integer.parseInt(years.get(getAdapterPosition())));
                    //}
                }
            });
        }
    }

    public void unSelectYear(){
        if(lastHolder!= null){
        lastHolder.selected.setVisibility(View.INVISIBLE);
        lastHolder.year.setTypeface(Aplication.robotoL);}
    }
}
