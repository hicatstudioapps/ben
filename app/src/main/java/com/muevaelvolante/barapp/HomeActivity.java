package com.muevaelvolante.barapp;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.lacostra.utils.notification.Permission.Permission;
import com.muevaelvolante.Aplication;
import com.muevaelvolante.barapp.activity.login.LoginActivity;
import com.muevaelvolante.barapp.activity.login.ProfileFragment;
import com.muevaelvolante.barapp.adapters.NewsAdapter;
import com.muevaelvolante.barapp.cache.JsonCacheReader;
import com.muevaelvolante.barapp.cache.JsonCacheWriter;
import com.muevaelvolante.barapp.commons.Constants;
import com.muevaelvolante.barapp.custom.CustomTextureVideoView;
import com.muevaelvolante.barapp.fragment.ACTheGuideFragment;
import com.muevaelvolante.barapp.fragment.BoatFragment;
import com.muevaelvolante.barapp.fragment.ComunityFragment;
import com.muevaelvolante.barapp.fragment.GalleryListFragment;
import com.muevaelvolante.barapp.fragment.ResultFragment;
import com.muevaelvolante.barapp.fragment.SustainabilityFragment;
import com.muevaelvolante.barapp.fragment.TeamFragment;
import com.muevaelvolante.barapp.fragment.VidioListFragment;
import com.muevaelvolante.barapp.interfaces.ISettings;
import com.muevaelvolante.barapp.model.home.Home;
import com.muevaelvolante.barapp.model.setting.Setting;
import com.muevaelvolante.barapp.services.JsonUpdateService;
import com.muevaelvolante.barapp.services.ServiceGenerator;
import com.muevaelvolante.barapp.util.ExceptionHandler;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.twitter.sdk.android.Twitter;

import java.lang.ref.WeakReference;
import java.util.LinkedList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


@SuppressWarnings("WrongConstant")
public class HomeActivity extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener,
        NewsAdapter.HomeAdapterGoto {

    public RecyclerView getNewsrecycler() {
        return newsrecycler;
    }

    @Bind(R.id.newsRV)
    RecyclerView newsrecycler;
    @Bind(R.id.refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    int steps = 0;
    Toolbar toolbar;
    @Bind(R.id.more_menu)
    View more_menu;
    @Bind(R.id.menu_more_container)
    LinearLayout menu_more_container;
    @Bind(R.id.profile_c)
    View profile_c;
    @Bind(R.id.imageView13)
    ImageView profile;
    @Bind(R.id.name)
    TextView nickName;
    @Bind(R.id.out)
    View logOut;
    @Bind(R.id.bottom_menu)
    LinearLayout bottom_menu;
    WeakReference<CustomTextureVideoView> video;
    ISettings setting;
    NewsAdapter newsAdapter;
    com.muevaelvolante.barapp.model.setting.Menu more_menu_item;
    com.muevaelvolante.barapp.model.setting.Menu home_menu_item;
    List<View> menuViews = new LinkedList<>();
    boolean isMoreActive = false;//esto me dice si la pantalla pertenece al menu more
    boolean isMenuOpen = false;
    View menu_selected_previous;
    View menu_previous_more_selected;// para saber que vista estaba activa antes de seleccionar more
    //reciver para cambiar el visual si cambian el json
    SettingsChangeReciver settingsChangeReciver;
    RacingReciver racingReciver;
    boolean settingsChangeReciverIsRegister = false;
    DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
            .cacheInMemory(true)
            .cacheOnDisk(true)
//                .showImageOnLoading(R.drawable.empty)
//                .showImageOnFail(R.drawable.empty)
            .bitmapConfig(Bitmap.Config.RGB_565)
            .imageScaleType(ImageScaleType.EXACTLY)
            .build();
    private SharedPreferences preferencesJson;
    private View result_menu_item;
    private ProgressDialog mProgressDialog;
    private List<com.muevaelvolante.barapp.model.setting.Menu> menu_list;
    private Fragment current_fragment;

    @Override
    public void onBackPressed() {
        if (isMenuOpen) {
            closeMenu();
        } else if (current_fragment != null) {
            showHome();
        } else
            super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        swipeRefreshLayout.setOnRefreshListener(this);
        settingsChangeReciver = new SettingsChangeReciver();
        racingReciver = new RacingReciver();
        preferencesJson = getSharedPreferences(Constants.JSON_UPDATE_PREFERENCE, MODE_PRIVATE);
        preferencesJson.edit().putBoolean(Constants.JSON_UPDATE_NEEDED, false).commit();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(" ");
        setSupportActionBar(toolbar);
        newsrecycler.setLayoutManager(new LinearLayoutManager(this));

        //creamor la clase interfaz que nos da acceso a los recursos del home
        setting = ServiceGenerator.createService(ISettings.class);
        showProgressDialog();
        loadAll();

        if (!Aplication.isAnonimous) {
            SharedPreferences pf = getSharedPreferences(Constants.LOGIN_DATA, MODE_PRIVATE);
            ImageLoader.getInstance().displayImage(pf.getString(Constants.LOGIN_DATA_PICT, ""), profile);
            nickName.setText(pf.getString(Constants.LOGIN_DATA_USER, "Anonimous"));
        } else {
            profile_c.setVisibility(View.GONE);
            logOut.setVisibility(View.GONE);
        }

    }

    private void loadAll() {
        String jsonOfflineData = JsonCacheReader.getCacheJson(Aplication.getAppFilepath(HomeActivity.this) + "/settings.json");
        if (jsonOfflineData != null) {
            Setting setting = new Gson().fromJson(jsonOfflineData, Setting.class);
            configureHome(setting);
            UpdateUI();
            hideProgressDialog();
        } else {
            retrofit2.Call<Setting> call = setting.getSetings("en");
            call.enqueue(new Callback<Setting>() {
                             @Override
                             public void onResponse(retrofit2.Call<Setting> call, Response<Setting> response) {
                                 if (response.isSuccessful()) {
                                     Setting setting1 = response.body();
                                     configureHome(setting1);
                                     UpdateUI();
                                     Gson gson = new Gson();
                                      JsonCacheWriter.writeCache(Aplication.getAppFilepath(HomeActivity.this) + "/settings.json", gson.toJson(setting1));
                                     swipeRefreshLayout.setRefreshing(false);

                                 } else {

                                     Gson gson = new Gson();
                                     String jsonOfflineData = JsonCacheReader.getCacheJson(Aplication.getAppFilepath(HomeActivity.this) + "/settings.json");
                                     if (jsonOfflineData != null) {
                                         Setting setting = gson.fromJson(jsonOfflineData, Setting.class);
                                         configureHome(setting);
                                         UpdateUI();
                                     }

                                     swipeRefreshLayout.setRefreshing(false);
                                 }
                             }

                             @Override
                             public void onFailure(retrofit2.Call<Setting> call, Throwable t) {
                                 /**
                                  * en este caso cargamos los datos de la cache pues no esta
                                  * disponible el recurso online
                                  */

                                 Gson gson = new Gson();
                                 String jsonOfflineData = JsonCacheReader.getCacheJson(Aplication.getAppFilepath(HomeActivity.this) + "/settings.json");
                                 if (jsonOfflineData != null) {
                                     Setting setting = gson.fromJson(jsonOfflineData, Setting.class);
                                     configureHome(setting);
                                     UpdateUI();
                                 } else {
                                     showNoConnectionError();
                                     HomeActivity.this.finish();
                                 }

                                 swipeRefreshLayout.setRefreshing(false);
                             }
                         }
            );
        }
    }

    /*
    * Configuro todo lo referente al menu y al header(ya no es asi hubieron cambios en el json)
    * Cargo todas las cosas para mostrarlas en el listado
    * */
    private void configureHome(final Setting setting1) {
        //habilito el menu
        menu_list = setting1.getMenu();
        bottom_menu.removeAllViews();
        menu_more_container.removeAllViews();
        for (int i = 0; i < menu_list.size(); i++) {
            com.muevaelvolante.barapp.model.setting.Menu menu = menu_list.get(i);
            if (menu.getSection().toLowerCase().equals("menu")) {
                View menu_item = HomeActivity.this.getLayoutInflater().inflate(R.layout.bottom_menu_item, bottom_menu, false);
                ((TextView) menu_item.findViewById(R.id.optionTxt)).setText(menu.getText());
                if (menu.getID().toLowerCase().equals("home")) {
                    menu_item.setId(R.id.id_home);
                    Aplication.homeUrl = menu.getURL();
                    ImageLoader.getInstance().displayImage(menu.getMedia().get(1).getMed(), ((ImageView) menu_item.findViewById(R.id.optionIMG)), defaultOptions, new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String s, View view) {
                            Log.d("", "");
                        }

                        @Override
                        public void onLoadingFailed(String s, View view, FailReason failReason) {
                            Log.d("", "");
                        }

                        @Override
                        public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                            Log.d("", "");
                        }

                        @Override
                        public void onLoadingCancelled(String s, View view) {
                            Log.d("", "");
                        }
                    });
                    menu_selected_previous = menu_item;
                    home_menu_item = menu;
                } else
                    ImageLoader.getInstance().displayImage(menu.getMedia().get(0).getMed(), ((ImageView) menu_item.findViewById(R.id.optionIMG)));
                if (menu.getID().equalsIgnoreCase("results")) {
                    result_menu_item = menu_item;
                }
                menu_item.setOnClickListener(HomeActivity.this);
                menu_item.setTag(i);
                if (menu.getID().toLowerCase().equals("more")) {
                    menu_item.setId(R.id.id_more);
                    menu_item.setTag(i);
                    more_menu_item = menu;
                }
                bottom_menu.addView(menu_item);
                menuViews.add(menu_item);
            } else {
                View menu_item = HomeActivity.this.getLayoutInflater().inflate(R.layout.more_menu_item, menu_more_container, false);
                ((TextView) menu_item.findViewById(R.id.optionTxt)).setText(menu.getText());
                menu_item.setOnClickListener(HomeActivity.this);
                menu_item.setTag(i);
                menu_more_container.addView(menu_item);
                menuViews.add(menu_item);
                //si nos logeamos por las apis web del cliente
                //no mostramos la opcion de profile
//                if(Aplication.isLoginFromWeb && menu.getText().toLowerCase().equals("profile") ||
//                        Aplication.isAnonimous && menu.getText().toLowerCase().equals("profile")) {
//                    menu_item.setVisibility(View.GONE);
//                    profile.setVisibility(View.INVISIBLE);
//                    nickName.setVisibility(View.INVISIBLE);
//                }
                if (menu.getID().toLowerCase().equals("more")) {
                    menu_item.setId(R.id.id_more);
                    menu_item.setTag(i);
                    more_menu_item = menu;
                }
            }
        }
        //sino esta logeado habilitamos esa opcion en el menu more
        if (Aplication.isAnonimous) {
            View menu_item = HomeActivity.this.getLayoutInflater().inflate(R.layout.more_menu_item, menu_more_container, false);
            ((TextView) menu_item.findViewById(R.id.optionTxt)).setText("LOGIN");
            menu_item.setOnClickListener(HomeActivity.this);
            menu_item.setTag(menu_list.size());
            menu_more_container.addView(menu_item);
            menuViews.add(menu_item);
            menu_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                    finish();
                }
            });
        }
        /*
        * Cargamos los datos del nuevo home
        * */
        Gson gson = new Gson();
        String jsonOfflineData = JsonCacheReader.getCacheJson(Aplication.getAppFilepath(HomeActivity.this) + "/newhome.json");
        if (jsonOfflineData != null) {
            Home content = gson.fromJson(jsonOfflineData, Home.class);
            configureNewHome(content, setting1);
            hideProgressDialog();
        }else {
            Call<ResponseBody> homeData = setting.getHome(home_menu_item.getURL());
            homeData.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        try {
                            String dataAll = response.body().string();
                            Home home = new Gson().fromJson(dataAll, Home.class);
                            configureNewHome(home, setting1);

                                JsonCacheWriter.writeCache(Aplication.getAppFilepath(HomeActivity.this) + "/newhome.json", dataAll);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        /**
                         * en este caso cargamos los datos de la cache pues no esta
                         * disponible el recurso online
                         */
                        Gson gson = new Gson();
                        String jsonOfflineData = JsonCacheReader.getCacheJson(Aplication.getAppFilepath(HomeActivity.this) + "/newhome.json");
                        if (jsonOfflineData != null) {
                            Home content = gson.fromJson(jsonOfflineData, Home.class);
                            configureNewHome(content, setting1);
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    /**
                     * en este caso cargamos los datos de la cache pues no esta
                     * disponible el recurso online
                     */
                    Gson gson = new Gson();
                    String jsonOfflineData = JsonCacheReader.getCacheJson(Aplication.getAppFilepath(HomeActivity.this) + "/newhome.json");
                    if (jsonOfflineData != null) {
                        Home content = gson.fromJson(jsonOfflineData, Home.class);
                        configureNewHome(content, setting1);
                        Toast.makeText(HomeActivity.this, "You have no internet conection. Last cache is used instead.", Toast.LENGTH_LONG).show();
                    } else {
                        showNoConnectionError();
                    }
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        newsrecycler.removeAllViews();
        newsAdapter = null;
        Log.d("on destroy", "");
        ImageLoader.getInstance().clearMemoryCache();
    }

    public void showNoConnectionError() {
        if (!isFinishing()) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage("You have no internet connection!!");
            dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    HomeActivity.this.finish();
                }
            });
            dialog.show();
        }
    }

    private void configureNewHome(Home home, Setting setting) {
        List<Object> all = new LinkedList<>();
        boolean news_tap_active = false;
        if (home.getHomeHighlights().size() > 0)
            all.add("hl");
        if (home.getHomeNews().size() > 0) {
            all.add("0");//aqui agregamos los divider de las seccione 0-article,1-videos y 2-galerias
            if (setting.getHomeDefault().get(0).getHomeDefault().toLowerCase().equals("home-news")) {
                all.addAll(home.getHomeNews());
                news_tap_active = true;
            } else
                all.addAll(home.getSocial());
        }
        if (home.getVideos().size() > 0) {
            all.add("video_slide");
        }
        if (home.getGalleries().size() > 0)
            all.add("gallery_slide");
        newsAdapter = new NewsAdapter(this, all, home.getVideos(), home.getGalleries(), home.getHomeHighlights(),
                home.getSocial(), home.getHomeNews(), 0);
        newsrecycler.setAdapter(newsAdapter);
        UpdateUI();
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Loading content...");
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    void UpdateUI() {
        ++steps;// los pasos son cargar el menu y las noticias o el social
        if (steps == 2) {
            //entonces todo esta listo para mostrar los datos
            //updateHeader();
            hideProgressDialog();
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                if (extras.get("section") != null && ((String) extras.get("section")).equalsIgnoreCase("results")) {
                    onClick(result_menu_item);
                }
            }
            steps = 0;
            startService(new Intent(getApplicationContext(), JsonUpdateService.class));
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        //registramos el reciver que actualiza la vista
        if (!settingsChangeReciverIsRegister) {
            LocalBroadcastManager.getInstance(this).registerReceiver(settingsChangeReciver, new IntentFilter("UPDATE"));
            LocalBroadcastManager.getInstance(this).registerReceiver(racingReciver, new IntentFilter("RACING"));
            settingsChangeReciverIsRegister = true;
        }
        /*
        * si la activity no esta visible aqui es donde se coge
        * que se debe actualizar
        * */
        if (getSharedPreferences(Constants.JSON_UPDATE_PREFERENCE, MODE_PRIVATE).getBoolean(Constants.JSON_UPDATE_NEEDED, false)) {
            SharedPreferences pf = getApplicationContext().getSharedPreferences(Constants.JSON_UPDATE_PREFERENCE, MODE_PRIVATE);
            SharedPreferences.Editor editor = pf.edit();
            editor.putBoolean(Constants.JSON_UPDATE_NEEDED, false).commit();
            loadAll();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (settingsChangeReciverIsRegister) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(settingsChangeReciver);
            LocalBroadcastManager.getInstance(this).unregisterReceiver(racingReciver);
            settingsChangeReciverIsRegister = false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showHome() {
        if (current_fragment != null) {
            FragmentManager fmr = getSupportFragmentManager();
            fmr.beginTransaction().setCustomAnimations(R.anim.slide_in_forward, R.anim.slide_out_forward, R.anim.slide_in_forward, R.anim.slide_out_forward)
                    .remove(current_fragment).commit();
            current_fragment = null;
            ImageView image_previous = (ImageView) menu_selected_previous.findViewById(R.id.optionIMG);
            if (!(menu_selected_previous instanceof TextView))
                ImageLoader.getInstance().displayImage(menu_list.get((Integer) menu_selected_previous.getTag()).getMedia().get(0).getMed(), image_previous, defaultOptions);
            ImageView home_image = (ImageView) bottom_menu.findViewById(R.id.id_home).findViewById(R.id.optionIMG);
            ImageLoader.getInstance().displayImage(menu_list.get(0).getMedia().get(1).getMed(), home_image, defaultOptions);
            menu_selected_previous = bottom_menu.findViewById(R.id.id_home);
            return;
        }

        ImageView home_image = (ImageView) bottom_menu.findViewById(R.id.id_home).findViewById(R.id.optionIMG);
        ImageLoader.getInstance().displayImage(menu_list.get(0).getMedia().get(1).getMed(), home_image, defaultOptions);
        menu_selected_previous = bottom_menu.findViewById(R.id.id_home);


    }

    @Override
    public void onClick(View v) {
        //manejo la seleccion del menu dinamico
        if (v.getTag() != null) {
            //posicion del elemento en la lista de menu(menu_list)
            int position = Integer.parseInt(String.valueOf(v.getTag()));
            com.muevaelvolante.barapp.model.setting.Menu menuItemPrevious;
            com.muevaelvolante.barapp.model.setting.Menu menuItem = menu_list.get(position);
            //imagen anterior
            ImageView imagePreviuos = null;
            ImageView imageCurrent = null;
            if (!(menu_selected_previous instanceof TextView))
                imagePreviuos = (ImageView) menu_selected_previous.findViewById(R.id.optionIMG);
            //imagen actual
            if (!(v instanceof TextView))
                imageCurrent = (ImageView) v.findViewById(R.id.optionIMG);
            //aqui valido los tipos de menu
            if (menuItem.getID().toLowerCase().equals("home")) {
                if (current_fragment != null) {
                    showHome();
                }
                closeMenu();
                return;
            } else if (menuItem.getID().toLowerCase().equals("more")) {//menu more
                if (!isMenuOpen) {
                    menu_previous_more_selected = menu_selected_previous;
                    openMenu(v);
                }
                return;
            }
            menuItemPrevious = menu_list.get((int) menu_selected_previous.getTag());
            //si lo que selecciono esta visible en el menu principal
            if (menuItem.getSection().toLowerCase().equals("menu")) {
                isMoreActive = false;
                //cambio el icono del anterior
                if (imagePreviuos != null)
                    ImageLoader.getInstance().displayImage(menuItemPrevious.getMedia().get(0).getMed(), imagePreviuos, defaultOptions);
                //actualizo la imagen actual
                if (imageCurrent != null)
                    ImageLoader.getInstance().displayImage(menuItem.getMedia().get(1).getMed(), imageCurrent, defaultOptions);
            } else
                isMoreActive = true;

            if (menuItem.getID().toLowerCase().equals("teams")) {
                //actualizo la vista anterior a la actual

                FragmentManager fm = getSupportFragmentManager();
                current_fragment = TeamFragment.newInstance(menuItem.getURL(),menuItem.getID().toLowerCase());
                fm.beginTransaction().setCustomAnimations(R.anim.slide_in_forward, R.anim.slide_out_forward, R.anim.slide_in_forward, R.anim.slide_out_forward)
                        .replace(R.id.container, current_fragment)
                        .commit();
                if (!isMoreActive) {
                    menu_selected_previous = v;
                    menu_previous_more_selected = v;
                }
                if (isMenuOpen) {
                    closeMenu();
                }
            } else if (menuItem.getID().toLowerCase().equals("results")) {
                //actualizo la vista anterior a la actual

                FragmentManager fmr = getSupportFragmentManager();
                current_fragment = ResultFragment.newInstance(menuItem.getURL());
                fmr.beginTransaction().setCustomAnimations(R.anim.slide_in_forward, R.anim.slide_out_forward, R.anim.slide_in_forward, R.anim.slide_out_forward)
                        .replace(R.id.container, current_fragment)
                        .commit();
                if (!isMoreActive) {
                    menu_selected_previous = v;
                    menu_previous_more_selected = v;
                }
                if (isMenuOpen) {
                    closeMenu();
                }
                return;
            } else if (menuItem.getID().toLowerCase().equals("raisethebar")) {
                //actualizo la vista anterior a la actual

                FragmentManager fm = getSupportFragmentManager();
                current_fragment = SustainabilityFragment.newInstance(menuItem.getURL(), menuItem.getID().toLowerCase());
                fm.beginTransaction().setCustomAnimations(R.anim.slide_in_forward, R.anim.slide_out_forward, R.anim.slide_in_forward, R.anim.slide_out_forward)
                        .replace(R.id.container, current_fragment)
                        .commit();
                if (!isMoreActive) {
                    menu_selected_previous = v;
                    menu_previous_more_selected = v;
                }
                if (isMenuOpen) {
                    closeMenu();
                }
            } else if (menuItem.getID().toLowerCase().equals("actheguide")) {

                FragmentManager fmr = getSupportFragmentManager();
                current_fragment = ACTheGuideFragment.newInstance(menuItem.getURL(), menuItem.getID().toLowerCase());
                fmr.beginTransaction().setCustomAnimations(R.anim.slide_in_forward, R.anim.slide_out_forward, R.anim.slide_in_forward, R.anim.slide_out_forward)
                        .replace(R.id.container, current_fragment)
                        .commit();
                if (!isMoreActive) {
                    menu_selected_previous = v;
                    menu_previous_more_selected = v;
                }
                if (isMenuOpen) {
                    closeMenu();
                }
            } else if (menuItem.getID().toLowerCase().equals("profile")) {
                FragmentManager fmr = getSupportFragmentManager();
                current_fragment = new ProfileFragment();
                fmr.beginTransaction().setCustomAnimations(R.anim.slide_in_forward, R.anim.slide_out_forward, R.anim.slide_in_forward, R.anim.slide_out_forward)
                        .replace(R.id.container, current_fragment)
                        .commit();
                if (!isMoreActive) {
                    menu_selected_previous = v;
                    menu_previous_more_selected = v;
                }
                if (isMenuOpen) {
                    closeMenu();
                }
            } else if (menuItem.getID().toLowerCase().equals("boat")) {
                FragmentManager fmr = getSupportFragmentManager();
                current_fragment = BoatFragment.newInstance(menuItem.getURL(), menuItem.getID().toLowerCase());
                fmr.beginTransaction().setCustomAnimations(R.anim.slide_in_forward, R.anim.slide_out_forward, R.anim.slide_in_forward, R.anim.slide_out_forward)
                        .replace(R.id.container, current_fragment)
                        .commit();

                if (!isMoreActive) {
                    menu_selected_previous = v;
                    menu_previous_more_selected = v;
                }
                if (isMenuOpen) {
                    closeMenu();
                }
            } else if (menuItem.getID().toLowerCase().equals("community")) {
                FragmentManager fmr = getSupportFragmentManager();
                current_fragment = ComunityFragment.newInstance(menuItem.getURL(), menuItem.getID().toLowerCase());
                fmr.beginTransaction().setCustomAnimations(R.anim.slide_in_forward, R.anim.slide_out_forward, R.anim.slide_in_forward, R.anim.slide_out_forward)
                        .replace(R.id.container, current_fragment)
                        .commit();
                if (!isMoreActive) {
                    menu_selected_previous = v;
                    menu_previous_more_selected = v;
                }
                if (isMenuOpen) {
                    closeMenu();
                }
            } else if (menuItem.getID().toLowerCase().equals("photos")) {
                FragmentManager fmr = getSupportFragmentManager();
                current_fragment = GalleryListFragment.newInstance(menuItem.getURL(), menuItem.getID().toLowerCase());
                fmr.beginTransaction().setCustomAnimations(R.anim.slide_in_forward, R.anim.slide_out_forward, R.anim.slide_in_forward, R.anim.slide_out_forward)
                        .replace(R.id.container, current_fragment)
                        .commit();
                if (!isMoreActive) {
                    menu_selected_previous = v;
                    menu_previous_more_selected = v;
                }
                if (isMenuOpen) {
                    closeMenu();
                }
            } else if (menuItem.getID().toLowerCase().equals("videos")) {
                FragmentManager fmr = getSupportFragmentManager();
                current_fragment = VidioListFragment.newInstance(menuItem.getURL(), menuItem.getID().toLowerCase());
                fmr.beginTransaction().setCustomAnimations(R.anim.slide_in_forward, R.anim.slide_out_forward, R.anim.slide_in_forward, R.anim.slide_out_forward)
                        .replace(R.id.container, current_fragment)
                        .commit();
                if (!isMoreActive) {
                    menu_selected_previous = v;
                    menu_previous_more_selected = v;
                }
                if (isMenuOpen) {
                    closeMenu();
                }
            }

        }
        switch (v.getId()) {
            case R.id.out:
                closeMenu();
                handleLogOff();
                break;
        }
    }

    private void openMenu(View v) {
        Animation out = AnimationUtils.loadAnimation(this, android.R.anim.slide_in_left);
        more_menu.setAnimation(out);
        more_menu.setVisibility(View.VISIBLE);
        isMenuOpen = true;

        //aqui desmarcamos el icono seleccionado y marcamos more
        //primero obtenemos la posiscion del menu actual
        int position = (int) menu_selected_previous.getTag();
        //obtenemos el menu actual
        com.muevaelvolante.barapp.model.setting.Menu item = menu_list.get(position);
        //obtenemos su imagen por defecto y la asignamos
        if (!(menu_selected_previous instanceof TextView))
            ImageLoader.getInstance().displayImage(item.getMedia().get(0).getMed(), (ImageView) menu_selected_previous.findViewById(R.id.optionIMG), defaultOptions);
        //ahora marcamos more
        ImageLoader.getInstance().displayImage(more_menu_item.getMedia().get(1).getMed(), (ImageView) bottom_menu.findViewById(R.id.id_more).findViewById(R.id.optionIMG), defaultOptions);
        menu_selected_previous = v;
    }

    public void closeMenu() {
        Animation out = AnimationUtils.loadAnimation(this, android.R.anim.slide_out_right);
        more_menu.setAnimation(out);
        more_menu.setVisibility(View.GONE);
        isMenuOpen = false;
        if (current_fragment == null) {
            showHome();
            ImageLoader.getInstance().displayImage(more_menu_item.getMedia().get(0).getMed(), (ImageView) bottom_menu.findViewById(R.id.id_more).findViewById(R.id.optionIMG), defaultOptions);
            return;
        }
        if (!isMoreActive) {
            //aqui desmarcamos el icono more y marcamos el actual
            //primero obtenemos la posiscion del menu actual
            int position = (int) menu_selected_previous.getTag();
            //obtenemos el menu actual
            com.muevaelvolante.barapp.model.setting.Menu item = menu_list.get(position);
            //obtenemos su imagen por defecto y la saignamos
            ImageLoader.getInstance().displayImage(item.getMedia().get(0).getMed(), (ImageView) menu_selected_previous.findViewById(R.id.optionIMG), defaultOptions);
            //ahora marcamos more
            // return;
        }
        if (current_fragment != null && !isMoreActive) {
            ImageView image_previous = (ImageView) menu_previous_more_selected.findViewById(R.id.optionIMG);
            if (!(menu_selected_previous instanceof TextView))
                ImageLoader.getInstance().displayImage(menu_list.get((Integer) menu_previous_more_selected.getTag()).getMedia().get(1).getMed(), image_previous, defaultOptions);
            image_previous = (ImageView) menu_selected_previous.findViewById(R.id.optionIMG);
            if (!menu_previous_more_selected.equals(menu_selected_previous))
                if (!(menu_selected_previous instanceof TextView))
                    ImageLoader.getInstance().displayImage(menu_list.get((Integer) menu_selected_previous.getTag()).getMedia().get(0).getMed(), image_previous, defaultOptions);
            menu_selected_previous = menu_previous_more_selected;
        }
    }

    private void handleLogOff() {
        //en dependencia de el tipo de logeo
        SharedPreferences pf = getSharedPreferences(Constants.LOGIN_DATA, MODE_PRIVATE);
        if (pf.getString(Constants.LOGIN_DATA_USER_F, "").equals(Constants.LOGIN_DATA_USER_F)) {
            LoginManager.getInstance().logOut();
            clearLoginData(pf);
        }
        if (pf.getString(Constants.LOGIN_DATA_USER_T, "").equals(Constants.LOGIN_DATA_USER_T)) {
            CookieSyncManager.createInstance(this);
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeSessionCookie();
            clearLoginData(pf);

        }
        clearLoginData(pf);
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    public void clearLoginData(SharedPreferences pf) {
        pf.edit().clear().commit();
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        loadAll();
    }

    @Override
    public void gotoSection(String section) {
        for (int i = 0; i < menu_list.size(); i++) {
            if (menu_list.get(i).getID().toLowerCase().equals(section)) {
                onClick(menuViews.get(i));
            }

        }
    }

    /*
    * hace los cambios pertinentes en el home una vez que
    * cambien el json en el server
    * */
    class SettingsChangeReciver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            /*
             * cargamos el setting local y actualizamos la vista,
             * ponemos en las preferencias false la update pk esta la hizo
             * el broadcast
             *  */
            SharedPreferences pf = getApplicationContext().getSharedPreferences(Constants.JSON_UPDATE_PREFERENCE, MODE_PRIVATE);
            SharedPreferences.Editor editor = pf.edit();
            editor.putBoolean(Constants.JSON_UPDATE_NEEDED, false).commit();
            loadAll();
        }
    }

    class RacingReciver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.cancel(23);
            AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
            Bundle data = intent.getExtras();
            builder.setTitle(data.getString("tittle"))
                    .setMessage(data.getString("msg"));
            if (((String) data.get("action")).equalsIgnoreCase("results")) {
                builder.setPositiveButton("See Results", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        HomeActivity.this.onClick(result_menu_item);
                    }
                })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
            } else
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
            builder.show();
        }
    }
}
