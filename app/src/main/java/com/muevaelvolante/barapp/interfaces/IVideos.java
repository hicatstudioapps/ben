package com.muevaelvolante.barapp.interfaces;

import com.muevaelvolante.barapp.model.videos.Video;
import com.muevaelvolante.barapp.model.videos.Videos;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by CQ on 15/06/2016.
 */
public interface IVideos {

    @GET
    public Call<Videos> getVideos(@Url String url);
}
