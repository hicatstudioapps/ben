package com.muevaelvolante.barapp.interfaces;

import com.muevaelvolante.barapp.model.boat.Boat;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by CQ on 23/05/2016.
 */
public interface IBoat {

    @GET()
    public Call<Boat> getBoat(@Url String url);

}
