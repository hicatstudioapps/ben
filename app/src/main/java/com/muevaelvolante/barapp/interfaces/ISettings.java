package com.muevaelvolante.barapp.interfaces;

import com.google.android.gms.fitness.request.ClaimBleDeviceRequest;
import com.muevaelvolante.barapp.model.boat.Boat;
import com.muevaelvolante.barapp.model.news.HomeContent;
import com.muevaelvolante.barapp.model.setting.Setting;
import com.muevaelvolante.barapp.model.social.Social;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Url;

/**
 * Created by CQ on 22/03/2016.
 */
public interface ISettings {

    //    @GET("{lang}/app/setting.txt")
//    Call<Settings> getSetings(@Path("lang")String lang);
    @GET("{lang}/app/settings.json")
    Call<Setting> getSetings(@Path("lang") String lang);

    //
    @GET
    Call<ResponseBody> getUrl(@Url String lang);

    //    @GET("{lang}/app/news.txt")
//    Call<HomeContent> getNews(@Path("lang")String lang);
    @GET
    public Call<Social> getSocial(@Url String url);

    @GET
    public Call<ResponseBody> getHome(@Url String url);

}

