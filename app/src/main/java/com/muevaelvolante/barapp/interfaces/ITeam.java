package com.muevaelvolante.barapp.interfaces;


import com.muevaelvolante.barapp.model.team.Team;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Url;

/**
 * Created by CQ on 29/03/2016.
 */
public interface ITeam {

    @GET()
    Call<Team> getTeam(@Url String lang);
//    @GET("{lang}/app/team/sailing.json")
//    Call<Team> getTeam(@Path("lang")String lang);
}
