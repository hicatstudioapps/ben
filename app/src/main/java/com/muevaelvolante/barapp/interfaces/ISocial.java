package com.muevaelvolante.barapp.interfaces;

import com.muevaelvolante.barapp.model.social.Social;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by CQ on 22/04/2016.
 */
public interface ISocial {

    @GET
    public Call<Social> getSocial(@Url String url);
}
