package com.muevaelvolante.barapp.interfaces;

import com.muevaelvolante.barapp.model.results.Result;
import com.muevaelvolante.barapp.model.results.ResultRoot;


import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Url;

/**
 * Created by CQ on 14/04/2016.
 */
public interface IResults {
    @GET
    Call<ResultRoot> getResultsRoot(@Url String lang);
    @GET
    Call<ArrayList<Result>> getResults(@Url String lang);
}
