package com.muevaelvolante.barapp.interfaces;

import com.muevaelvolante.barapp.model.login.LoginResult;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 * Created by CQ on 26/06/2016.
 */
public interface IRegistrationAndLogin {

    @GET
    public Call<ResponseBody> register(@Url String url,@QueryMap Map<String,String> mapData);

    @GET
    public Call<LoginResult> login(@Url String url, @QueryMap Map<String,String> mapData);

    @GET
    public Call<ResponseBody> recoverPassw(@Url String url,@QueryMap Map<String,String> mapData);
}
