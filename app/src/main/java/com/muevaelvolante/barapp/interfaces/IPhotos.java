package com.muevaelvolante.barapp.interfaces;

import com.muevaelvolante.barapp.model.photos.Photos;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by CQ on 15/06/2016.
 */
public interface IPhotos {

    @GET
    public Call<Photos> getPhotos(@Url String url);
}
