package com.muevaelvolante.barapp.interfaces;

import com.muevaelvolante.barapp.model.AC.AC;

import retrofit.http.GET;
import retrofit2.Call;
import retrofit2.http.Url;

/**
 * Created by CQ on 18/04/2016.
 */
public interface IACTheGuide  {

    @retrofit2.http.GET
    public Call<AC> getAc(@Url String url);

}
