package com.muevaelvolante.barapp.interfaces;

import com.muevaelvolante.barapp.model.sustainability.Sustiantability;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by CQ on 20/04/2016.
 */
public interface ISustainability {

    @GET
    public Call<Sustiantability> getSust(@Url String url);

    @GET
    public Call<ResponseBody> sendPledge(@Url String url,@Query("tableName") String table, @Query("pledge") int pledge, @Query("deviceid") String id);
}
