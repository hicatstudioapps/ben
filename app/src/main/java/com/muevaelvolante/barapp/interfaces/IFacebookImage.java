package com.muevaelvolante.barapp.interfaces;

import com.muevaelvolante.barapp.model.FaceBookImage;
import com.muevaelvolante.barapp.model.facebook.ImageModel;
import com.muevaelvolante.barapp.model.news.HomeContent;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Url;

public interface IFacebookImage{
    @GET
    Call<ImageModel> getNews(@Url String url);
}
