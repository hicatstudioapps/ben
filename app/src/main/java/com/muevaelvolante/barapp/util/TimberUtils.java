/*
 * Copyright (C) 2015 Naman Dwivedi
 *
 * Licensed under the GNU General Public License v3
 *
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 */

package com.muevaelvolante.barapp.util;

import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.media.audiofx.AudioEffect;
import android.net.Uri;
import android.os.Build;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import android.util.TypedValue;

import com.muevaelvolante.barapp.R;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Period;
import org.joda.time.PeriodType;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class TimberUtils {

    public static final String MUSIC_ONLY_SELECTION = MediaStore.Audio.AudioColumns.IS_MUSIC + "=1"
            + " AND " + MediaStore.Audio.AudioColumns.TITLE + " != ''";

    public static boolean isMarshmallow() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

    public static boolean isLollipop() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }


    public static boolean isJellyBeanMR2() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2;
    }

    public static boolean isJellyBean() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
    }

    public static boolean isJellyBeanMR1() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1;
    }

    public static Uri getAlbumArtUri(long paramInt) {
        return ContentUris.withAppendedId(Uri.parse("content://media/external/audio/albumart"), paramInt);
    }



    public static final String makeLabel(final Context context, final int pluralInt,
                                         final int number) {
        return context.getResources().getQuantityString(pluralInt, number, number);
    }

    public static final String makeShortTimeString(final Context context, DateTime secs) {
        long days, hours, mins,sec;

        DateTime start= new DateTime();
        start= start.withZone(DateTimeZone.UTC);
        DateTime end= secs;
        Period period=new Period(start,end,PeriodType.yearDayTime().withYearsRemoved());
        days=period.getDays();
        hours= period.getHours();
        mins=period.getMinutes();
        sec=period.getSeconds();
//        days=secs/(24*3600);
//        hours = secs/(24*3600) / 3600;
//        secs %= 3600;
//        mins = secs/(24*3600) / 3600 / 60;
//        secs = secs/(24*3600) / 3600 % 60;
        Calendar c=Calendar.getInstance();


        final String durationFormat = context.getResources().getString(
                R.string.durationformatlong);
        return String.format(durationFormat,days, hours, mins, sec);
    }
    public static final long isCountDonwOver(final Context context, DateTime secs) {
        long days, hours, mins,sec;

        DateTime start= new DateTime();
        DateTime end= secs;
        Period period=new Period(start,end,PeriodType.yearDayTime().withYearsRemoved());
        days=period.getDays();
        hours= period.getHours();
        mins=period.getMinutes();
        sec=period.getSeconds();
        //        days=secs/(24*3600);
//        hours = secs/(24*3600) / 3600;
//        secs %= 3600;
//        mins = secs/(24*3600) / 3600 / 60;
//        secs = secs/(24*3600) / 3600 % 60;
        Calendar c=Calendar.getInstance();


        return days+hours+mins+sec;
    }

    public static int getActionBarHeight(Context context) {
        int mActionBarHeight;
        TypedValue mTypedValue = new TypedValue();

        context.getTheme().resolveAttribute(R.attr.actionBarSize, mTypedValue, true);

        mActionBarHeight = TypedValue.complexToDimensionPixelSize(mTypedValue.data, context.getResources().getDisplayMetrics());

        return mActionBarHeight;
    }




    public static int getBlackWhiteColor(int color) {
        double darkness = 1 - (0.299 * Color.red(color) + 0.587 * Color.green(color) + 0.114 * Color.blue(color)) / 255;
        if (darkness >= 0.5) {
            return Color.WHITE;
        } else return Color.BLACK;
    }

    public enum IdType {
        NA(0),
        Artist(1),
        Album(2),
        Playlist(3);

        public final int mId;

        IdType(final int id) {
            mId = id;
        }

        public static IdType getTypeById(int id) {
            for (IdType type : values()) {
                if (type.mId == id) {
                    return type;
                }
            }

            throw new IllegalArgumentException("Unrecognized id: " + id);
        }
    }




}
