package com.muevaelvolante.barapp.commons;

import android.os.Environment;

public class Constants {

	public static final boolean DEVELOPER_DEBUG = true;

	/*
	* CONSTANTES DE PREFERENCIAS
	* */
	public static String LOGIN_DATA="login";
	public static String LOGIN_DATA_USER_G="g+";
	public static String LOGIN_DATA_USER_F="fb";
	public static String LOGIN_DATA_USER_T="tw";
	public static String LOGIN_DATA_ACCES_TOKEN="token";
	public static String LOGIN_DATA_PICT="url";
	public static String LOGIN_DATA_USER="user";
	public static String LOGIN_DATA_EMAIL="email";
	public static String LOGIN_DATA_IDENTITY="identityID";
    
	public static String NOTIFICATION_NEWS="NEWS";
	public static String NOTIFICATION_RACING_ALERT="RACING";
	public static String NOTIFICATION_SUST_ALERT="SUS";
	public static String NOTIFICATION_EMAIL_ALERT="EMAIL";
	public static String NOTIFICATION_NEWS_ARN="NEWSARN";
	public static String NOTIFICATION_RACING_ARN="RACINGARN";

	public static String AC_THE_GUIDE="http://land-rover-bar.americascup.com/en/app/ac-the-guide.json";
//	public static String AC_THE_GUIDE="http://192.168.137.1/ben/en/app/ac-the-guide.json";

	public static String appRootDir=Environment.getExternalStorageDirectory()+"/Land Rover";
	public static String jsonCacheRootDir= Environment.getExternalStorageDirectory()+"/Land Rover/json";


	
	/*
	*Service updater json
	* */
	public static final String JSON_UPDATE_RUNNING = "is_running";
	public static final String JSON_UPDATE_PREFERENCE = "json";
	public static final String JSON_UPDATE_NEEDED = "update_needed";
	public static final String JSON_UPDATE_HEADER = "update_header";


	/*
	 * Clave de Google Developer.
	 */
	public static final String DEVELOPER_KEY = "AIzaSyD3VMFqpu0w_YvgSq0OdlGDbNNNH_4BkIs";

	public static final String Direct_Market_app = "market://details?id=";
	public static final String URL_Market_app = "http://play.google.com/store/apps/details?id=";
	
}