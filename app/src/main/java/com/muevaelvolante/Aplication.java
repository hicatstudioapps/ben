package com.muevaelvolante;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Environment;
import android.support.multidex.MultiDexApplication;

import com.facebook.FacebookSdk;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.muevaelvolante.barapp.activity.login.LoginActivity;
import com.muevaelvolante.barapp.cognito.CognitoClientManager;
import com.muevaelvolante.barapp.cognito.Constants;
import com.muevaelvolante.barapp.model.sustainability.Pledge;
import com.muevaelvolante.barapp.model.team.Sailing;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by CQ on 24/03/2016.
 */
public class Aplication extends MultiDexApplication {
    public static CognitoClientManager cognitoClientManager;
    public static boolean isLoginFromWeb=false;
    public static LoginActivity loginActivity=null;
    public static String homeUrl;
    public static Sailing sailing;
    public static boolean isAnonimous=false;
    public  static List<Pledge> pledge;
    public static Typeface robotoM;
    public static Typeface robotoR;
    public static Typeface robotoL;
    public static Typeface robotoB;
    private Tracker mTracker;
    public static String result_order="";

    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker("UA-21043334-29");
        }
        return mTracker;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        pledge= new LinkedList<>();
        CognitoClientManager.init(this);
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
//                .showImageOnLoading(R.drawable.empty)
//                .showImageOnFail(R.drawable.empty)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)

                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                //.memoryCacheExtraOptions(480,480)
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        ImageLoader.getInstance().init(config);
        FacebookSdk.sdkInitialize(this);

        robotoM= Typeface.createFromAsset(getAssets(),"rm.ttf");
        robotoR=Typeface.createFromAsset(getAssets(), "rr.ttf");
        robotoL=Typeface.createFromAsset(getAssets(), "rl.ttf");
        robotoB=Typeface.createFromAsset(getAssets(), "rb.ttf");
        /**
         * creamos el directorio de cache de los json
         */
        File cache= new File(com.muevaelvolante.barapp.commons.Constants.jsonCacheRootDir);
        if(!cache.exists())
            cache.mkdirs();

    }

    public static String getAppFilepath(Context context)
    {

        File path = context.getExternalFilesDir(null);
        if(path != null)
            return path.getAbsolutePath();
        else
        {
            String pkg = context.getPackageName();

            return Environment.getExternalStorageDirectory().getPath()
                    .concat("/Android/data/")
                    .concat(pkg)
                    .concat("/file");
        }
    }
}
